<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ListPriceDetail extends Model
{
    protected $table = "c1_baogia_chitiet";
    public $timestamp = false;
    public function dbPriceDetail()
    {
    	return DB::table('c1_baogia_chitiet');
    }
    public function listPriceDetail()
    {
    	return $this->dbPriceDetail()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddPriceDetail($data)
    {
    	return $this->dbPriceDetail()->insert($data);
    }
    public function editPriceDetail($id)
    {
    	return $this->dbPriceDetail()->where('id_baogia',$id)->get();
    }
    public function updatePriceDetail($data,$id)
    {
    	return $this->dbPriceDetail()->whereId($id)->update($data);
    }
    public function delPriceDetail($data)
    {
    	return $this->dbPriceDetail()->delete($data);
    }
}
