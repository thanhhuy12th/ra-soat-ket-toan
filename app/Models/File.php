<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class File extends Model
{
    protected $table = "c1_files";
    public $timestamp = false;
    public function dbFile()
    {
    	return DB::table('c1_files');
    }
    public function listFile()
    {
    	return $this->dbFile()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddFile($data)
    {
    	return $this->dbFile()->insert($data);
    }
    public function editFile($id)
    {
    	return $this->dbFile()->find($id);
    }
    public function updateFile($data,$id)
    {
    	return $this->dbFile()->whereId($id)->update($data);
    }
    public function delFile($data)
    {
    	return $this->dbFile()->delete($data);
    }
}
