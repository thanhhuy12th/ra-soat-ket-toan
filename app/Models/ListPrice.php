<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ListPrice extends Model
{
    protected $table = "c1_baogia";
    public $timestamp = false;
    public function dbBaogia()
    {
    	return DB::table('c1_baogia');
    }
    public function listBaogia()
    {
    	return $this->dbBaogia()->get()->toArray();
    }
    public function AddBaogia($data)
    {
    	return $this->dbBaogia()->insert($data);
    }
    public function getLastInsertId($data)
    {
        return $this->dbBaoGia()->insertGetId($data);
    }   
    public function editBaogia($id)
    {
    	return $this->dbBaogia()->find($id);
    }
    public function updateBaogia($data,$id)
    {
    	return $this->dbBaogia()->whereId($id)->update($data);
    }
    public function delBaogia($data)
    {
    	return $this->dbBaogia()->delete($data);
    }
}
