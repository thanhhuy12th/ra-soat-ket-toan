<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PriceContact extends Model
{
    
    protected $table = "c1_baogia_contact";
    public $timestamp = false;
    public function dbPriceContact()
    {
    	return DB::table('c1_baogia_contact');
    }
    public function listPriceContact()
    {
    	return $this->dbPriceContact()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddPriceContact($data)
    {
    	return $this->dbPriceContact()->insert($data);
    }
    public function editPriceContact($id)
    {
    	return $this->dbPriceContact()->find($id);
    }
    public function updatePriceContact($data,$id)
    {
    	return $this->dbPriceContact()->whereId($id)->update($data);
    }
    public function delPriceContact($data)
    {
    	return $this->dbPriceContact()->delete($data);
    }
}
