<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    protected $table = "c1_faq";
    public $timestamp = false;
    public function dbFAQ()
    {
    	return DB::table('c1_faq');
    }
    public function listFAQ()
    {
    	return $this->dbFAQ()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddFAQ($data)
    {
    	return $this->dbFAQ()->insert($data);
    }
    public function editFAQ($id)
    {
    	return $this->dbFAQ()->find($id);
    }
    public function updateFAQ($data,$id)
    {
    	return $this->dbFAQ()->whereId($id)->update($data);
    }
    public function delFAQ($data)
    {
    	return $this->dbFAQ()->delete($data);
    }
}
