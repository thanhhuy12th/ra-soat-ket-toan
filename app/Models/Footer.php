<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Footer extends Model
{
    protected $table = 'c1_config';
    public $timestamp = false;
    public function dbFooter()
    {
    	return DB::table('c1_config');
    }
    public function editFooter($id)
    {
    	return $this->dbFooter()->select('content','language')->find($id);
    }
    public function updateFooter($data)
    {
    	return $this->dbFooter()->whereId(9)->update($data);
    }
    public function updateLanguage($data)
    {
        return $this->dbFooter()->whereId(9)->update($data);
    }
}
