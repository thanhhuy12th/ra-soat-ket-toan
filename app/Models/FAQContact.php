<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class FAQContact extends Model
{
    
    protected $table = "c1_faq_contact";
    public $timestamp = false;
    public function dbFAQ()
    {
    	return DB::table('c1_faq_contact');
    }
    public function listFAQContact()
    {
    	return $this->dbFAQ()->get()->toArray();
    }
    public function AddFAQContact($data)
    {
    	return $this->dbFAQ()->insert($data);
    }
    public function editFAQContact($id)
    {
    	return $this->dbFAQ()->find($id);
    }
    public function updateFAQContact($data,$id)
    {
    	return $this->dbFAQ()->whereId($id)->update($data);
    }
    public function delFAQContact($data)
    {
    	return $this->dbFAQ()->delete($data);
    }
}
