<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Lienhe extends Model
{
    protected $table = "c1_lienhe";
    public $timestamp = false;
    public function dbLienhe()
    {
    	return DB::table('c1_lienhe');
    }
    public function listLienhe()
    {
    	return $this->dbLienhe()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddLienhe($data)
    {
    	return $this->dbLienhe()->insert($data);
    }
    public function editLienhe($id)
    {
    	return $this->dbLienhe()->find($id);
    }
    public function updateLienhe($data,$id)
    {
    	return $this->dbLienhe()->whereId($id)->update($data);
    }
    public function delLienhe($data)
    {
    	return $this->dbLienhe()->delete($data);
    }
}
