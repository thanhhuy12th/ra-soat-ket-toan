<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = "c1_dmtt";
    public $timestamp = false;
    public function dbCate()
    {
    	return DB::table('c1_dmtt');
    }
    public function listCate()
    {
    	return $this->dbCate()->orderBy('id','DESC')->get()->toArray();
    }
    public function addCate($data)
    {
    	return $this->dbCate()->insert($data);
    }
    public function editCate($id)
    {
    	return $this->dbCate()->find($id);
    }
    public function updateCate($data,$id)
    {
    	return $this->dbCate()->whereId($id)->update($data);
    }
    public function delCate($data)
    {
    	return $this->dbCate()->delete($data);
    }
}
