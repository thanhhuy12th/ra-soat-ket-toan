<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class FAQFile extends Model
{
    protected $table = "c1_faq_file";
    public $timestamp = false;
    public function dbFaqFile()
    {
    	return DB::table('c1_faq_file');
    }
    public function listFaqFile()
    {
    	return $this->dbFaqFile()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddFaqFile($data)
    {
    	return $this->dbFaqFile()->insert($data);
    }
    public function editFaqFile($id)
    {
    	return $this->dbFaqFile()->where('id_faq',$id)->get();
    }
    public function updateFaqFile($data,$id)
    {
    	return $this->dbFaqFile()->whereId($id)->update($data);
    }
    public function delFaqFile($id)
    {
    	return $this->dbFaqFile()->where('id_faq', '=', $id)->delete();
    }
   
}
