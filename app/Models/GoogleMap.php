<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class GoogleMap extends Model
{
    protected $table = 'c1_config';
    public $timestamp = false;
    public function dbGGMap()
    {
    	return DB::table('c1_config');
    }
    public function editGGmap($id)
    {
    	return $this->dbGGMap()->select('content')->find($id);
    }
    public function updateGGmap($data)
    {
    	return $this->dbGGMap()->whereId(5)->update($data);
    }
}
