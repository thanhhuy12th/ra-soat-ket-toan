<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use DB;

class SendMail extends Model
{
    public function sendMail($tenwebsite,$email_receive,$subject,$body)
    {
    	$email_gmail_send = DB::table('c1_config')->whereId(17)->first()->content;
        $password_gmail   = DB::table('c1_config')->whereId(18)->first()->content;
        $mail = new PHPMailer(true);                            // Passing `true` enables exceptions
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        ); 
        $mail->CharSet    = 'UTF-8';
        $mail->SMTPDebug  = 0;                                  // Enable verbose debug output
        $mail->isSMTP();                                        // Set mailer to use SMTP                  
        $mail->Host       = 'smtp.gmail.com';                   // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                               // Enable SMTP authentication
        $mail->Username   = $email_gmail_send;                       // SMTP username
        $mail->Password   = $password_gmail;                    // SMTP password
        $mail->SMTPSecure = 'tls';                              // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                // TCP port to connect to
        $mail->setFrom( $email_gmail_send, $tenwebsite);
        $mail->addAddress($email_receive, 'Chào bạn');          // Add a recipient, Name is optional
        $mail->addReplyTo($email_gmail_send, 'Mailer');
        $mail->isHTML(true);                                                                    
        $mail->Subject    = $subject;
        $mail->Body       = $body;
        return $mail;
    }

    function bodyQuanTri($baseurl)
    {
        $body = '<span>Khách hàng đã gửi cho bạn một câu hỏi. Vui lòng kiểm tra.</span>';
        return $body;
    }

    function bodyKhachHangVN($tenwebsite)
    {
        $body = '<span>Cảm ơn bạn đã đặt câu hỏi tại '.$tenwebsite.'. Chúng tôi sẽ liên hệ lại với bạn.</span>';
        return $body;
    }

    function bodyKhachHangEN($tenwebsite)
    {
        $body = '<span>Thank you for asking questions at '.$tenwebsite.'. We will contact you again.</span>';
        return $body;

    }
}
