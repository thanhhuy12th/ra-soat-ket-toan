<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class NhanXet extends Model
{
   	protected $table = "c1_nhanxet";
    public $timestamp = false;
    public function dbNhanXet()
    {
    	return DB::table('c1_nhanxet');
    }
    public function listNhanXet()
    {
    	return $this->dbNhanXet()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddNhanXet($data)
    {
    	return $this->dbNhanXet()->insert($data);
    }
    public function editNhanXet($id)
    {
    	return $this->dbNhanXet()->find($id);
    }
    public function updateNhanXet($data,$id)
    {
    	return $this->dbNhanXet()->whereId($id)->update($data);
    }
    public function delNhanXet($data)
    {
    	return $this->dbNhanXet()->delete($data);
    }
}
