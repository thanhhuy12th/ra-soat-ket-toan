<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    protected $table = "c1_menu";
    public $timestamp = false;
    public function dbMenu()
    {
    	return DB::table('c1_menu');
    }
    public function listMenu()
    {
    	return $this->dbMenu()->get()->toArray();
    }
    public function addMenu($data)
    {
    	return $this->dbMenu()->insert($data);
    }
    public function editMenu($id)
    {
    	return $this->dbMenu()->find($id);
    }
    public function updateMenu($data,$id)
    {
    	return $this->dbMenu()->whereId($id)->update($data);
    }
     public function delMenu($data)
    {
    	return $this->dbMenu()->delete($data);
    }
    public function getParent()
    {
        return $this->dbMenu()
                    ->select('id','name','parent_id')
                    ->get()
                    ->toArray();
    }
}
