<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Introduce extends Model
{
    protected $table = "c1_config";
    public $timestamp = false;
    public function dbIntroduce()
    {
    	return DB::table('c1_config');
    }
    public function editIntroduce($id)
    {
    	return $this->dbIntroduce()->select('content','language')->find($id);
    }
    public function updateIntroduce($data,$id,$lag)
    {
    	return $this->dbIntroduce()->whereId($id)->where('language',$lag)->update($data);
    }
    public function updateLanguage($data)
    {
    	return $this->dbIntroduce()->whereId(1)->update($data);
    }

}
