<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BaoGiaContact extends Model
{
      
    protected $table = "c1_baogia_contact";
    public $timestamp = false;
    public function dbBaoGiaContact()
    {
    	return DB::table('c1_baogia_contact');
    }
    public function listBaoGiaContact()
    {
    	return $this->dbBaoGiaContact()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddBaoGiaContact($data)
    {
    	return $this->dbBaoGiaContact()->insert($data);
    }
    public function editBaoGiaContact($id)
    {
    	return $this->dbBaoGiaContact()->find($id);
    }
    public function updateBaoGiaContact($data,$id)
    {
    	return $this->dbBaoGiaContact()->whereId($id)->update($data);
    }
    public function delBaoGiaContact($data)
    {
    	return $this->dbBaoGiaContact()->delete($data);
    }
}
