<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Service extends Model
{
    protected $table = "c1_dichvu";
    public $timestamp = false;
    public function dbService()
    {
    	return DB::table('c1_dichvu');
    }
    public function listService()
    {
    	return $this->dbService()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddService($data)
    {
    	return $this->dbService()->insert($data);
    }
    public function editService($id)
    {
    	return $this->dbService()->find($id);
    }
    public function updateService($data,$id)
    {
    	return $this->dbService()->whereId($id)->update($data);
    }
    public function delService($data)
    {
    	return $this->dbService()->delete($data);
    }
}
