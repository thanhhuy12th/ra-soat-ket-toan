<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class News extends Model
{
    protected $table = "c1_tintuc";
    public $timestamp = false;
    public function dbNews()
    {
    	return DB::table('c1_tintuc');
    }
    public function listNews()
    {
    	return $this->dbNews()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddNews($data)
    {
    	return $this->dbNews()->insert($data);
    }
    public function editNews($id)
    {
    	return $this->dbNews()->find($id);
    }
    public function updateNews($data,$id)
    {
    	return $this->dbNews()->whereId($id)->update($data);
    }
    public function delNews($data)
    {
    	return $this->dbNews()->delete($data);
    }
}
