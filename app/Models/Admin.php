<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = "c1_admin";
    public $timestamp = false;
    public function dbAdmin()
    {
    	return DB::table('c1_admin');
    }
    public function listAdmin()
    {
    	return $this->dbAdmin()->get()->toArray();
    }
    public function addAdmin($data)
    {
   		return $this->dbAdmin()->insert($data);
    }
    public function editAdmin($id)
    {
    	return $this->dbAdmin()->find($id);
    }
    public function updateAdmin($data,$id)
    {
    	return $this->dbAdmin()->whereId($id)->update($data);
    }
    public function delAdmin($id)
    {
    	$check = $this->editAdmin($id);
        if($check->role != 1){
            $this->dbAdmin()->delete($id);
            return true;
        } else {
            return false;
        }
    }
}
