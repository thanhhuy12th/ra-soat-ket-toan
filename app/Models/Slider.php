<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Slider extends Model
{
    protected $table = "c1_slider";
    public $timestamp = false;
    public function dbSider()
    {
    	return DB::table('c1_slider');
    }
    public function listSlider()
    {
    	return $this->dbSider()->get()->toArray();
    }
    public function AddSlider($data)
    {
    	return $this->dbSider()->insert($data);
    }
    public function editSlider($id)
    {
    	return $this->dbSider()->find($id);
    }
    public function updateSlider($data,$id)
    {
    	return $this->dbSider()->whereId($id)->update($data);
    }
    public function delSlider($data)
    {
    	return $this->dbSider()->delete($data);
    }
}
