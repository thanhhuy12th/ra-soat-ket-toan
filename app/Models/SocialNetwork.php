<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    protected $table = 'c1_config';
    public $timestamp = false;
    public function dbSocial()
    {
    	return DB::table('c1_config');
    }
    public function editSocial($id)
    {
    	return $this->dbSocial()->select('content')->find($id);
    }
     public function updateTwitter($data)
    {
        return $this->dbSocial()->whereId(3)->update($data);
    }
    public function updateFacebook($data)
    {
    	return $this->dbSocial()->whereId(4)->update($data);
    }
}
