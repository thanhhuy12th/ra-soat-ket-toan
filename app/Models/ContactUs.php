<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ContactUs extends Model
{
    protected $table = 'c1_config';
    public $timestamp = false;
    public function dbContactsUs()
    {
    	return DB::table('c1_config');
    }
  	public function editContactUs($id)
  	{
  		return $this->dbContactsUs()->select('content','name_phone')->find($id);
  	}
    public function updateHotLine1($data)
    {
      return $this->dbContactsUs()->whereId(6)->update($data);
    }
     public function updatePhoneName1($data)
    {
      return $this->dbContactsUs()->whereId(6)->update($data);
    }
    public function updateHotLine2($data)
    {
      return $this->dbContactsUs()->whereId(7)->update($data);
    }
    public function updatePhoneName2($data)
    {
      return $this->dbContactsUs()->whereId(7)->update($data);
    }
    public function updateHotLine3($data)
    {
      return $this->dbContactsUs()->whereId(8)->update($data);
    }
    public function updatePhoneName3($data)
    {
      return $this->dbContactsUs()->whereId(8)->update($data);
    }
}
