<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class CategoryFile extends Model
{
    protected $table = "c1_files_category";
    public $timestamp = false;
    public function dbCateFile()
    {
    	return DB::table('c1_files_category');
    }
    public function listCateFile()
    {
    	return $this->dbCateFile()->orderBy('id','DESC')->get()->toArray();
    }
    public function AddCateFile($data)
    {
    	return $this->dbCateFile()->insert($data);
    }
    public function editCateFile($id)
    {
    	return $this->dbCateFile()->find($id);
    }
    public function updateCateFile($data,$id)
    {
    	return $this->dbCateFile()->whereId($id)->update($data);
    }
    public function delCateFile($data)
    {
    	return $this->dbCateFile()->delete($data);
    }

    public function getParent()
    {  
        return $this->dbCateFile()->get()->toArray();
    }

    public function listCateByIdParent($id) 
    {
        return $this->dbCateFile()->where("parent_id",$id)->get()->toArray();
    }
        public function getParentV2()
    {
        return $this->dbCateFile()
                    ->select('id','name','parent_id')
                    ->get()
                    ->toArray();
    }

}
