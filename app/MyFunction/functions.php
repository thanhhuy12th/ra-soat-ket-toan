<?php

function select_products_to_category_parent($id)
{
    $result = \DB::table('c1_menu')->where('parent_id',$id)->orderBy('created_at','ASC')->get()->toArray();
    return $result;
}
function select_category_cha()
{
	$result = \DB::table('c1_files_category')->where('parent_id',0)->orderBy('id','ASC')->take(6)->get()->toArray();
	return $result;
}
function select_category_con($id_cha)
{
    $result = \DB::table('c1_files_category')->where('parent_id',$id_cha)->orderBy('id','ASC')->get()->toArray();
    return $result;
}
function showCategories($categories, $parent_id = 0)
{
    
    $cate_child = array();
    $file_child = array();
    foreach ($categories as $key => $item)
    {
        if ($item->parent_id == $parent_id)
        {
            $cate_child[] = $item;
        }
        // $data = \DB::table('c1_files')->where('type',2)->where('id_category',$item->id)->get()->toArray();
        // foreach ($data as $key => $value) {
                
        //     $file_child[] = $value;
        // }
    }
    if ($categories)
    {
        echo '<ul id="f1combined" class="jslists">';
        foreach ($cate_child as $key => $item)
        {
        	echo '<li style="margin-left: 0px">'.$item->name;
                echo '<ul>';
                showCategories($categories, $item->id);
                    $data = \DB::table('c1_files')->where('type',2)->where('id_category',$item->id)->get()->toArray();
                    foreach ($data as $key => $value) {
                        $splittedstring=explode("||",$value->file);   
                        for($i =0; $i<count($splittedstring)-1; $i++)
                        {
                            echo '<li style="margin-left: 12px">';
                                echo '<img src="frontend/assets/js/libs/tree/document.png"><a href="./uploads/file/'.$splittedstring[$i].'" download>'.$value->title.'</a>';   
                            echo '</li>';
                        }
                    }
                echo '</ul>';   
            echo '</li>'; 
        }
        echo '</ul>';
    }
}
?>
