<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin()
    {
    	return view('login');
    }
    public function postLogin(Request $request)
    {
    	$credentials = $request->only('email', 'password');

        if (Auth::guard('admin')->attempt($credentials)) {
            
            return redirect('./admin');

        } else {

        	return redirect()->route('getLogin');
        }
    }
    public function getLogout()
    {
    	Auth::guard('admin')->logout();

    	return redirect()->route('getLogin');
    }

    public function isAdmin()
    {
        if(Auth::guard('admin')->user()->role == 2 || Auth::guard('admin')->user()->role == null) {
            Auth::guard('admin')->logout();
            return redirect()->route('getLogin');
        }
    }
}
