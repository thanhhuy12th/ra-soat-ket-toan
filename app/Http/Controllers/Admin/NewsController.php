<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\NewsRequest;
use App\Http\Controllers\Controller;
use App\Models\News;
use DateTime;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $news;
    public function __construct(News $news)
    {
        $this->news = $news;
    }
    public function ImgUpdate($id)
    {
        $item = $this->news->editNews($id);
        $filename = 'public/uploads/news'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    } 
    public function index()
    {
        $data['news'] = $this->news->listNews();

        return view('admin.modules.news.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
         $data  = array(
            'title'         => $request->title,
            'alias'         => (empty($request->alias))? str_slug($request->title,"-"):$request->alias,
            'intro'         => $request->intro,
            'content'       => $request->content,
            'parent_id'     => $request->parent_id,
            'language'      => $request->language,
            'status'        => $request->status,
            'created_at'    => new DateTime
        );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/news';
                $image->move($des,$image_name);
                $data['image']=$image_name;
        }

       $insert = $this->news->AddNews($data);
       if ($insert) {
           return redirect()->route('news.index')->with("success",'message.store_required');
       }else{
        return redirect()->back()->with('error','Thêm dữ liệu không thành công. Vui lòng thử lại');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news'] = $this->news->editNews($id);

        return view('admin.modules.news.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data  = array(
            'title'         => $request->title,
            'alias'         => (empty($request->alias))? str_slug($request->title,"-"):$request->alias,
            'intro'         => $request->intro,
            'content'       => $request->content,
            'parent_id'     => $request->parent_id,
            'language'      => $request->language,
            'status'        => $request->status,
            'created_at'    => new DateTime
        );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/news';
                $image->move($des,$image_name);
                $data['image']=$image_name;
                $this->ImgUpdate($id);
        }
        $update = $this->news->updateNews($data,$id);
        if ($update) {
           return redirect()->route('news.index')->with("success",'message.update_required');
        }else {
            
         return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->news->delNews($data[$i]);
        }

        return redirect()->route('news.index')->with("success",'message.delete_required');
    }
}
