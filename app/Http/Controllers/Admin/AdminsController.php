<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LoginController;
use App\Models\Admin;
use Auth,DB,Hash,DateTime;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $admin;
    private $login;
    public function __construct(Admin $admin, LoginController $login)
    {
        $this->login = $login;
        $this->admins = $admin;
    }
     public function UpdateImg($id)
    {
        $item = $this->admins->editAdmin($id);
        $filename = 'public/uploads/avatar'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $this->login->isAdmin();
        $data['admin'] = $this->admins->listAdmin();

        return view('admin.modules.admin.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->login->isAdmin();
        return view('admin.modules.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
         $data = array(
                'username' => $request->username,
                'fullname' => $request->fullname,
                'email'    => $request->email,
                'phone'    => $request->phone,
                'password' => bcrypt($request->password),
                'role'     => $request->role,
                'created_at' => new DateTime
        );
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_name = time(). '-'.$image->getClientOriginalName();
            $des = 'public/uploads/avatar';
            $image->move($des,$image_name);
            $data['image']=$image_name;
        } 
        
         $insert = $this->admins->addAdmin($data);

         if ($insert) {

             return redirect()->route('admin.index')->with("success",'message.store_required');
         } else {

            return redirect()->back()->with('error','Thêm thành viên không thành công. Vui lòng thử lại');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->login->isAdmin();
        $data['admin'] = $this->admins->editAdmin($id);

        return view('admin.modules.admin.edit',$data);
    }

    public function Getchangepass()
    {
        return view('admin.modules.admin.changepass');
    }

    public function changepass(Request $request)
    {

        if (!(Hash::check($request->currentpassword, Auth::guard('admin')->user()->password))) {
            // Mật khẩu trùng khớp
            return redirect()->back()->with("error","Mật khẩu hiện tại của bạn không khớp với mật khẩu bạn cung cấp. Vui lòng thử lại.");
        }
        if($request->newpassword != $request->newpassword_confirmation) {
            //Mật khẩu hiện tại và mật khẩu mới giống nhau
            return redirect()->back()->with("error","Mật khẩu mới không thể giống như mật khẩu hiện tại của bạn. Vui lòng chọn một mật khẩu khác nhau.");
        }

        $validatedData = $request->validate([
            'currentpassword' => 'required',
            'newpassword' => 'required|min:6|confirmed',
        ]);
        // Đổi mật khẩu
        
        $data =  array('password' =>bcrypt($request->newpassword));
         
        
         DB::table('c1_admin')->whereId(Auth::guard('admin')->user()->id)->update($data);

         echo "<script>
                alert('Mật khẩu của bạn đã được thay đổi thành công.');
                window.location = '".url('/admin')."'
              </script>";
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->login->isAdmin();
        $data = array(
                'username' => $request->username,
                'fullname' => $request->fullname,
                'email'    => $request->email,
                'phone'    => $request->phone,
                'password' => bcrypt($request->password),
                'role'     => $request->role,
                'created_at' => new DateTime
        );
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_name = time(). '-'.$image->getClientOriginalName();
            $des = 'public/uploads/avatar';
            $image->move($des,$image_name);
            $data['image']=$image_name;
            $this->UpdateImg($id);
        }
       $update =  $this->admins->updateAdmin($data,$id);
       if ($update) {
            return redirect()->route('admin.index')->with('success',"message.update_required");
       } else {

          return redirect()->back()->with('error','Cập nhật thành viên không thành công. Vui lòng thử lại');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->login->isAdmin();
        $result = $this->admins->delAdmin($id);

        if($result){
             return redirect()->route('admin.index')->with('success','message.destroy_required');
         } else{
            return redirect()->route('admin.index')->with('error','message.error_required');
         }    
    }
}
