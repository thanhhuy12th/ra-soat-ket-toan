<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ListPrice;
use App\Models\ListPriceDetail;
use DB,DateTime;

class ListPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $baogia;
    private $baogiachitiet;
    public function __construct(ListPrice $baogia, ListPriceDetail $baogiachitiet)
    {
        $this->baogia = $baogia;
        $this->baogiachitiet = $baogiachitiet;
    }
    public function CreateBaogia($request)
    {
        $data  = array(
            'title'         => $request->title,
            'alias'         =>(empty($request->alias))? str_slug($request->title, "-") : $request->alias,
            'description'   => $request->description,
            'position'      => $request->position,
            'created_at'    => new DateTime 
        );

        return $data;
    }
    public function index()
    {
        $data['baogia'] = $this->baogia->listBaogia();

        return view('admin.modules.banggia.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.banggia.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $data  = $this->CreateBaogia($request);
        $last_insert_id = $this->baogia->getLastInsertId($data);  
        $content = $request->content;
        $price =  $request->price;
        $position =  $request->position_detail;
        for ($i=0; $i < count($content); $i++) {

            $insert = DB::table('c1_baogia_chitiet')->insert([
                            'id_baogia'     => $last_insert_id,
                            'content'       => $content[$i],
                            'price'         => $price[$i],
                            'position'      => $position[$i], 

            ]);

         }
          

        return redirect()->route('banggia.index')->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['baogia'] = $this->baogia->editBaogia($id);

        // lay chi tiet bao gia ư idbso gia
        
        $data['baogia_chitiet'] = $this->baogiachitiet->editPriceDetail($id);


        return view('admin.modules.banggia.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('c1_baogia')
                        ->whereId($id)
                        ->update([
                            'title' => $request->title,
                            'alias'         =>(empty($request->alias))? str_slug($request->title, "-") : $request->alias,
                            'description'   => $request->description,
                            'position'      => $request->position,
                            'updated_at'    => new DateTime 
                        ]);
        // update bao gia


       DB::table('c1_baogia_chitiet')->where('id_baogia', '=', $id)->delete();

        // delete all chi tiet bao gia where id_baogia = $id

        $content = $request->content;
        $price =  $request->price;
        $position =  $request->position_detail;
        // dd($content);
         for ($i=0; $i < count($content); $i++) {

            $insert = DB::table('c1_baogia_chitiet')->insert([
                            'id_baogia'     => $id,
                            'content'       => $content[$i],
                            'price'         => $price[$i],
                            'position'      => $position[$i], 

            ]);

         }
        return redirect()->route('banggia.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->baogia->delBaogia($data[$i]);
            DB::table('c1_baogia_chitiet')->where('id_baogia', '=', $data[$i])->delete();
        }

        return redirect()->route('banggia.index')->with("success",'message.delete_required');
    }
}
