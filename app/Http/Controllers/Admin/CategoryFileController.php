<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategoryFile;
use DB,DateTime;

class CategoryFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $categoryfile;
    public function __construct(CategoryFile $categoryfile)
    {
        $this->categoryfile = $categoryfile;
    }
    public function CreateCateFile($request)
    {
        $data  = array(
            'name'      => $request->name,
            'alias'     => (empty($request->alias)) ? str_slug($request->name, "-") : $request->alias,
            'parent_id' => $request->parent_id,
            'created_at'=> new DateTime
        );

        return $data;
    }
    public function index()
    {
        $data['optArr'] = $this->categoryfile->getParentV2();
        $data['catefile'] = $this->categoryfile->listCateFile();

        return view('admin.modules.category-file.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['optArr'] = $this->categoryfile->getParentV2();
        $data['menu'] = DB::table('c1_menu')->get()->toArray();

        return view('admin.modules.category-file.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data =  $this->CreateCateFile($request);

       $insert = $this->categoryfile->AddCateFile($data);
       if ($insert) {
         return redirect()->route('category-file.index')->with("success",'message.store_required');
       }else {
        return redirect()->back()->with('error','Thêm dữ liệu không thành công. Vui lòng thử lại');
       }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['optArr'] = $this->categoryfile->getParentV2();
        $data['menu'] = DB::table('c1_menu')->get()->toArray();
        $data['catefile'] = $this->categoryfile->editCateFile($id);

        return view('admin.modules.category-file.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  $this->CreateCateFile($request);

       $update = $this->categoryfile->updateCateFile($data,$id);
       if ( $update ) {
           return redirect()->route('category-file.index')->with("success",'message.update_required');
       } else {
            return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
       }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->categoryfile->delCateFile($data[$i]);
        }

        return redirect()->route('category-file.index')->with("success",'message.delete_required');
    }
}
