<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class HomeController extends Controller
{
    public function index()
    {
    	$data = [];

    	$data['role'] = (Auth::guard('admin')->user()->role);
    	$data['fullname'] =(Auth::guard('admin')->user()->fullname);
    	
    	$data['news'] = DB::table('c1_tintuc')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
    	$data['lienhe'] = DB::table('c1_lienhe')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
    	$data['file'] = DB::table('c1_files')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
    	$data['user'] = DB::table('c1_admin')->orderBy('id', 'DESC')->limit(5)->get()->toArray();

    	return view('admin.dashboard.index',$data);
    }
}
