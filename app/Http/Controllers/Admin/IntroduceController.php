<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Introduce;
use DateTime;

class IntroduceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $introduce;
    public function __construct(Introduce $introduce)
    {
        $this->introduce = $introduce;
    }
    public function index()
    {
        // $data['introduces']  = $this->introduce->editIntroduce(1);  
        $data['introducesVN'] = $this->introduce->editIntroduce(1);
        $data['introducesEN'] = $this->introduce->editIntroduce(20);
        $data['language'] = $this->introduce->editIntroduce(1);

        return view('admin.modules.gioithieu.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->introduce->updateIntroduce(['content' =>$request->GioiThieu],1,$request->Language);
        $this->introduce->updateIntroduce(['content' =>$request->GioiThieu],20,$request->Language);
        $this->introduce->updateLanguage(['language' =>$request->Language]);

        return redirect()->back()->with('success',"message.update_required");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
