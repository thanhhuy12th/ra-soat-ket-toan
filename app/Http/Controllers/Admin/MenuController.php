<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MenuRequest;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use DateTime;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $menu;
    public function __construct(Menu $menu)
    {
        $this->menus = $menu;
    }
    public function CreateMenu($request)
    {
        $data = array(
            'name'      => $request->name,
            'slug'      => (empty($request->slug))? str_slug($request->name,"-"):$request->slug,
            'parent_id' => $request->parent_id,
            'link'      => $request->link,
            'position'  => $request->position,
            'language'  => $request->language,
            'created_at'=> new DateTime
        );
        return $data;
    }
    public function index()
    {
        $data['optArr'] = $this->menus->getParent();
        $data['menus'] = $this->menus->listMenu();

        return view('admin.modules.menu.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['optArr'] = $this->menus->getParent();
        return view('admin.modules.menu.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $data = $this->CreateMenu($request);

       $insert = $this->menus->addMenu($data);
        if ($insert) {
            return redirect()->route('menu.index')->with("success",'message.store_required');
        }else {
            return redirect()->back()->with('error','Thêm dữ liệu không thành công. Vui lòng thử lại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['optArr'] = $this->menus->getParent();
        $data['menu']   = $this->menus->editMenu($id);

        return view('admin.modules.menu.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->CreateMenu($request);

       $update = $this->menus->updateMenu($data,$id);
       if ($update) {
            return redirect()->route('menu.index')->with("success",'message.update_required');
       }else{
            return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
       }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->menus->delMenu($data[$i]);
        }

        return redirect()->route('menu.index')->with("success",'message.delete_required');
    }
}
