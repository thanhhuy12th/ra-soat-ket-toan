<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use DateTime;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $contactus;
    public function __construct(ContactUs $contactus)
    {
        $this->contactus =$contactus;
    }
    public function index()
    {
        $data['hotline1'] = $this->contactus->editContactUs(6);
        $data['phonename1'] = $this->contactus->editContactUs(6);

        $data['hotline2'] = $this->contactus->editContactUs(7);
        $data['phonename2'] = $this->contactus->editContactUs(7);

        $data['hotline3'] = $this->contactus->editContactUs(8);
        $data['phonename3'] = $this->contactus->editContactUs(8);
       

        return view('admin.modules.contactus.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.contactus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.contactus.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //$this->contactus->updateContactUs(['content' => $request->Lienhe]);
        $this->contactus->updateHotLine1(['content' => $request->HotLine1]);
        $this->contactus->updatePhoneName1(['name_phone' => $request->PhoneName1]);

        $this->contactus->updateHotLine2(['content' => $request->HotLine2]);
        $this->contactus->updatePhoneName2(['name_phone' => $request->PhoneName2]);

        $this->contactus->updateHotLine3(['content' => $request->HotLine3]);
        $this->contactus->updatePhoneName3(['name_phone' => $request->PhoneName3]);

        return redirect()->route('contactus.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }
}
