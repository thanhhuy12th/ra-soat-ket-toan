<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lienhe;
use DateTime;

class LienheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $lienhe;
    public function __construct(Lienhe $lienhe)
    {
        $this->lienhe = $lienhe;
    }
    public function createContact($request)
    {
        $data = array(
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'address'   => $request->address,
            'content'   => $request->content,
            'created_at'=> new DateTime 
        );

        return $data;
    }
    public function index()
    {
        $data['lienhe'] = $this->lienhe->listLienhe();

        return view('admin.modules.lienhe.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.lienhe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['lienhe'] = $this->lienhe->editLienhe($id);

        return view('admin.modules.lienhe.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->createContact($request);

       $update = $this->lienhe->updateLienhe($data,$id);
       if ( $update) {
          return redirect()->route('lienhe.index')->with("success",'message.update_required');
       }else{
         return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
       }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data) ; $i++) { 
            $this->lienhe->delLienhe($data[$i]);
        }

        return redirect()->route('lienhe.index')->with("success",'message.delete_required');
    }
}
