<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\FAQRequest;
use App\Http\Controllers\Controller;
use App\Models\FAQ;
use App\Models\FAQFile;
use DateTime;

class FAQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $faq;
    private $faqfile;

    public function __construct(FAQ $faq, FAQFile $faqfile)
    {
        $this->faq = $faq;
        $this->faqfile = $faqfile;
    }
    public function CreateFAQ($request)
    {
        $data = array(
            'title'         => $request->title,
            'content'       => $request->content,
            'position'      => $request->position,
            'keyword'       => $request->keyword,
            'language'      => $request->language,
            'created_at'    => new Datetime
        );
        return $data;
    }
    public function index()
    {
        $data['faq'] = $this->faq->listFAQ();

        return view('admin.modules.hoidap.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.hoidap.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FAQRequest $request)
    {
        if(isset($_POST['btnAddFaq']))
        {
        $faq             = new FAQ;
        $faq->title      = $request->title;
        $faq->content    = $request->content;
        $faq->position   = $request->position;
        $faq->keyword    = $request->keyword;
        $faq->language   = $request->language;
        $faq->created_at = new Datetime;
        $faq->save();
        $link    = $request->linkfile ;
        $tenfile         = $request->tenfile;
        for ($i=0; $i < count($link); $i++) {
            $faqfile             = new FAQFile;
            $faqfile->id_faq     = $faq->id;
            $faqfile->link       = $link[$i];
            $faqfile->tenfile    = $tenfile[$i];
            $faqfile->created_at = new Datetime;
            $faqfile->save();
        }
        return redirect()->route('faq.index')->with("success",'message.store_required');
        }else{
        return redirect()->back()->with('error','Thêm dữ liệu không thành công. Vui lòng thử lại');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['faqfile'] = $this->faqfile->editFaqFile($id);
        $data['faq'] = $this->faq->editFAQ($id);

        return view('admin.modules.hoidap.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FAQRequest $request, $id)
    {
        
        if(isset($_POST['btnEditFile']))
        {
            $data    = $this->CreateFAQ($request);
            $this->faq->updateFAQ($data,$id);
            $this->faqfile->delFaqFile($id);
            
             $link    = $request->linkfile ;
            $tenfile = $request->tenfile;
            //dd($link);
            for ($i=0; $i < count($link); $i++) {

                $faqfile             = new FAQFile;
                $faqfile->id_faq     = $id;
                $faqfile->link       = $link[$i];
                $faqfile->tenfile    = $tenfile[$i];
                $faqfile->created_at = new Datetime;
                $faqfile->save();
            }
            return redirect()->route('faq.index')->with("success",'message.store_required');
        } else {
            return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data); $i++) { 
            $this->faq->delFAQ($data[$i]);
            $this->faqfile->delFaqFile($data[$i]);
        }

        return redirect()->route('faq.index')->with("success",'message.delete_required');
    }
}
