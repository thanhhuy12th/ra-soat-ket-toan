<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FAQContact;
use DateTime;

class FAQContactController extends Controller
{
    private $faqcontact;
    public function __construct(FAQContact $faqcontact)
    {
        $this->faqcontact = $faqcontact;
    }
    public function createFaqContact($request)
    {
        $data = array(
            'email'     => $request->email,
            'phone'     => $request->phone,
            'content'   => $request->content,
            'created_at'=> new DateTime 
        );

        return $data;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['faqcontact'] = $this->faqcontact->listFAQContact();

        return view('admin.modules.faqcontact.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.faqcontact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['faqcontact'] = $this->faqcontact->editFAQContact($id);

        return view('admin.modules.faqcontact.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->createFaqContact($request);

       $update = $this->lienhe->updateFAQContact($data,$id);
       if ( $update) {
          return redirect()->route('faqcontact.index')->with("success",'message.update_required');
       }else{
         return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data) ; $i++) { 
            $this->faqcontact->delFAQContact($data[$i]);
        }

        return redirect()->route('faqcontact.index')->with("success",'message.delete_required');
    }
}
