<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ServiceRequest;
use App\Http\Controllers\Controller;
use App\Models\Service;
use DateTime;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $service;
    public function __construct(Service $service)
    {
        $this->service = $service;
    }
    public function UpdateImgService($id)
    {
        $item = $this->service->editService($id);
        $filename = 'public/uploads/service'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $data['service'] = $this->service->listService();

        return view('admin.modules.dichvu.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.dichvu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $data =array(
            'name'      => $request->name,
            'alias'     => (empty($request->alias))? str_slug($request->name,"-"): $request->alias,
            'link'      => $request->link,
            'content'   => $request->content,
            'position'  => $request->position,
            'language'  => $request->language,   
            'created_at'=> new DateTime
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/service';
                $image->move($des,$image_name);
                $data['image']=$image_name;

            }

        $insert = $this->service->AddService($data);
        if ($insert) {
            return redirect()->route('dichvu.index')->with("success",'message.store_required');
        } else {
            return redirect()->back()->with('error','Thêm dữ liệu không thành công. Vui lòng thử lại');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['service'] = $this->service->editService($id);

        return view('admin.modules.dichvu.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =array(
            'name'      => $request->name,
            'alias'     => (empty($request->alias))? str_slug($request->name,"-"): $request->alias,
            'link'      => $request->link,
            'content'   => $request->content,
            'position'  => $request->position,
            'language'  => $request->language,   
            'created_at'=> new DateTime
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/service';
                $image->move($des,$image_name);
                $data['image']=$image_name;
                $this->UpdateImgService($id);
            }
       $update = $this->service->updateService($data,$id);
       if ($update) {
            return redirect()->route('dichvu.index')->with("success",'message.update_required');
       } else {
            return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data); $i++) { 
            $this->service->delService($data[$i]);
        }

        return redirect()->route('dichvu.index')->with("success",'message.delete_required');
    }
}
