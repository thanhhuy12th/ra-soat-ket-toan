<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BaoGiaContact;
use DateTime;

class BaoGiaContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $baogiacontact;
    public function __construct(BaoGiaContact $baogiacontact)
    {
        $this->baogiacontact = $baogiacontact;
    }
    public function createBaoGiaContact($request)
    {
        $data = array(
            'name'          => $request->name,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'detail_baogia' => $request->detail_baogia,
            'content'       => $request->content,
            'created_at'    => new DateTime 
        );

        return $data;
    }
    public function index()
    {
        $data['baogiacontact'] = $this->baogiacontact->listBaoGiaContact();

        return view('admin.modules.baogiacontact.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['baogiacontact'] = $this->baogiacontact->editBaoGiaContact($id);

        return view('admin.modules.baogiacontact.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->createBaoGiaContact($request);

        $update = $this->baogiacontact->updateBaoGiaContact($data,$id);
        if ( $update) {
          return redirect()->route('baogiacontact.index')->with("success",'message.update_required');
       }else{
         return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data) ; $i++) { 
            $this->baogiacontact->delBaoGiaContact($data[$i]);
        }

        return redirect()->route('baogiacontact.index')->with("success",'message.delete_required');
    }
}
