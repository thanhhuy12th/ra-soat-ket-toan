<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\NhanXetRequest;
use App\Http\Controllers\Controller;
use App\Models\NhanXet;
use DateTime;

class NhanXetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $nhanxet;
    public function __construct(NhanXet $nhanxet)
    {
        $this->nhanxet = $nhanxet;
    }
    public function CapnhatImage($id)
    {
        $item = $this->nhanxet->editNhanXet($id);
        $filename = 'public/uploads/avatar'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $data['nhanxet'] = $this->nhanxet->listNhanXet();

        return view('admin.modules.nhanxet.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.nhanxet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NhanXetRequest $request)
    {
        $data  = array(
            'fullname'      => $request->fullname,
            'content'       => $request->content,
            'created_at'    => new DateTime 
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/avatar';
                $image->move($des,$image_name);
                $data['image']=$image_name;
            }
      $insert =  $this->nhanxet->AddNhanXet($data);
      if ($insert) {
          return redirect()->route('nhanxet.index')->with("success",'message.store_required');
      } else {
        return redirect()->back()->with('error','Thêm dữ liệu không thành công. Vui lòng thử lại');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['nhanxet'] = $this->nhanxet->editNhanXet($id);

        return view('admin.modules.nhanxet.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data  = array(
            'fullname'      => $request->fullname,
            'content'       => $request->content,
            'created_at'    => new DateTime 
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/avatar';
                $image->move($des,$image_name);
                $data['image']=$image_name;
                $this->CapnhatImage($id);
            }

        $update = $this->nhanxet->updateNhanXet($data,$id);
        if ($update) {
           return redirect()->route('nhanxet.index')->with("success",'message.update_required');
        }else{
            return redirect()->back()->with('error','Cập nhật dữ liệu không thành công. Vui lòng thử lại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data); $i++) { 
            $this->nhanxet->delNhanXet($data[$i]);
        }

        return redirect()->route('nhanxet.index')->with("success",'message.delete_required');
    }
}
