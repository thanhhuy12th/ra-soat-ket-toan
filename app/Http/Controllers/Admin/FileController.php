<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\FileRequest;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\CategoryFile;
use DateTime;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $file;
    private $categoryfile;
    public function __construct(File $file, CategoryFile $categoryfile)
    {
        $this->files = $file;
        $this->categoryfile = $categoryfile; 
    }
    public function UpdateFile($id)
    {
        $item = $this->files->editFile($id);
        $splittedstring=explode("||",$item->file);
        foreach ($splittedstring as $key) {
            $filename = 'public/uploads/file/'.$key;
            if (!is_null($splittedstring)) {
                 if(\File::exists($filename)){
                \File::delete($filename);
                }  
            }
        }
       
    }
    public function index()
    {
        $data['file']  = $this->files->listFile();
        $data['opt_parent'] = $this->categoryfile->getParent();

        return view('admin.modules.file.index',$data);
    }

    public function Linkfile()
    {

         $data['file']  = $this->files->listFile();
         $data['opt_parent'] = $this->categoryfile->getParent();
         // dd($data);
        return view('admin.modules.file.linkfile',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['cate_option'] = [];

        $option_parent = $this->categoryfile->listCateByIdParent(0); 
        foreach ($option_parent as $key => $value) {
          
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            
            array_push($data['cate_option'], $option_item);

            $option_child = $this->categoryfile->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
               
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
                $option_nho = $this->categoryfile->listCateByIdParent($valueC->id);

                foreach ($option_nho as $keyD => $valueD) {
               
                    $option_nho_item = ["id"=>$valueD->id,"name"=>"---- ".$valueD->name];
                    array_push($data['cate_option'], $option_nho_item);
                }
            }
        }

        return view('admin.modules.file.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileNameTotal = "";
        if ($request->hasFile('file')) {
            $rules = array("jpg","jpeg","png","doc","docx","pdf","xlsx","xls");
             foreach ($request->file('file') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.str_replace(" ","_",$file->getClientOriginalName());
                     $uploadPath = 'public/uploads/file';
                     $file->move($uploadPath, $fileName);

                     //array_push($fileNameTotal,$fileName);

                    $fileNameTotal .= $fileName."||";
             }

        }
        $data = array(
                        'id_category'           => $request->id_category,
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'type'                  => $request->type,
                        'file'                  => $fileNameTotal,
                        'keyword'               => $request->keyword,
                        'short_description'     => $request->short_description,
                        'description'           => $request->description,
                        'created_at'            => new DateTime
                    );
         $this->files->AddFile($data);
         return redirect()->route('file.index')->with("success",'message.store_required');

         } else {

           return redirect()->back()->with('error', 'Upload files thất bại!');
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['file'] = $this->files->editFile($id);
        
        $data['cate_option'] = [];

        $option_parent = $this->categoryfile->listCateByIdParent(0); 
        foreach ($option_parent as $key => $value) {
          
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            
            array_push($data['cate_option'], $option_item);

            $option_child = $this->categoryfile->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
               
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }

        return view('admin.modules.file.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fileNameTotal = "";
        if ($request->hasFile('file')) {
            $rules = array("jpg","jpeg","png","doc","docx","pdf","xlsx","xls");
             foreach ($request->file('file') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $fileName = time().'_'.str_replace(" ","_",$file->getClientOriginalName());
                     $uploadPath = 'public/uploads/file';
                     $file->move($uploadPath, $fileName);
                     
                     $this->UpdateFile($id);
                    $fileNameTotal .= $fileName."||";
                     //array_push($fileNameTotal,$fileName);
             }
        }
        $data = array(
                        'id_category'           => $request->id_category,
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'type'                  => $request->type,
                        'file'                  => $fileNameTotal,
                        'keyword'               => $request->keyword,
                        'short_description'     => $request->short_description,
                        'description'           => $request->description,
                        'created_at'            => new DateTime
                    );

         $this->files->updateFile($data,$id);
         return redirect()->route('file.index')->with("success",'message.update_required');

         } else {

           return redirect()->back()->with('error', 'Upload files thất bại!');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data); $i++) { 
            $this->files->delFile($data[$i]);
        }

        return redirect()->route('file.index')->with("success",'message.delete_required');
    }
}
