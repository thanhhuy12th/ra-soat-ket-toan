<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
use App\Models\Configuration;
use DateTime;
use DB;

class EmailController extends Controller
{

    private $mailer;
    public function __construct(PHPMailer $mailer, Configuration $configuration) {
        $this->configuration = $configuration;

        $this->mailer = $mailer;

        $this->mailer->IsSMTP();
        $this->mailer->Host         = "smtp.gmail.com";
        $this->mailer->SMTPAuth     = true;
        $this->mailer->Host         = "smtp.gmail.com";
        $this->mailer->Port         = 587;
        $this->mailer->SMTPSecure   = 'tls';
        $this->mailer->SMTPOptions  = array(
        'ssl' => array(
            'verify_peer'           => false,
            'verify_peer_name'      => false,
            'allow_self_signed'     => true
            )
        );
        $this->mailer->CharSet      = 'UTF-8';
        $this->mailer->Username     = $this->configuration->editConfig(17)->content;
        $this->mailer->Password     = $this->configuration->editConfig(18)->content;
        $this->mailer->AdminEmail   = $this->configuration->editConfig(10)->content;
    }

    public function send_mail_customer_baogia($email, $name, $detail) {
        $Title      = "[Thuế 3M]- No reply";
        $Subject    = "[Thuế 3M] - Gửi thông tin báo giá";
        $Content    = "Chào ".$name.", <p>Bạn vừa gửi thông tin báo giá thành công</p> <p>Chúng tôi sẽ liên hệ với bạn sớm nhất.</p> <p></p> <p>Trân trọng,</p><p>Công ty Kế Toán Thuế Ba Miền</p>";

        $current_lang = \Session::get('website_language');

        if($current_lang == "en") {
           $Title      = "[3M]- No reply";
           $Subject    = "[3M] - Sending Price Information";
           $Content    = "Hi ".$name.", <p>You You have successfully submitted your price information.</p> <p>We will contact you as soon as posible.</p> <p></p> <p>Best regards,</p><p>3M</p>";
        }

        $this->mailer->SetFrom($this->mailer->Username, $Title);
        $this->mailer->Subject      = $Subject;
        $this->mailer->MsgHTML($Content);
        $this->mailer->AddAddress($email, $Title);
        if(!$this->mailer->Send()) {
            return false;
        }
        else
        {

            return true;
        } 

    }

    public function send_mail_admin_baogia() {
        $Title      = "[Thuế 3M] - Có người vừa gửi báo giá";
        $Subject    = "[Thuế 3M] - Có người vừa gửi báo giá";
        $Content    = "Chào admin, <p>Hệ thống vừa nhận được báo giá từ trang chi tiết báo giá, bạn vui lòng check trang quản trị để xem thông tin.</p> <p></p> <p>Trân trọng,</p><p>Công ty Kế Toán Thuế Ba Miền</p>";
        $this->mailer->SetFrom($this->mailer->Username, $Title);
        $this->mailer->Subject      = $Subject;
        $this->mailer->MsgHTML($Content);
        $this->mailer->AddAddress($this->mailer->AdminEmail, $Title);
        if(!$this->mailer->Send()) {
            return false;
        }
        else
        {

            return true;
        } 

    }
}


