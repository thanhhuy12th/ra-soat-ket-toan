<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class LangController extends Controller
{
    public function ShareLangVN()
    {
        $data = [
                 "connect_us"         => "Kết nối chúng tôi",
                 "contact_us"         => "Liên hệ ngay với chúng tôi",
                 "subscribe"          => "Nhận thông tin mới nhất",
                 "ph_subscribe"       => "Nhập email để nhận tin",
                 "send"               => "Gửi",
                 "faq_send"           => "Gửi câu hỏi",
                 "history_report"     => "Lượng truy cập trang",
                 "history_online"     => "Đang truy cập",
                 "history_today"      => "Truy cập hôm nay",
                 "history_total"      => "Tổng truy cập",
                 "about_us"           => "Thông tin công ty",
                 "banner_text"        => "Welcome to Rà Soát Kế Toán",
                 "banner_contact"     => "Liên hệ với chúng tôi",
                 "banner_service"     => "Dịch vụ công ty",
                 "read_more"          => "Xem thêm",
                 "search"             => "Tìm kiếm",
                 "see_all_post"       => "XEM TẤT CẢ BÀI VIẾT",
                 "featured_article"   => "Bài viết nổi bật",
                 "btn_detail"         => "Chi tiết",
                 "btn_contact_us"     => "LIÊN HỆ NGAY",
                 "title_contact_us"   => "LIÊN HỆ",
                 "receive_contact_us" => "NHẬN THÔNG TIN",
                 "additional_advice"  => "Tư vấn thêm",
                 "client_said"        => "Nhận xét từ khách hàng",
                 "client_said_des"    => "Chúng tôi luôn học hỏi từ đối thủ, về mọi thứ có thể học hỏi được, nhưng chúng tôi không bao giờ đi sao chép. Với chúng tôi, sao chép có nghĩa là chết. Cạnh tranh cũng giống như chơi một ván cờ. Khi chúng ta thua, chúng ta có thể chơi lại một ván cờ khác. Cả hai người chơi đừng nên chiến đấu triệt hạ lẫn nhau",
                 "employee"           => "Nhân sự công ty",
                 "employee_des"       => "Giới thiệu về sơ lược nhân sự của công ty",
                 "fullname"           => "Họ và tên",
                 "phone"              => "Số điện thoại",
                 "message"            => "Lời nhắn",
                 "email"              => "Thư điện tử",
                 "adress"             => "Địa chỉ",
                 "ph_fullname"        => "Nhập họ và tên",
                 "ph_phone"           => "Nhập số điện thoại",
                 "ph_message"         => "Nhập lời nhắn",
                 "faq_message"        => "Đặt câu hỏi cho chúng tôi",
                 "ph_email"           => "Nhập thư điện tử",
                 "ph_adress"          => "Nhập địa chỉ",
                 "num_projects"       => "DỰ ÁN",
                 "num_customers"      => "KHÁCH HÀNG",
                 "num_employees"      => "NHÂN VIÊN",
                 "home"               => "TRANG CHỦ",
                 "introduce"          => "GIỚI THIỆU",
                 "service"            => "DỊCH VỤ",
                 "news"               => "TIN TỨC",
                 "share"              => "CHIA SẺ",
                 "contact"            => "LIÊN HỆ",
                 "pricelist"          => "BẢNG GIÁ",
                 "service_check"      => "KIỂM TRA",
                 "form"               => "Biểu mẫu",
                 "form_title"         => "Tiêu đề",
                 "form_document"      => "Mô tả",
                 "form_number"        => "Lượt tải xuống",
                 "form_downloads"     => "Tải về",
                 "document"           => "Tài liệu",
                 "work_time"          => "Giờ làm việc",
                 "price"              => "BẢNG GIÁ",
                 "price_stt"          => "STT",
                 "price_title"        => "Tiêu đề",
                 "price_describe"     => "Mô tả",
                 "price_unit"         => "Đơn giá",
                 "price_total"        => "Tổng kết",
                 "news_post"          => "Tin tức mới nhất",

                 "user_name1"         => "Bà Dương Thị Ngọc Lan",
                 "user_title1"        => "Microsoft's CEO",
                 "user_content1"      => "Hơn 30 năm kinh nghiệm về kế toán thuế. Thành viên sáng lập Công Ty kế toán – Thuế Ba Miền. Hội viên sáng lập Câu Lạc Bộ Đại Lý Thuế TP.HCM. Ủy Viên Ban Chấp Hành Hội Tư Vấn Thuế Việt Nam Nhiệm Kỳ II (2018-2023)",
                 "user_name2"         => "Ông Trịnh Hồng Khánh",
                 "user_title2"        => "Microsoft's CEO",
                 "user_content2"      => "Hơn 12 năm kinh nghiệm về kiểm toán, kế toán, thuế.
                                          Có chứng chỉ kiểm toán viên do Bộ Tài Chính cấp.
                                          Chứng chỉ hành nghề dịch vụ làm thủ tục về thuế do Tổng cục thuế cấp.
                                          Có chứng chỉ Quản tài viên do Bộ Tư Pháp cấp.
                                          Có chứng chỉ kế toán trưởng",
                 "user_name3"         => "Ông Mai Thanh Nhựt",
                 "user_title3"        => "CEO",
                 "user_content3"      => "  Là Giám đốc điều hành Công ty.
                                            Hơn 15 năm kinh nghiệm về kế toán, kiểm toán.
                                            Có chứng chỉ kế toán viên do Bộ Tài Chính cấp.
                                            Chứng chỉ hành nghề dịch vụ làm thủ tục về thuế do Tổng cục thuế cấp",
                 "user_name4"         => "Bà Nguyễn Thị Thanh Châu",
                 "user_title4"        => "Microsoft's CEO",
                 "user_content4"      => "  Hơn 12 năm kinh nghiệm về kiểm toán, kế toán, thuế.
                                            Có chứng chỉ kiểm toán viên do Bộ Tài Chính cấp.
                                            Chứng chỉ hành nghề dịch vụ làm thủ tục về thuế do Tổng cục thuế cấp.",


                 "caregory_news"      => "Danh mục tin tức",

                 // Dich vu
                 "3m_prestige_and_service_quality"      => "BA MIỀN - UY TÍN VÀ CHẤT LƯỢNG",
                 "accounting_consulting"                => "TƯ VẤN KẾ TOÁN",
                 "intro_accounting_consulting"          => "Hỗ trợ phương pháp lưu trữ và truy xuất thông tin dữ liệu nếu doanh nghiệp cần, sắp xếp có hệ thống và bàn giao lại cho doanh nghiệp.",
                 "tax_consulting_and_planning"          => "TƯ VẤN VÀ LẬP KẾ HOẠCH THUẾ",
                 "intro_tax_consulting_and_planning"    => "Với hơn 5 năm kinh nghiệm, chúng tôi sẽ tư vấn chi tiết cho khách hàng về hệ thống kế toán, tài chính và quản trị rủi ro cho từng loại hình doanh nghiệp.",
                 "tax_declaration"                      => "KÊ KHAI THUẾ",
                 "intro_tax_declaration"                => "Thực hiện việc kê khai, báo cáo thuế hàng tháng, quý, báo cáo thu nhập tạm tính và tình hình sử dụng tạm tính, sau đó nạp đến cơ quan thuế theo đúng quy định.",
                 "annual_settlement"                    => "QUYẾT TOÁN NĂM",
                 "intro_annual_settlement"              => "Dựa vào hóa đơn, chứng từ hợp lệ trong năm, chúng tôi sẽ thực hiện định khoản, cân đối doanh thu, chi phí sao cho hợp lý để làm báo cáo tài chính, quyết toán năm.",
                 "other_tax_services"                   => "HỖ TRỢ KHÁC VỀ THUẾ",
                 "intro_other_tax_services"             => "Cảnh báo những rủi ro tiềm ẩn trong doanh nghiệp và hướng xử lý hiệu quả. Hỗ trợ làm việc với cơ quan thuế khi cần",
                 "management_consulting"                => "TƯ VẤN QUẢN LÝ",
                 "intro_management_consulting"          => "Hỗ trợ và tư vấn làm các thủ tục hành chính, xây dựng chiến lược nhân sự, xây dựng hệ thống lương. Đăng ký lao động và làm hồ sơ BHXH hàng tháng",
                 // uu diem
                 
                 "our_advantages"                       => "ƯU ĐIỂM CỦA CHÚNG TÔI",
                 "prestige_and_service_quality"         => "UY TÍN VÀ CHẤT LƯỢNG HÀNG ĐẦU",
                 "intro_prestige_and_service_quality"   => "Với thông điệp “Uy tín và chất lượng”, chúng tôi luôn tâm niệm sự hài lòng của khách hàng là thước đo thành công và là yếu tố quyết định sự phát triển của công ty.",
                 "scrupulously_accurate"                => "CHÍNH XÁC TRÊN TỪNG CON SỐ",
                 "intro_scrupulously_accurate"          => "Chúng tôi cam kết đem lại cho khách hàng những con số tuyệt đối chính xác trong các báo cáo tài chính, kê khai thuế…",
                 "close_procedure"                      => "QUY TRÌNH CHẶT CHẼ",
                 "intro_close_procedure"                => "Nhằm hạn chế sai sót trong quá trình thực hiện dịch vụ, từng nội dung công việc luôn được kiểm tra chéo và kiểm soát bởi các nhân viên cấp cao.",
                 "absolute_security"                    => "BẢO MẬT TUYỆT ĐỐI",
                 "intro_absolute_security"              => "Tính bảo mật được thể hiện ở các điều khoản Hợp đồng bảo mật được ký kết. Toàn bộ các thông tin doanh nghiệp cung cấp trong quá trình làm việc được bảo mật một cách tuyệt đối.",
                 "cost_saving"                          => "TIẾT KIỆM ĐỐI ĐA CHI PHÍ",
                 "intro_cost_saving"                    => "Hỗ trợ khách hàng tối ưu hóa chi phí đóng thuế mà vẫn đảm bảo tuân thủ đúng qui định của luật Thuế của nhà nước và giảm thiểu những rủi ro không mong muốn.",
                 "free_consultation"                    => "TƯ VẤN MIỄN PHÍ",
                 "intro_free_consultation"              => "Chúng tôi sẽ tư vấn miễn phí cho quý khách hàng các văn bản pháp luật mới ban hành, tư vấn trả lời các vấn đề liên quan đến hoạt động kinh doanh, chế độ, chính sách kế toán, tài chính, thuế.",
                 "human_resources"                      => "NGUỒN NHÂN LỰC TRẺ",
                 "intro_human_resources"                => "Đội ngũ nhân viên với kiến thức chuyên môn cao, nghiệp vụ vững vàng được tuyển chọn và đào tạo kĩ lưỡng.",       
                 "aboutus_title"                        => "Giới thiệu về công ty",
                 
                 "Search_suggestions" => "Gợi ý tìm kiếm",
                 "pit"                => "Thuế TNCN",
                 "cit"                => "Thuế TNDN",
                 "invoices"           => "Hóa đơn",
                 "fct"                => "Nhà thầu",
                 "accouting"          => "Kế toán",
                 "auditing"           => "Kiểm toán",
                 "social_insuarance"  => "BHXH",
                 "labour"             => "Lao động",
                 "salary"             => "Tiền lương",
                 "order"              => "Khác",

        ];
        return $data;
    }
    public function ShareLangUS()
    {
        $data = [
                 "connect_us"         => "Connect with us",
                 "contact_us"         => "Contact us now",
                 "subscribe"          => "Receive latest news",
                 "ph_subscribe"       => "Type your email",
                 "send"               => "Send",
                 "faq_send"           => "Send question",
                 "history_report"     => "History report",
                 "history_online"     => "Online",
                 "history_today"      => "Today",
                 "history_total"      => "Total",
                 "about_us"           => "About us",
                 "banner_text"        => "Welcome to Rà Soát Kế Toán",
                 "banner_contact"     => "Contact us",
                 "banner_service"     => "Our aservice",
                 "read_more"          => "Read more",
                 "search"             => "Search",
                 "see_all_post"       => "SEE ALL BLOG POSTS",
                 "featured_article"   => "Latest article",
                 "btn_detail"         => "Detail",
                 "btn_contact_us"     => "CONTACT",
                 "title_contact_us"   => "CONTACT",
                 "receive_contact_us" => "RECEIVE INFORMATION",
                 "additional_advice"  => "Additional Advice",
                 "client_said"        => "What Our Clients Say",
                 "client_said_des"    => "We always learn from our opponents, about everything we can learn, but we never copy. For us, copying means death. Competition is like playing a game of chess. When we lose, we can play another match. Both players should not fight against each other",
                 "employee"           => "Our staff",
                 "employee_des"       => "Introduced about our staff",
                 "user_name"          => "User",
                 "user_title"         => "Title",
                 "user_content"       => "Introduce the user described here. Introduce the user described here. Introduce the user described here",
                 "fullname"           => "Full name",
                 "phone"              => "Phone",
                 "message"            => "Message",
                 "email"              => "Email",
                 "adress"             => "Adress",
                 "ph_fullname"        => "Enter your full name",
                 "ph_phone"           => "Enter your phone",
                 "ph_message"         => "Enter your message",
                 "faq_message"        => "Ask us questions",
                 "ph_email"           => "Enter your email",
                 "ph_adress"          => "Enter your adress",
                 "num_projects"       => "PROJECTS",
                 "num_customers"      => "CUSTOMERS",
                 "num_employees"      => "EMPLOYEES",
                 "home"               => "HOME",
                 "introduce"          => "INTRODUCE",
                 "service"            => "SERVICE",
                 "news"               => "NEWS",
                 "share"              => "SHARE",
                 "contact"            => "CONTACT",
                 "pricelist"          => "PRICE LIST",
                 "service_check"      => "CHECK IT OUT",
                 "form"               => "Form",
                 "form_title"         => "Title",
                 "form_document"      => "Description",
                 "form_number"        => "Number of downloads",
                 "form_downloads"     => "Downloads",
                 "document"           => "Documents",
                 "work_time"          => "Work time",
                 "price"              => "PRICE LIST",
                 "price_stt"          => "ID",
                 "price_title"        => "Title",
                 "price_describe"     => "Describe",
                 "price_unit"         => "Unit price",
                 "price_total"        => "Total",
                 "news_post"          => "News post",

                 "user_name1"         => "Mrs Duong Thi Ngoc Lan",
                 "user_title1"        => "Microsoft's CEO",
                 "user_content1"      => "  More than 30 year-experience in accounting.    
                                            Founder of 3M tax & accounting company.
                                            Founder of tax agent club.   
                                            Member of the Party Vietnam Tax Consultant Association term II (2018-2023).",
                 "user_name2"         => "Mr Trinh Hong Khanh",
                 "user_title2"        => "Microsoft's CEO",
                 "user_content2"      => "  More than 12 year-experience in auditing, accounting, tax.
                                            An auditor is certified by the Ministry of Finance.
                                            Has practising cerificate on tax services certified by General Tax Department.
                                            Has...certified by Ministry of Justice.
                                            Chief accountant certified by ...",
                 "user_name3"         => "Mr Mai Thanh Nhut",
                 "user_title3"        => "CEO",
                 "user_content3"      => "  More than 15 year-experience in auditing, accounting.  
                                            Has accounting certified by Ministry of Finance. 
                                            Has practising cerificate on tax services certified by General Tax Department",
                 "user_name4"         => "Ms Nguyen Thi Thanh Chau",
                 "user_title4"        => "Microsoft's CEO",
                 "user_content4"      => "  More than 12 year-experience in auditing, accounting, tax.   
                                            An auditor is certified by the Ministry of Finance.  
                                            Has practising cerificate on tax services certified by General Tax Department.",


                 "caregory_news"      => "News category",
                 // Dich vu
                 "3m_prestige_and_service_quality"      => "3M- PRESTIGE AND SERVICE QUALITY",
                 "accounting_consulting"                => "ACCOUNTING CONSULTING",
                 "intro_accounting_consulting"          => "Support the method of storing and retrieving data in case of need, systematically arranged and handed over to the enterprises.",
                 "tax_consulting_and_planning"          => "TAX CONSULTING AND PLANNING",
                 "intro_tax_consulting_and_planning"    => "With more than 5 years of experience, we will advise clients on accounting, finance and risk management systems for each type of business in details.",
                 "tax_declaration"                      => "TAX DECLARATION",
                 "intro_tax_declaration"                => "To make monthly and quarterly tax declaration reports on temporary income and the situation of temporary use and then submit to the tax offices as prescribed.",
                 "annual_settlement"                    => "ANNUAL SETTLEMENT",
                 "intro_annual_settlement"              => "Based on the valid invoices and documents in the year, we will make the balance sheet, balancing the revenue and expenses so as to make the financial statement and annual settlement.",
                 "other_tax_services"                   => "OTHER TAX SERVICES",
                 "intro_other_tax_services"             => "Warning of potential risks in the business and the direction of effective handling.Supporting to work with tax authorities in case of need",
                 "management_consulting"                => "MANAGEMENT CONSULTING",
                 "intro_management_consulting"          => "Support and advise on administrative procedures, building human resources strategy, building salary system.Labor registration and monthly social insurance application.",
                 // uu diem
                 "our_advantages"                       => "OUR ADVANTAGES",
                 "prestige_and_service_quality"         => "PRESTIGE AND SERVICE QUALITY",
                 "intro_prestige_and_service_quality"   => "With the message Prestige and Quality, we always consider customers’ satisfaction as a measure of success and a decisive factor in the development of our company.",
                 "scrupulously_accurate"                => "SCRUPULOUSLY ACCURATE",
                 "intro_scrupulously_accurate"          => "We are committed to providing customers with accurate numbers in financial reports, tax returns...",
                 "close_procedure"                      => "CLOSE PROCEDURE",
                 "intro_close_procedure"                => "In order to minimize errors in the implementation of services, each job content is always crosschecked and controlled by senior staff.",
                 "absolute_security"                    => "ABSOLUTE SECURITY",
                 "intro_absolute_security"              => "Confidentiality is reflected in the terms of the signed contract. All business information provided during the work process is absolutely confidential.",
                 "cost_saving"                          => "COST SAVING",
                 "intro_cost_saving"                    => "Optimizing your taxpayer expense while still ensuring compliance with the Tax laws and minimizing unwanted risks.",
                 "free_consultation"                    => "FREE CONSULTATION",
                 "intro_free_consultation"              => "We will give you free advice of new legal documents, on business issues, regimes, accounting policies, finance, and tax.",
                 "human_resources"                      => "HUMAN RESOURCES",
                 "intro_human_resources"                => "Staffs with high professional knowledge, professional skills are selected and trained carefully.",
                 "aboutus_title"        => "About us",

                 "Search_suggestions" => "Search suggestions",
                 "pit"                => "PIT",
                 "cit"                => "CIT",
                 "invoices"           => "Invoices",
                 "fct"                => "FCT",
                 "accouting"          => "Accouting",
                 "auditing"           => "Auditing",
                 "social_insuarance"  => "Social Insuarance",
                 "labour"             => "Labour",
                 "salary"             => "Salary",
                 "order"              => "Order",
        ];
        return $data;
    }
}
