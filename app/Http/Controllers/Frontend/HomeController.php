<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Models\Lienhe;
use App\Models\FAQContact;
use App\Models\SendMail;
use App\Models\FAQFile;
use App\Models\CategoryFile;
use App\Models\File;
use App\Models\BaoGiaContact;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use DB,DateTime;

class HomeController extends Controller
{
    private $lienhe;
    private $faqcontact;
    private $sendmail;
    private $faqfile;
    private $baogiacontact;
    private $categoryfile;
    private $file;
    public function __construct(Lienhe $lienhe, FAQContact $faqcontact, SendMail $sendmail, FAQFile $faqfile, CategoryFile $categoryfile, File $file, BaoGiaContact $baogiacontact)
    {
        $this->lienhe       = $lienhe;
        $this->faqcontact   = $faqcontact;
        $this->sendmail     = $sendmail;
        $this->faqfile      = $faqfile;
        $this->categoryfile = $categoryfile;
        $this->file         = $file;
        $this->baogiacontact = $baogiacontact;
    }
    public function changeLanguage($language)
    {
        \Session::put('website_language', $language);

        return redirect()->back();
    }
    public function index()
    {

        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $data['news'] = DB::table('c1_tintuc')->where('status',1)->where('language','EN')->orderBy('id','ASC')->limit(3)->get()->toArray();
            $data['service'] = DB::table('c1_dichvu')->where('language','EN')->orderBy('position','ASC')->limit(3)->get()->toArray();
        }
        else {
            $data['news'] = DB::table('c1_tintuc')->where('status',1)->where('language','VN')->orderBy('id','ASC')->limit(3)->get()->toArray();
            $data['service'] = DB::table('c1_dichvu')->where('language','VN')->orderBy('position','ASC')->limit(3)->get()->toArray();
        }

        $data['nhanxet'] = DB::table('c1_nhanxet')->take(3)->get()->toArray();
       
    	return view('frontend.pages.home',$data);
    }
    public function Introduce()
    {
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
           $data['gioithieu'] = DB::table('c1_config')->whereId(20)->first();

        }
        else {
            $data['gioithieu'] = DB::table('c1_config')->whereId(1)->first();
        }
    	return view('frontend.pages.gioithieu',$data);
    }
    public function Service()
    {
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $data['service'] = DB::table('c1_dichvu')->where('language','EN')->orderBy('position','ASC')->limit(3)->get()->toArray();

        }
        else {
            $data['service'] = DB::table('c1_dichvu')->where('language','VN')->orderBy('position','ASC')->limit(3)->get()->toArray();
        }
    	return view('frontend.pages.dichvu',$data);
    }
    public function News()
    {
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $data['news'] = DB::table('c1_tintuc')->where('language','EN')->orderBy('id','DESC')->limit(10)->paginate(8);
            $data['dmtt'] = DB::table('c1_dmtt')->orderBy('id','ASC')->take(6)->get()->toArray();
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','EN')->orderBy('id','DESC')->take(6)->get()->toArray();

        }
        else {
            $data['news'] = DB::table('c1_tintuc')->where('language','VN')->orderBy('id','DESC')->limit(10)->paginate(8);
            $data['dmtt'] = DB::table('c1_dmtt')->orderBy('id','ASC')->take(6)->get()->toArray();
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','VN')->orderBy('id','DESC')->take(6)->get()->toArray();
        }
    	return view('frontend.pages.tintuc',$data);
    }
    public function Newsdetail($alias)
    {
    	$data['chitiettin'] = DB::table('c1_tintuc')->where('alias',$alias)->first();
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $data['dmtt'] = DB::table('c1_dmtt')->where('language','EN')->orderBy('id','DESC')->take(6)->get()->toArray();
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','EN')->orderBy('id','DESC')->take(6)->get()->toArray();

        }
        else {
            $data['dmtt'] = DB::table('c1_dmtt')->where('language','VN')->orderBy('id','DESC')->take(6)->get()->toArray();
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','VN')->orderBy('id','DESC')->take(6)->get()->toArray();
        }

    	return view('frontend.pages.tintuc_detail',$data);
    }
    public function DanhmucNews($id)
    {
        $data['news'] = DB::table('c1_tintuc')->orderBy('id','ASC')->limit(10)->paginate(8);
        $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
        $data['dmtt'] = DB::table('c1_dmtt')->orderBy('id','ASC')->take(6)->get()->toArray();
        // $data['news'] = DB::table('c1_tintuc')
        //                             ->where('parent_id',$id)
        //                             ->orderBy('id','DESC')
        //                             ->limit(10)
        //                             ->paginate(8);

        return view('frontend.pages.tintuc',$data);
    }
    public function Lienhe()
    {
        $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
    	return view('frontend.pages.lienhe',$data);
    }
    public function PostLienhe(Request $request)
    {
        $captcha = null;
        if(isset($_POST['g-recaptcha-response'])){
            $captcha=$_POST['g-recaptcha-response'];
        }
        if(!$captcha){
          echo "<script>
               alert('Chưa nhập mã xác thực!');
               window.location = '".url('/')."'
            </script>";
        }
        else {
            $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ldsp60UAAAAAI8t6ApSI_w4kR5g7VBy2dmZuzj8&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
            $obj =json_decode($response); 
            if($obj->{'success'}==true)            
            {
              $data = array(
                'name'      => $request->name,
                'email'     => $request->email,
                'phone'     => $request->phone,
                'address'   => $request->address,
                'content'   => $request->content,
                'created_at'=> new DateTime 
            );
              $this->lienhe->AddLienhe($data);

                echo "<script>
                alert('Cảm ơn bạn đã liên hệ. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất');
                window.location = '".url('/')."'
                </script>";
            }
            else {
                echo "<script>
                alert('Sai mã xác thực, vui lòng nhập lại');
                window.location = '".url('/')."'
                </script>";

            }
        }
        
    }
    public function Bieumau()
    {
        $data['dm'] = DB::table('c1_files_category')->orderBy('id','ASC')->take(6)->get()->toArray();
        $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
        $data['bieumau'] = DB::table('c1_files')->where('type',1)->limit(10)->paginate(8);

        return view('frontend.pages.bieumau',$data);
    }
    public function Bieumaudetails($alias)
    {
        $data['bieumaudetail'] = DB::table('c1_files')->where('alias',$alias)->first();
        
        $data['dmtt'] = DB::table('c1_dmtt')->orderBy('id','ASC')->take(6)->get()->toArray();
        $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();

        return view('frontend.pages.bieumau_detail',$data);
    }
    public function TaiLieu()
    {
         $data['dm'] = DB::table('c1_files_category')->get()->toArray();
         $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
         $data['tailieu'] = DB::table('c1_files')->where('type',2)->limit(10)->paginate(8);
         $data['files'] = DB::table('c1_files')->where('type',2)->get()->toArray();
        return view('frontend.pages.tailieu',$data);
    }
    public function FAQ()
    {  
        $data['faqfile'] = DB::table('c1_faq_file')->get()->toArray();
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $data['faq'] = DB::table('c1_faq')->where('language','EN')->limit(8)->paginate(8);
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','EN')->orderBy('id','DESC')->take(6)->get()->toArray();

        }
        else {
            $data['faq'] = DB::table('c1_faq')->where('language','VN')->limit(8)->paginate(8);
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','VN')->orderBy('id','DESC')->take(6)->get()->toArray();
        }

        return view('frontend.pages.hoidap',$data);
    }
    public function CateDanhmucBieumau($id)
    {
        $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
        $data['danhmuc_category_bieumau'] = DB::table('c1_files')
                                    ->where('id_category',$id)
                                    ->where('type',1)
                                    ->orderBy('id','DESC')
                                    ->limit(10)
                                    ->paginate(6);
        return view('frontend.pages.danhmuc_category_bieumau',$data);
    }
    public function CateDanhmucTaiLieu($id)
    {
        $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
        $data['danhmuc_category_tailieu'] = DB::table('c1_files')
                                    ->where('id_category',$id)
                                    ->where('type',2)
                                    ->orderBy('id','DESC')
                                    ->limit(10)
                                    ->paginate(6);

        return view('frontend.pages.danhmuc_category_tailieu',$data);
    }
    public function timkiem_hoidap(Request $request)
    {
       
        $tukhoa = $request->tukhoa;
        $faq  = DB::table('c1_faq')->where('title', 'like', "%$tukhoa%")->orWhere('content','like',"%$tukhoa%")->take(10)->paginate(6);
        $faqfile= DB::table('c1_faq_file')->get()->toArray();

        return view('frontend.pages.timkiem_hoidap',[
                'tukhoa' => $tukhoa,
                'faq'   => $faq,
                'faqfile' => $faqfile
        ]);
    }
    public function timkiem_bieumau(Request $request)
    {
        $tukhoa = $request->tukhoa;
        $bieumau  = DB::table('c1_files')->where('type',1)->where('title', 'like', "%$tukhoa%")->orWhere('short_description','like',"%$tukhoa%")->take(10)->paginate(6);

        return view('frontend.pages.timkiem_bieumau',[
                'tukhoa' => $tukhoa,
                'bieumau'   => $bieumau
        ]);
    }

    public function timkiem(Request $request)
    {
        $tukhoa = $request->tukhoa;
        $tintuc  = DB::table('c1_tintuc')->where('title', 'like', "%$tukhoa%")->orWhere('intro','like',"%$tukhoa%")->take(30)->paginate(6);
        $hot = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
        

        return view('frontend.pages.timkiem',
            [
                'tintuc' => $tintuc,
                'tukhoa' => $tukhoa,
                'hot'   => $hot
                
            ]
        );
    }

    public function BangGia()
    {
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','EN')->orderBy('id','DESC')->take(6)->get()->toArray();

        }
        else {
            $data['hot'] = DB::table('c1_tintuc')->where('status',1)->where('language','VN')->orderBy('id','DESC')->take(6)->get()->toArray();
        }

        $data['banggia'] = DB::table('c1_baogia')->limit(10)->paginate(8);
        
        return view('frontend.pages.banggia',$data);
    }
    public function Banggiadetail($alias)
    {
        $data['hot'] = DB::table('c1_tintuc')->where('status',1)->orderBy('id','DESC')->take(6)->get()->toArray();
        $data['banggia'] = DB::table('c1_baogia')->first();
        //dd( $data['banggia']);
        
        
        $data['baogia'] = DB::table('c1_baogia_chitiet')
                                    ->select("c1_baogia_chitiet.id as id_detail","c1_baogia_chitiet.*","c1_baogia.*")
                                    ->join('c1_baogia', 'c1_baogia.id', '=', 'c1_baogia_chitiet.id_baogia')
                                    ->where('alias',$alias)
                                    ->get()
                                    ->toArray();

        return view('frontend.pages.banggia_detail',$data);
    }
    public function postBaoGiaContact(Request $request)
    {
        $data = array(
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'content'   => $request->content,
            'created_at'=> new DateTime 
        );
        $this->baogiacontact->AddBaoGiaContact($data);
        // echo "<script>
        //            alert('Cảm ơn bạn đã gửi báo giá. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất');
        //            window.location = '".url('bang-gia')."'
        //      </script>";
    }
  
    public function postFaqContact(Request $request)
    {
        $baseurl                 = DB::table('c1_config')->whereId(14)->first()->content;
        $tenwebsite              = DB::table('c1_config')->whereId(13)->first()->content;
        $email_receive_quantri   = DB::table('c1_config')->whereId(10)->first()->content;
        $email_receive_khachhang = $request->email;
        $phone                   = $request->phone;
        $content                 = $request->content;
        if($email_receive_khachhang != "" || $phone != "" || $content != "" )
        {
            $current_lang = \Session::get('website_language');
            $data = array(
            'email'     => $email_receive_khachhang,
            'phone'     => $phone,
            'content'   => $content,
            'created_at'=> new DateTime 
            );
            $bodyQuanTri     = $this->sendmail->bodyQuanTri($baseurl);
            $bodyKhachHangVN = $this->sendmail->bodyKhachHangVN($tenwebsite);
            $bodyKhachHangEN = $this->sendmail->bodyKhachHangEN($tenwebsite);
            $subject         = "FAQ Contact";            
            try {
                $this->faqcontact->AddFAQContact($data);
                $this->sendmail->sendMail($tenwebsite,$email_receive_quantri,$subject,$bodyQuanTri)->send();
                if($current_lang == "en") {
                    $this->sendmail->sendMail($tenwebsite,$email_receive_khachhang,$subject,$bodyKhachHangEN)->send();
                }
                else {
                    $this->sendmail->sendMail($tenwebsite,$email_receive_khachhang,$subject,$bodyKhachHangVN)->send();
                }
                echo "<script>
                   alert('Cảm ơn bạn đã liên hệ. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất');
                   window.location = '".url('/')."'
                </script>";
            } catch (Exception $e) {
                if($current_lang == "en") {
                    echo "<script>
                   alert('Send failed please try again.');
                   window.location = '".url('hoi-dap')."'
                </script>";
                }
                else {
                    echo "<script>
                       alert('Gửi thất bại bạn vui lòng thử lại.');
                       window.location = '".url('hoi-dap')."'
                    </script>";
                }
                
            }
        }
        else{
            echo "<script>
                   alert('Bạn vui lòng nhập đầy đủ thông tin.');
                   window.location = '".url('hoi-dap')."'
             </script>";

        }
        
    }
}
