<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PriceContact;
use App\Http\Controllers\EmailController;

use DB,DateTime;

class ApiController extends Controller
{

    private $priceContact;
    private $mailer;

    public function __construct(PriceContact $priceContact, EmailController $mailer)
    {
        $this->priceContact     = $priceContact;
        $this->mailer           = $mailer;
    }
    public function change_status_tintuc(Request $request)
    {
        $id = $request->id;
        $status = $request->status =='true' ? 1 : 0;
        //$msg = $request->status;
        $data = DB::table('c1_tintuc')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('c1_tintuc')->where('id',$id)->update(['status'=>$status]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }

    public function send_baogia(Request $request) {
        $Name       = $request->Name;
        $Email      = $request->Email;
        $Phone      = $request->Phone;
        $Message    = $request->Message;
        $Detail     = $request->Detail;
        try {
            if($Name != "" || $Email != "" || $Phone != "" ){
               // Luu vào databae
                $data = array(
                    'name'              => $Name,
                    'email'             => $Email,
                    'phone'             => $Phone,
                    'content'           => $Message,
                    'detail_baogia'     => $Detail,
                    'created_at'        => new DateTime 
                );

                $this->priceContact->AddPriceContact($data);
                // Send mail thông báo cho Admin
                $res_admin = $this->mailer->send_mail_admin_baogia();
                if($res_admin) {
                    // Send mail thông báo cho KH
                    $res_customer = $this->mailer->send_mail_customer_baogia($Email, $Name, $Detail);
                    if($res_customer) {
                        // /Trả kết quả về
                        echo 200; 
                    }
                }   
            }
            else {
                echo 400;
            }

        }
        catch (Exception $e) {
            echo 500;
        }
        

    }
}
