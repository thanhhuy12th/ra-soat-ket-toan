<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:50',
            'language'  => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.max'              => trans('message.name_max'),
            'language.required'     => trans('message.language_required'),

        ];
    }
}
