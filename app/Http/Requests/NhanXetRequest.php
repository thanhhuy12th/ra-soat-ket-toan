<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NhanXetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'  => 'required|max:50|unique:c1_nhanxet,fullname',
            'content'   => 'required',
            'image'     => 'required|mimes:jpeg,jpg,png,gif,tiff,bmp,svg|max:5000',
        ];
    }
    public function messages()
    {
        return [
            'fullname.required'         => trans('message.fullname_required'),
            'fullname.max'              => trans('message.fullname_max'),
            'fullname.unique'           => trans('message.fullname_unique'),
            'content.required'          => trans('message.content_required'),
            'image.required'            => trans('message.image_required'),
            'image.mimes'               => trans('message.image_mimes'),
            'image.max'                 => trans('message.image_max'),
        ];
    }
}
