<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required',
            'intro'     => 'required|max:255',
            'content'   => 'required',
            'image'     => 'required|mimes:jpeg,jpg,png,gif,tiff,bmp,svg|max:5000',
            'language'  => 'required',
            'status'     => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required'            => trans('message.title_required'),
            'intro.required'            => trans('message.intro_required'),
            'intro.max'                 => trans('message.intro_max'),
            'content.required'          => trans('message.content_required'),
            'image.required'            => trans('message.image_required'),
            'image.mimes'               => trans('message.image_mimes'),
            'image.max'                 => trans('message.image_max'),
            'status.required'            => trans('message.status_required'),
        ];
    }
}
