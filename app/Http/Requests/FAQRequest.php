<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FAQRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|max:255',
            'content'       => 'required',
            'position'      => 'required',
            'keyword'       => 'required',
            'language'      => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required'            => trans('message.title_required'),
            'title.max'                 => trans('message.title_max'),
            'content.required'          => trans('message.content_required'),
            'keyword.required'          => trans('message.keyword_required'),
            'position.required'         => trans('message.position_required'),
            'language.required'         => trans('message.language_required')
        ];
    }
}
