<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FAQContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'             => 'required',
            'phone'             => 'required',
            'type'              => 'required',
            'keyword'           => 'required',
            'file'              => 'required|max:10485760|mimes:jpg,jpeg,png,doc,docx,pdf,xlsx',

        ];
    }
    public function messages()
    {
        return [
            'title.required'            => trans('message.title_required'),
            'id_category.required'      => trans('message.id_category_required'),
            'type.required'             => trans('message.type_required'),
            'keyword.required'          => trans('message.keyword_required'),
            'file.required'             => trans('message.file_required'),
            'file.max'                  => trans('message.file_max'),
            'file.mimes'                => trans('message.file_mimes'),
        ];
    }
}
