<?php

namespace App\Http\View\Composers;

//use Illuminate\View\View;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\LangController;
use DB;

class HeadComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    public function __construct(LangController $lang)
    {
        $this->lang = $lang;
        
        
    }
    public function compose()
    {

        $this->data_lang = $this->lang->ShareLangVN();
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $this->data_lang = $this->lang->ShareLangUS();
        }
        else {
            $this->data_lang = $this->lang->ShareLangVN();
        }
        View::share('lang', $this->data_lang);

    }
}