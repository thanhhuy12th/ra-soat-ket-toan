<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
// use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\LangController;
use DB;

class MenuComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    public function changeLanguage($language)
    {
        \Session::put('website_language', $language);

        return redirect()->back();
    }
    public function compose(View $view)
    {
        
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $menu = DB::table('c1_menu')->where('parent_id',0)->where('language','EN')->orderBy('position','ASC')->get()->toArray();
        }
        else {
            $menu = DB::table('c1_menu')->where('parent_id',0)->where('language','VN')->orderBy('position','ASC')->get()->toArray();
        }
        
        $view->with('menu',$menu);

         $logo = DB::table('c1_config')->whereId(2)->first();
         $view->with('logo',$logo);
    }
}