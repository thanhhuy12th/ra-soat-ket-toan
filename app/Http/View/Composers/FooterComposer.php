<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Http\Controllers\LangController;
use DB;

class FooterComposer  
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    public function __construct(LangController $lang)
    {
        $this->lang = $lang;
    }
     public function compose(View $view)
    {

      

        $address = DB::table('c1_config')->whereId(15)->first();
        $view->with('address', $address);

        $hotline1 = DB::table('c1_config')->whereId(6)->first();
        $view->with('hotline1', $hotline1);

        $phonename1 = DB::table('c1_config')->whereId(6)->first();
        $view->with('phonename1',$phonename1);

        $email = DB::table('c1_config')->whereId(10)->first();
        $view->with('email', $email);

        $link = DB::table('c1_config')->whereId(14)->first();
        $view->with('link', $link);

        $map = DB::table('c1_config')->whereId(5)->first();
        $view->with('map', $map);


        
    }

}