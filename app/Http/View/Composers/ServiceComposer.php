<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
// use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\LangController;
use DB;

class ServiceComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    public function changeLanguage($language)
    {
        \Session::put('website_language', $language);

        return redirect()->back();
    }
    public function compose(View $view)
    {
        
        $current_lang = \Session::get('website_language');
        if($current_lang == "en") {
            $service = DB::table('c1_dichvu')->where('language','EN')->get()->toArray();
        }
        else {
            $service = DB::table('c1_dichvu')->where('language','VN')->get()->toArray();
        }
        
        $view->with('service',$service);
    }
}