<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;

class SliderComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        $slider = DB::table('c1_slider')->orderBy('image', 'ASC')->get()->toArray();
        $view->with('slider', $slider);
    }

}