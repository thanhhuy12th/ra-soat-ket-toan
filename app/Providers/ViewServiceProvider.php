<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            [
                'frontend.blocks.head',
                'frontend.pages.home',
                'frontend.pages.lienhe',
                'frontend.pages.banggia',
                'frontend.pages.banner',
                'frontend.pages.bieumau',
                'frontend.pages.bieumau_detail',
                'frontend.pages.dichvu',
                'frontend.pages.gioithieu',
                'frontend.pages.hoidap',
                'frontend.pages.bieumau',
                'frontend.pages.tailieu',
                'frontend.pages.timkiem',
                'frontend.pages.tintuc',
                'frontend.pages.tintuc_detail',
                'frontend.pages.danhmuc_category_bieumau',
                'frontend.pages.danhmuc_category_tailieu',
                'frontend.pages.timkiem_bieumau',
            ],
                  'App\Http\View\Composers\HeadComposer'    
        );
        View::composer(
            [
                'frontend.pages.banner_home',
                'frontend.pages.banner',
            ],
                  'App\Http\View\Composers\SliderComposer'    
        );
        View::composer(
            [
                'frontend.pages.home',
                'frontend.pages.lienhe',
                'frontend.blocks.footer',
            ],
                  'App\Http\View\Composers\FooterComposer'    
        );
        View::composer(
            [
                'frontend.blocks.menu',
            ],
                  'App\Http\View\Composers\MenuComposer'    
        );
        View::composer(
            [
                'frontend.pages.home',
                'frontend.pages.dichvu',
            ],
                  'App\Http\View\Composers\ServiceComposer'    
        );
    }
}
