<?php
return [
	'create' 				=> 'Thêm',
	'cancel' 				=> 'Làm lại',
	'edit'					=> 'Sửa',
	'update'				=> 'Cập nhật',
	'delete'				=> 'Xóa',
	'add_price'				=> 'Thêm giá',
	

	// Admin
	'add_admin' 			=> 'Thêm quản trị viên',
	'username_admin'		=> 'Tên quản trị viên',
	'fullname_admin'		=> 'Họ tên quản trị viên',
	'email_admin' 			=> 'Email quản trị viên',
	'phone_admin'			=> 'Số điện thoại',
	'image_admin'			=> 'Ảnh đại diện',
	'password_admin' 		=> 'Mật khẩu',
	'repasswork_admin' 		=> 'Nhập lại mật khẩu',
	'level_admin' 			=> 'Quyền quản trị viên',
	'list_admin' 			=> 'Danh sách thành viên',
	'edit_admin'			=> 'Chỉnh sửa thông tin quản trị viên',

	// Menu
	'add_menu'				=> 'Thêm Menu',
	'menu_parent'			=> 'Danh mục Menu',
	'menu_name'				=> 'Tên Menu',
	'menu_slug'				=> 'Menu không dấu',
	'menu_link'				=> 'Menu Link',
	'menu_position'			=> 'Vị trí',
	'language_menu'			=> 'Chọn ngôn ngữ',
	'list_menu'				=> 'Danh sách Menu',
	'edit_menu'				=> 'Chỉnh sửa Menu',

	// Slider
	'add_slider'			=> 'Thêm Banner',
	'name_slider'			=> 'Tên banner',
	'intro_slider'			=> 'Mô tả ngắn',
	'image_slider'			=> 'Upload banner',
	'list_slider'			=> 'Danh sách Banner',
	'edit_slider'			=> 'Chỉnh sửa Banner',

	// Lienhe
	'add_contact'			=> 'Thêm Liên hệ',
	'name_contact'			=> 'Tên liên hệ',
	'email_contact'			=> 'Email',
	'phone_contact'			=> 'Số điện thoại',
	'address_contact'		=> 'Địa chỉ',
	'content_contact'		=> 'Nội dung liên hệ',
	'list_contact'			=> 'Danh sách liên hệ',
	'edit_contact'			=> 'Chỉnh sửa liên hệ',

	// FAQ
	'add_faq'				=> 'Thêm FAQ',
	'faq_title'				=> 'Tiêu đề FAQ',
	'faq_content'			=> 'Nội dung FAQ',
	'faq_position'			=> 'Vị trí FAQ',
	'faq_keyword'			=> 'Từ khóa FAQ',
	'faq_language'			=> 'Chọn ngôn ngữ',
	'list_faq'				=> 'Danh sách FAQ',
	'edit_faq'				=> 'Chỉnh sửa FAQ',
	'add_link'				=> 'Thêm link',

	// nhận xét
	'add_awareness' 		=> 'Thêm nhận xét',
	'fullname_awareness'	=> 'Họ tên người nhận xét',
	'content_awareness'		=> 'Nội dung nhận xét',
	'image_awareness'		=> 'Chọn Ảnh',
	'list_awareness'		=> 'Danh sách nhận xét',
	'edit_awareness'		=> 'Chỉnh sửa nhận xét',
	
	// Danh mục tập tin
	'parent_category_file'	=> 'Chọn danh mục file',
	'name_category_file'	=> 'Tên danh mục file',
	'alias_category_file'	=> 'Tên danh mục không dấu',
	'add_category_file'		=> 'Thêm danh mục file',
	'list_category_file'	=> 'Danh sách danh mục file',
	'edit_category_file'	=> 'Chỉnh sửa danh mục',

	// File
	'category_parent'		=> 'Chọn danh mục',
	'type_file'				=> 'Kiểu file',
	'title_file'			=> 'Tiêu đề',
	'file'					=> 'Chọn file',
	'keyword_file'			=> 'Từ khóa File',
	'description'			=> 'Mô tả',
	'short_description'		=> 'Mô tả ngắn',
	'description'			=> 'Mô tả',
	'add_file'				=> 'Thêm File',
	'list_file'				=> 'Danh sách File',
	'edit_file'				=> 'Sửa File',

	// Danh mục tin tức
	'add_category'			=> 'Thêm danh mục tin tức',
	'parent_category'		=> 'Danh mục',
	'name_category'			=> 'Tên danh mục',
	'alias_category'		=> 'Danh mục không dấu',
	'language_category'		=> 'Chọn ngôn ngữ',
	'list_category'			=> 'Danh sách Category',
	'edit_category'			=> 'Chỉnh sửa Category',

	// tin tức
	'add_news'				=> 'Thêm tin tức',
	'parent_news'			=> 'Chọn danh mục tin',
	'title_news'			=> 'Tiêu đề tin',
	'alias_news'			=> 'Tiêu đề không dấu',
	'content'				=> 'Nội dung',
	'intro_news'			=> 'Mô tả ngắn',
	'content_news'			=> 'Nội dung tin',
	'content_detail'		=> 'Nội dung chi tiết',
	'image_news'			=> 'Chọn ảnh',
	'language_news'			=> 'Chọn ngôn ngữ',
	'status_news'			=> 'Chọn trạng thái',
	'list_news'				=> 'Danh sách tin tức',
	'edit_news'				=> 'Chỉnh sửa tin tức',

	// dich vu
	'name_service'			=> 'Tên dịch vụ',
	'image_service'			=> 'Chọn Ảnh dịch vụ',
	'content_service'		=> 'Nội dung dịch vụ',
	'link_service'			=> 'Đường dẫn',
	'position_service'		=> 'Vị trí dịch vụ',
	'add_service'			=> 'Thêm Service',
	'list_service'			=> 'Danh sách Service',
	'edit_service'			=> 'Chỉnh Sửa Service',

	// bang gia
	'title_price_quote'		=> 'Tiêu đề báo giá',
	'alias_price_quote'		=> 'Tiêu đề không dấu',
	'description_price_quote'=> 'Mô tả',
	'position_price_quote'	=> 'Vị trí',
	'add_price_quote'		=> 'Thêm báo giá',
	'list_price_quote'		=> 'Danh sách báo giá',
	'edit_price_quote'		=> 'Chỉnh sửa báo giá',
	
	// bang gia chi tiet
	'parent_price_detail'	=> 'Chọn danh mục báo giá',
	'content_price_detail'	=> 'Nội dung',
	'price_detail'			=> 'Giá',
	'position_price_detail'	=> 'Vị trí',
	'add_price_detail'		=> 'Thêm báo giá chi tiết',
	'list_price_detail'		=> 'Danh sách báo giá chi tiết',
	'edit_price_detail'		=> 'Sửa báo giá chi tiết',

	// Cấu hình website
	'Introduce'				=> 'Giới thiệu',
	'content_introduce'		=> 'Nội dung giới thiệu',
	'language_introduce'	=> 'Chọn ngôn ngữ',					
	'image_logo'			=> 'Ảnh Logo',
	'hotline'				=> 'Số điện thoại',
	'phone_name'			=> 'Tên người dùng số điện thoại',
	'language_footer'		=> 'Chọn ngôn ngữ',
	'Other Configuration' 	=> 'Cấu Hình Khác',
	'Generality'			=> 'Tổng quát',
	'Customer'				=> 'Khách hàng',
	'Email'					=> 'Email',
	'Script'				=> 'Script',
	// 'Hotline'			=> 'Số điện thoại',
	'PhoneName'				=> 'Tên người dùng số điện thoại', 
	'Keyword'				=> 'Từ khóa website: (SEO)',
	'Description'			=> 'Mô tả website: (SEO)',
	'Title'					=> 'Tiêu đề website',
	'Base url'				=> 'Link website',
	'Address'				=> 'Địa Chỉ',
	'Customer Vip Money'	=> 'Số tiền để trở thành Khách hàng thân thiết',
	'Username Gmail'		=> 'Username',
	'Password Gmail'		=> 'Password',
	'Chatbox'				=> 'Chatbox',
	// báo giá contact
	'name_contact'			=> 'Tên người gửi báo giá',
	'email_contact'			=> 'Email',
	'phone_contact'			=> 'Phone',
	'detail_contact'		=> 'Chi tiết báo giá',
	'content_contact'		=> 'Nội dung báo giá',
	'edit_contact'			=> 'Sửa liên hệ báo giá',


];
?>