<?php
return[
	'store_required'		=> 'Thêm dữ liệu thành công',
	'update_required' 		=> 'Cập nhật dữ liệu thành công',
	'delete_required' 		=> 'Xóa dữ liệu thành công',
	'destroy_required'		=> 'Xóa thành viên thành công',
	'fail_required'			=> 'Cập nhật dữ liệu không thành công. Vui lòng thử lại',
	'error_required'		=> 'Bạn không có quyền xóa thành viên này',

	// admin
	'username_required'		=> 'Vui lòng nhập trường tên',
	'username_unique'		=> 'Tên của bạn đã tồn tại.Vui lòng chọn tên khác',
	'username_min'			=> 'Tên của bạn ít nhất phải 3 ký tự',
	'username_max'			=> 'Tên của bạn không được vượt quá 25 ký tự',
	'fullname_required'		=> 'Vui lòng nhập họ tên',
	'fullname_min'			=> 'Họ tên của bạn ít nhất phải 10 ký tự',
	'fullname_max'			=> 'Họ tên của bạn không được vượt quá 50 ký tự',
	'fullname_unique'		=> 'Họ tên của bạn đã tồn tại.Vui lòng nhập họ tên khác',
	'email_required'		=> 'Vui lòng nhập trường Email',
	'email_unique'			=> 'Email của bạn đã tồn tại.Vui lòng nhập email khác',
	'phone_required'		=> 'Vui lòng nhập số điện thoại',
	'phone_min'				=> 'Số điện thoại không quá 11 ký tự',
	'phone_numeric'			=> 'Số điện thoại phải ở dạng số',
	'password_required'		=> 'Vui lòng nhập mật khẩu',
	'password_min'			=> 'Mật khẩu tối đa phải 6 ký tự',
	'password_max'			=> 'Mật khẩu của bạn không được vượt quá 25 ký tự',
	'password_confirmed'	=> 'Vui lòng nhập lại mật khẩu',
	'role_required'			=> 'Vui lòng chọn quyền quản trị',
	'image_required'		=> 'Vui lòng chọn ảnh',
	'image_mimes'			=> 'Hình ảnh không đúng định dạng',
	'image_max'				=> 'Hình ảnh Maximun 5M',

	// menu
	'name_required'			=> 'Vui lòng nhập trường tên',
	'name_max'				=> 'Tên Menu không được vượt quá 50 ký tự',
	'language_required'		=> 'Vui lòng chọn trường ngôn ngữ',

	// slider
	'intro_required'		=> 'Vui lòng nhập trường mô tả ngắn',
	'image_required'		=> 'Vui lòng chọn ảnh',
	'image_mimes'			=> 'Hình ảnh phải đúng định dạng: jpeg,jpg,png,gif,tiff,bmp,svg',
	'image_max'				=> 'Hình ảnh Maximun là 5M',

	//File
	'id_category_required'	=> 'Vui lòng chọn danh mục',
	'type_required'			=> 'Vui lòng chọn kiểu file',
	'keyword_required'		=> 'Vui lòng nhập từ khóa',
	'file_required'			=> 'Vui lòng chọn file',
	'file_max'				=> 'File tối đa 10485760KB',
	'file_mimes'			=> 'Định dạng file phải chưa đúng.Vui lòng chọn lại.',
	
	// tin tuc
	'title_required'		=> 'Vui lòng nhập vào trường tiêu đề',
	'intro_max'				=> 'Mô tả không được phép vượt quá 255 ký tự',
	'content_required'		=> 'Vui lòng nhập vào trường nội dung',
	'status_required'		=> 'Vui lòng chọn trạng thái',

	// faq
	'title_max'				=> 'Tiêu đề không được vượt quá 255 ký tự',
	'position_required'		=> 'Vui lòng nhập trường vị trí',
	// dich vu
	'name_max'				=> ' Tên của bạn không được vượt quá 10 ký tự',
];
?>