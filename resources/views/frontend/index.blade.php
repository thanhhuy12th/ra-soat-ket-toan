<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body data-rsssl=1 class="home page-id-2568 custom-background wp-custom-logo homepage-template">
    @include('frontend.blocks.menu')    
    <div id="page" class="site">
        @include('frontend.pages.banner_home')

        <div class="">
           	@yield('content')
     		@include('frontend.blocks.footer')
        </div>
</body>
</html>