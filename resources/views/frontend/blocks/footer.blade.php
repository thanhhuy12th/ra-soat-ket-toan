<div class="footer">
    <a href="tel:{!! $hotline1->content !!}" class="call-now" rel="nofollow">
        <div class="bor-tel">
        </div>
        <div class="bg-tel">
            <i class="fa fa-phone"></i>
        </div>
        <div class="hotline">
            <span class="before-hotline">Hotline:</span>
            <span class="hotline-number">{!! $hotline1->content !!}</span>
        </div>
    </a>
    <div class="container">
        <div class="row pt-4 pb-4">
            <div class="col-md-6 col-lg-3">
                <div class="footer-title">
                    <p>{!! $lang["connect_us"] !!}</p>
                </div>
                <div class="footer-social">
                    <a href="https://www.facebook.com/rasoatketoan/" title="facebook.com" target="_blank"><i class="fa fa-facebook icon"></i></a>
                    <a href="https://www.youtube.com" title="youtube.com" target="_blank"><i class="fa fa-youtube icon"></i></a>
                    <a href="tel:{!! $hotline1->content !!}" mypage="" class="call-now" rel="nofollow" /><i class="fa fa-headphones icon"></i></a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="footer-title">
                    <p>{!! $lang["subscribe"] !!}</p>
                </div>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" maxlength="50" name="email" placeholder='{!! $lang["ph_subscribe"] !!}'>
                    <div class="input-group-append">
                        <button class="input-group-text">{!! $lang["send"] !!}</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="footer-title">
                    <p>{!! $lang["history_report"] !!}</p>
                </div>
                <div class="footer-hit">
                    <p><i class="fa fa-users mr-1" style="color:#7cff99"></i>{!! $lang["history_online"] !!} :80 </p>
                    <p><i class="fa fa-bar-chart mr-1" style="color:#f7133d"></i>{!! $lang["history_today"] !!} :5000 </p>
                    <p><i class="fa fa-pie-chart mr-1" style="color:#f7e013"></i>{!! $lang["history_total"] !!} :22222</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="footer-title">
                    <p>{!! $lang["about_us"] !!}</p>
                </div>
                <div class="address">
                    <p><i class="fa fa-location-arrow pr-2"></i>{!! $address->content !!}</p>
                    <p><i class="fa fa-phone-square pr-2"></i> {!! $hotline1->content !!} - {!! $phonename1->name_phone !!}</p>
                    <p><i class="fa fa-chrome pr-2"></i>{!! $email->content !!}</p>
                    <p><i class="fa fa-link pr-2"></i>{!! $link->content !!}</p>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <p> &copy;2019 Design by <a href="https://cloudone.vn" target="_blank">CloudOne</a></p>
        </div>
    </div>
</div>
<div id="fb-root"></div>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    $(document).ready(function () {
        $("body").on("contextmenu",function(e){
            return false;
        });
    });
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v3.3'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="216943225349542"
  logged_in_greeting="Xin chào, hãy chat ngay với chúng tôi để được tư vấn trực tiếp!"
  logged_out_greeting="Xin chào, hãy chat ngay với chúng tôi để được tư vấn trực tiếp!">
</div>