<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title','Rà soát kế toán')</title>

<link rel="stylesheet" href="{{asset('frontend/assets/css/style.css')}}"/>
<link rel="stylesheet" href="{{asset('frontend/assets/font-awesome/font-awesome.min1efe.css')}}" />
<link rel="stylesheet" href="{{asset('frontend/assets/css/animate1efe.css')}}" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

 <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A200%2Cnormal%2C300%2C600%2C700%7CPlayfair+Display%3Aregular%2Citalic%2C700%2C900&amp;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >

<script type="text/javascript" src="{{asset('frontend/assets/js/jquery/jqueryb8ff.js?ver=1.12.4')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/js/drop_menu_selection1efe.js?ver=1.2.8')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/js/libs/fixto1efe.js?ver=1.2.8')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/js/sticky1efe.js?ver=1.2.8')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/js/jquery/ui/effect.mine899.js?ver=1.11.4')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/js/jquery/ui/effect-slide.mine899.js?ver=1.11.4')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
<script src="{{asset('frontend/assets/js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
<script>
    $(function() {
        $("#accordion").accordion();
    });
</script>
<link rel="stylesheet" href="{{asset('frontend/assets/css/custom.css')}}" />
