<div class="header-top homepage " data-sticky='0' data-sticky-mobile='1' data-sticky-to='top'>
    <div class="navigation-wrapper ope-front-page mt-1 mb-1">
        <div class="logo_col">
            <a href="." class="logo-link dark logo" rel="home" itemprop="url"><img src="{{ asset('uploads/logo/'.$logo->content)}}" class="logo dark" alt="" itemprop="logo" /></a>
            <a href="." class="custom-logo-link logo" rel="home" itemprop="url"><img src="{{ asset('uploads/logo/'.$logo->content)}}" class="custom-logo" alt="" itemprop="logo" /></a>
        </div>
        <div class="main_menu_col cus-width">
            <div class="language">
                <a href="{{ route('user.change-language', ['vi']) }}"><img src="{{asset('frontend/images/vietnam.png')}}" /></a>
                <a href="{{ route('user.change-language', ['en']) }}"><img src="{{asset('frontend/images/us.png')}}" class="ml-2" /></a>
            </div>
            <div id="drop_mainmenu_container" class="menu-primary-menu-container">
                <ul id="drop_mainmenu" class="fm2_drop_mainmenu">
                    @foreach($menu as $item)
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home">
                        <a href="{{$item->link}}"> {{$item->name}}</a>
                        <?php
                        $data = select_products_to_category_parent($item->id);
                        ?>
                        <ul class="sub-menu">
                            @foreach($data as $dt)
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category">
                                <a href="{{$dt->link}}">{{$dt->name}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </div>
            <!-- Menu Mobile -->
            <div class="show-menu">
                <div class="bar-menu">
                    <i class="fa fa-bars "></i>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Menu -->
<div class="menu menu_mm trans_300">
    <div class="menu_container menu_mm">
        <div class="page_menu_content">
            <ul class="page_menu_nav menu_mm">
                @foreach($menu as $item)
                <li class="page_menu_item menu_mm has-children"><a href="{{$item->link}}">{{$item->name}}</a>
                    <?php
                    $data = select_products_to_category_parent($item->id);
                    ?>
                    <ul class="page_menu_selection menu_mm">
                        @foreach($data as $dt)
                        <li class="page_menu_item menu_mm">
                            <a href="{{$dt->link}}">{{$dt->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>