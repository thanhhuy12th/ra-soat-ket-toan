@extends('frontend.index')
@section('content')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="">
    <div id="overlapable-2" style="background-color: #ffffff;" data-label="Overlapable" data-id="stripped-coloured-icon-boxes" data-category="overlapable" class="features-coloured-icon-boxes-section" data-overlap="true">
        <div class="container">

            <h3 style= "text-align: center" class="pb-3 pt-3">{!! $lang['3m_prestige_and_service_quality'] !!}</h3>
             <div class="row" >
                <div class="features-coloured-icon-boxes-featurecol col-md-4">
                    <div class="features-coloured-icon-boxes-iconcontainer" data-content-item-container="true"> <i data-cp-fa="true" class="features-coloured-icon fa fa-bolt"></i> </div>
                    <h4 class="s-title">{!! $lang['accounting_consulting'] !!}</h4>
                    <p class="s-content">{!! $lang['intro_accounting_consulting'] !!} &nbsp;</p> 
                </div>
                <div class="features-coloured-icon-boxes-featurecol col-md-4">
                    <div class="features-coloured-icon-boxes-iconcontainer" data-content-item-container="true"> <i data-cp-fa="true" class="features-coloured-icon fa fa-cloud"></i> </div>
                    <h4 class="s-title">{!! $lang['tax_consulting_and_planning'] !!}</h4>
                    <p class="s-content">{!! $lang['intro_tax_consulting_and_planning'] !!} &nbsp;</p> 
                </div>
                <div class="features-coloured-icon-boxes-featurecol col-md-4">
                    <div class="features-coloured-icon-boxes-iconcontainer" data-content-item-container="true"> <i data-cp-fa="true" class="features-coloured-icon fa fa-signal"></i> </div>
                    <h4 class="s-title">{!! $lang['tax_declaration'] !!}</h4>
                    <p class="s-content">{!! $lang['intro_tax_declaration'] !!} &nbsp;</p> 
                </div>
                <div class="features-coloured-icon-boxes-featurecol col-md-4">
                    <div class="features-coloured-icon-boxes-iconcontainer" data-content-item-container="true"> <i data-cp-fa="true" class="features-coloured-icon fa fa-calculator"></i> </div>
                    <h4 class="s-title">{!! $lang["annual_settlement"] !!}</h4>
                    <p class="s-content">{!! $lang["intro_annual_settlement"] !!} &nbsp;</p> 
                </div>
                <div class="features-coloured-icon-boxes-featurecol col-md-4">
                    <div class="features-coloured-icon-boxes-iconcontainer" data-content-item-container="true"> <i data-cp-fa="true" class="features-coloured-icon fa fa-paperclip"></i> </div>
                    <h4 class="s-title">{!! $lang["other_tax_services"] !!}</h4>
                    <p class="s-content"> {!! $lang['intro_other_tax_services'] !!} &nbsp;</p>
                </div>
                <div class="features-coloured-icon-boxes-featurecol col-md-4">
                    <div class="features-coloured-icon-boxes-iconcontainer" data-content-item-container="true"> <i data-cp-fa="true" class="features-coloured-icon fa fa-eyedropper"></i> </div>
                    <h4 class="s-title">{!! $lang['management_consulting'] !!}</h4>
                    <p class="s-content"> {!! $lang['intro_management_consulting'] !!} &nbsp;</p>
                </div>
            </div>   
        </div>
    </div>
    <div data-label="Overlapable" data-id="stripped-features-overlapped-icons-section" data-category="overlapable" class="features-overlapped-icons-section stripped-features-overlapped-icons-section" data-overlap="true" id="overlapable-4" style="background-color: rgb(255, 255, 255);">
        <div class="container">
             <h3 style= "text-align: center" class="pb-3">{!! $lang['our_advantages'] !!}</h3>
            <div class="row">
                <div class="features-large-icons-featcol col-md-3 ">
                    <div class="features-overlapped-icons-featinnerrow">
                        <div class="features-icon-container"><i data-cp-fa="true" class="font-icon-features-icon-container fa fa-bolt"></i> </div>
                        <div class="v">
                            <h4 class="s-title">{!! $lang["prestige_and_service_quality"] !!}</h4>
                            <p class="s-content">{!! $lang["intro_prestige_and_service_quality"] !!} &nbsp;</p>
                        </div>
                    </div>
                </div>
                <div class="features-large-icons-featcol col-md-3">
                    <div class="features-overlapped-icons-featinnerrow">
                        <div class="features-icon-container"><i data-cp-fa="true" class="font-icon-features-icon-container fa fa-rocket"></i> </div>
                        <div class="v">
                            <h4 class="s-title">{!! $lang["scrupulously_accurate"] !!}</h4>
                            <p class="s-content"> {!! $lang['intro_scrupulously_accurate'] !!} &nbsp;</p>
                        </div>
                    </div>
                </div>
                <div class="features-large-icons-featcol col-md-3">
                    <div class="features-overlapped-icons-featinnerrow">
                        <div class="features-icon-container"><i data-cp-fa="true" class="font-icon-features-icon-container fa fa-signal"></i> </div>
                        <div class="v">
                            <h4 class="s-title">{!! $lang['close_procedure'] !!}</h4>
                            <p class="s-content">{!! $lang['intro_close_procedure'] !!} &nbsp;</p>
                        </div>
                    </div>
                </div>
                <div class="features-large-icons-featcol col-md-3">
                    <div class="features-overlapped-icons-featinnerrow">
                        <div class="features-icon-container"><i data-cp-fa="true" class="features-coloured-icon fa fa-cloud"></i> </div>
                        <div class="v">
                            <h4 class="s-title">{!! $lang['absolute_security'] !!}</h4>
                            <p class="s-content">{!! $lang['intro_absolute_security'] !!} &nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="features-large-icons-featcol col-md-4">
                    <div class="features-overlapped-icons-featinnerrow">
                        <div class="features-icon-container"><i data-cp-fa="true" class="font-icon-features-icon-container fa fa-money"></i> </div>
                        <div class="v">
                            <h4 class="s-title">{!! $lang["cost_saving"] !!}</h4>
                            <p class="s-content"> {!! $lang['intro_cost_saving'] !!} &nbsp;</p>
                        </div>
                    </div>
                </div>
                <div class="features-large-icons-featcol col-md-4">
                    <div class="features-overlapped-icons-featinnerrow">
                        <div class="features-icon-container"><i data-cp-fa="true" class="font-icon-features-icon-container fa fa-free-code-camp"></i> </div>
                        <div class="v">
                            <h4 class="s-title">{!! $lang["free_consultation"] !!}</h4>
                            <p class="s-content"> {!! $lang['intro_free_consultation'] !!} &nbsp;</p>
                        </div>
                    </div>
                </div>
                <div class="features-large-icons-featcol col-md-4">
                    <div class="features-overlapped-icons-featinnerrow">
                        <div class="features-icon-container"><i data-cp-fa="true" class="font-icon-features-icon-container fa fa-male"></i> </div>
                        <div class="v">
                            <h4 class="s-title">{!! $lang["human_resources"] !!}</h4>
                            <p class="s-content"> {!! $lang['intro_human_resources'] !!} &nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @for ($i = 0; $i < count($news); $i++)
        @if($i%2==0)
        <div style="background-color: #ffffff;" class="content-left-image-section">
            <div class="gridContainer">
                <div class="row">
                    <div class="content-left-image-imgcol">
                        <img src="{!!asset('uploads/news/'. $news[0]->image ) !!}">
                    </div>
                    <div class="content-left-image-textcol dynamic-color">
                        <h2 class="">{!! $news[$i]->title !!}</h2>
                        <p class="large_text"> {!! $news[$i]->intro !!} </p> <a class="button blue" href="{{route('tin-tuc-detail',$news[$i]->alias)}}" target="_self"> {!! $lang["read_more"] !!} </a>
                    </div>
                </div>
            </div>
        </div>
        @else 
        <div class="gridContainer">
            <div class="row">
                <div class="content-right-image-section">
                    <div class="content-right-image-textcol dynamic-color">
                        <h2 class="">{!! $news[$i]->title !!}</h2>
                        <p class="large_text"> {!! $news[$i]->title !!}</p> <a class="button blue" href="{{route('tin-tuc-detail',$news[$i]->alias)}}" target="_self"> {!! $lang["read_more"] !!} </a>
                    </div>
                    <div class="content-right-image-imgcol"> <img src="{!!asset('uploads/news/'. $news[1]->image ) !!}"> </div>
                </div>
            </div>
        </div>
        @endif

    @endfor

        <div id="portfolio-2" style="background-color: #ffffff;"> 
            <div class="portfolio-full-section">
                 @foreach($service as $dv)
                <div class="portfolio-full-projectco cp4cols cp4cols-tablet">
                    <div class="contentswap-effect ContentSwap103" id="ContentSwap103">
                        <div class="ContentSwap103_content initial-image"><img class="portfolio-full-projectimg" src="{{asset('uploads/service/'. $dv->image)}}"> </div>
                        <div class="overlay" style="display: block; opacity: 1;"></div>
                        <div class="swap-inner" style="opacity: 1; display: block;">
                            <div class="ContentSwap103-center">
                                <h4 class="protfolio-full-itemtitle">{{$dv->name}}</h4><a class="button yellow" href="{{$dv->link}}" target="_self">{!! $lang["btn_detail"] !!}</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div id="testimonials-1" style="background-color: #f6f6f6;" data-label="Testimonials" data-id="testimonials-boxed-section" data-category="testimonials" class="testimonials-boxed-section">
            <div class="gridContainer">
                <div class="testimonials-boxed-textcol dynamic-color">
                    <h2 class="">{!! $lang["client_said"] !!}</h2>
                    <p class="">{!! $lang["client_said_des"] !!}</p>
                </div>
                <div class="testimonials-boxed-itemsrow dark-text">
                    @foreach($nhanxet as $item)
                    <div class="testimonials-boxed-itemcol cp4cols cp6cols-tablet">
                        <div class="testimonials-boxed-itemcard">
                            <h4 class="emp-talk testimonials-boxed-itemtext ">“{{$item->content}}”</h4>
                            <img class="testimonials-boxed-itemimg" src="{{ asset('uploads/avatar/'.$item->image)}}">
                            <h5 class="testimonials-boxed-itemname">{{$item->fullname}}</h5>
                            <h6 class="testimonials-boxed-itempos">{{-- CEO @ COOLAPP --}}</h6>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div id="cta-1" data-label="Call to action" data-id="cta-blue-section" data-category="cta" class="cta-blue-section">
            <div class="gridContainer">
                <div class="flexbox-list flexbox-list-align-center row">
                    <div class="cta-blue-textcol dynamic-color">
                        <h2 class="cta-blue-text"> <b class="element2"> {!! $lang["contact_us"] !!}</b> </h2>
                    </div>
                    <div class="cta-blue-btncol"> <a href="tel:{!! $hotline1->content !!}" class="button big yellow dark-text" href="#"> {!! $lang["btn_contact_us"] !!} </a> </div>
                </div>
            </div>
        </div>
        <div id="team-1" style="background-color: #ffffff;" data-label="Team" data-id="team-colors-section" data-category="team" class="team-colors-section">
            <div class="gridContainer">
                <div class="team-colors-textcol dynamic-color">
                    <h2>{!! $lang["employee"] !!}</h2>
                    <p class="">{!! $lang["employee_des"] !!}</p>
                </div>
                <div class="team-colors-membersrow dark-text" data-type="row" >
                    <div class="team-colors-membercol cp3cols cp6cols-tablet">
                        <div class="team-colors-membercard">
                            <div class="team-colors-background"></div>
                            <div class="team-colors-memberinfo" style="text-align: justify;">
                                <img class="team-colors-memberimg" src="{{asset('frontend/images/nhansu/placeholder.jpg')}}">
                                <h4 class="team-colors-membername" style="font-size: 20px;">{!! $lang["user_name1"] !!} </h4>
                                <p class="team-colors-memberposition">{{-- {!! $lang["user_title1"] !!} --}} </p>
                                <p class="emp-content">{!! $lang["user_content1"] !!}</p>
                                <hr class="team-colors-separator">
                            </div>
                           {{--  <div class="team-colors-membericons" data-type="group"> 
                                <a href="https://www.facebook.com" title="facebook.com" target="_blank"><i data-cp-fa="true" class="team-colors-icon fa fa-facebook"></i></a> 
                                <a href="#"><i data-cp-fa="true" class="fa fa-envelope team-colors-icon"></i></a> 
                                <a href="#"><i data-cp-fa="true" class="fa fa-phone team-colors-icon"></i></a> 
                            </div> --}}
                        </div>
                    </div>
                    <div class="team-colors-membercol cp3cols cp6cols-tablet">
                        <div class="team-colors-membercard">
                            <div class="team-colors-background"></div>
                            <div class="team-colors-memberinfo" style="text-align: justify;">
                                <img class="team-colors-memberimg" src="{{asset('frontend/images/nhansu/user.png')}}">
                                <h4 class="team-colors-membername" style="font-size: 20px; ">{!! $lang["user_name2"] !!}</h4>
                                <p class="team-colors-memberposition">{{-- {!! $lang["user_title2"] !!} --}}</p>
                                <p class="emp-content">{!! $lang["user_content2"] !!}</p>
                                <hr class="team-colors-separator">
                            </div>                          
                        </div>
                    </div>
                    <div class="team-colors-membercol cp3cols cp6cols-tablet">
                        <div class="team-colors-membercard">
                            <div class="team-colors-background"></div>
                            <div class="team-colors-memberinfo" style="text-align: justify;">
                                <img class="team-colors-memberimg" src="{{asset('frontend/images/nhansu/placeholder.jpg')}}">
                                <h4 class="team-colors-membername" style="font-size: 20px; ">{!! $lang["user_name3"] !!}</h4>
                                <p class="team-colors-memberposition">{!! $lang["user_title3"] !!}</p>
                                <p class="emp-content">{!! $lang["user_content3"] !!}</p>
                                <hr class="team-colors-separator">
                            </div>                          
                        </div>
                    </div>
                    <div class="team-colors-membercol cp3cols cp6cols-tablet">
                        <div class="team-colors-membercard">
                            <div class="team-colors-background"></div>
                            <div class="team-colors-memberinfo" style="text-align: justify;">
                                <img class="team-colors-memberimg" src="{{asset('frontend/images/nhansu/user.png')}}">
                                <h4 class="team-colors-membername" style="font-size: 20px; ">{!! $lang["user_name4"] !!}</h4>
                                <p class="team-colors-memberposition">{{-- {!! $lang["user_title4"] !!} --}}</p>
                                <p class="emp-content">{!! $lang["user_content4"] !!}</p>
                                <hr class="team-colors-separator">
                            </div>                        
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div id="numbers" data-label="Numbers" data-id="numbers-section" data-category="numbers" data-bg="transparent" class="numbers-section" data-parallax-depth="20">
            <div class="gridContainer dynamic-color">
                <div class="numbers-itemcol">
                    <h2 class="numbers-numbertext">39</h2>
                    <h3 class="numbers-titletext">{!! $lang["num_projects"] !!}</h3>
                </div>
                <div class="numbers-itemcol">
                    <h2 class="numbers-numbertext">79</h2>
                    <h3 class="numbers-titletext">{!! $lang["num_customers"] !!}</h3>
                </div>
                <div class="numbers-itemcol-last">
                    <h2 class="numbers-numbertext">15</h2>
                    <h3 class="numbers-titletext">{!! $lang["num_employees"] !!}</h3>
                </div>
            </div>
        </div>
        <div id="latest-posts" style="background-color: #ffffff;" class="blog-section">
            <div class="gridContainer">
                <div class="blog-textrow">
                    <div class="blog-textcol dynamic-color">
                        <h2 class="">{!! $lang["news_post"] !!}</h2>
                    </div>
                </div>
                <div class="blog-postsrow dark-text">
                    @foreach($news as $tt)
                    <div class="blog-postcol cp4cols">

                        <div class="post-content">
                            <a class="post-list-item-thumb" href="">
                                <img width="825" height="510" src="{{ asset('uploads/news/'. $tt->image ) }}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" /> </a>
                            <div class="row_345">
                                <h3 class="blog-title">
                                    <a href="{{route('tin-tuc-detail',$tt->alias)}}" rel="bookmark">
                                        {{$tt->title}}</a>
                                </h3>
                                <p class="blog-content">{{$tt->intro}}</p>
                                <a class="button blue small" href="{{route('tin-tuc-detail',$tt->alias)}}">
                                    <span data-theme="one_page_express_latest_news_read_more">{!! $lang["read_more"] !!}</span>
                                </a>
                                <hr class="blog-separator">
                                <div class="post-header">
                                    {{-- <i class="font-icon-post fa fa-user"></i>
                                                <a href="author/nana3107/index.html" title="Posts by Ca Thy" rel="author">Ca Thy</a> <i class="font-icon-post fa fa-calendar"></i> --}}
                                    <span class="span12">{{$tt->created_at}}</span>

                                    <i class="font-icon-post fa fa-comment-o"></i>
                                    <span>0</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </div>
                <div class="blog-textcol"> <a class="button blue" data-attr-shortcode="href:one_page_express_blog_link" href="{{route('tin-tuc')}}" target="_self">{!! $lang["see_all_post"] !!}</a> </div>
            </div>
        </div>

    </div>
    <div class="contact-section-formsection white-text cus-contact parallax" style="background-image: url(https://www.pixelstalk.net/wp-content/uploads/images2/Free-download-Computer-Art-Photo.jpg)">
        <div class="container">
            <div class="row">
                <div class="form-contanier ">
                    <form action="{{route('lien-he')}}" method="POST" />
                    @csrf
                    <div class="form-group">
                        <label>{!! $lang["fullname"] !!}</label>
                        <input type="text" class="form-control" maxlength="50" name="name" placeholder="{!! $lang["ph_fullname"] !!}" required="">
                    </div>
                    <div class="form-group">
                        <label>{!! $lang["email"] !!}</label>
                        <input type="email" class="form-control" maxlength="50" name="email" placeholder="{!! $lang["ph_email"] !!}" required="">
                    </div>
                    <div class="form-group">
                        <label>{!! $lang["phone"] !!}</label>
                        <input type="number" class="form-control" maxlength="50" name="phone" placeholder="{!! $lang["ph_phone"] !!}" required="">
                    </div>
                    <div class="form-group">
                        <label>{!! $lang["adress"] !!}</label>
                        <input type="text" class="form-control" maxlength="50" name="address" placeholder="{!! $lang["ph_adress"] !!}" required="">
                    </div>
                    <div class="form-group">
                        <label>{!! $lang["message"] !!}</label>
                        <textarea rows="1" class="form-control" name="content" placeholder="{!! $lang["ph_message"] !!}" required=""></textarea>
                    </div>
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="6Ldsp60UAAAAAI-lfsDSsaLGnD_O9xRXW9-YOH3q"></div>
                    </div>
                    <button type="submit" class="btn btn-primary">{!! $lang["send"] !!}</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection