<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body class="home custom-background wp-custom-logo homepage-template">
    @include('frontend.blocks.menu')
    <div id="page" class="site">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <div class="new-conntent">
                            <h2>{!! $chitiettin->title !!}</h2> <br />
                            <p><img src="{{asset('uploads/news/'.$chitiettin->image)}}" style="width: 70%;" class="img-center" alt="..."></p> <br />
                            <p>{!! $chitiettin->content !!}</p>
                        </div>
                        <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator_{!! $chitiettin->id !!}" data-width="" data-numposts="10"></div>
                    </div>

                    <div class="col-md-3 col-lg-3 col-xl-3 ">

                        <ul class="list-group mb-4">
                            <li class="list-group-item">{!! $lang["caregory_news"] !!}</li>
                            @foreach($dmtt as $dm)
                            <li class="list-group-item"><a href="{{route('danh-muc-tin-tuc',[$dm->id,$dm->alias])}}">{{$dm->name}}</a></li>
                            @endforeach
                        </ul>

                        <div class="fixpo">
                            <ul class="list-group mb-4">
                                <li class="list-group-item">{!! $lang["featured_article"] !!}</li>
                                @foreach($hot as $tt)
                                <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                                @endforeach
                            </ul>

                            {{-- <ul class="list-group mb-4">
                                <li class="list-group-item">Bài viết gần nhất</li>
                                <li class="list-group-item">Dapibus ac facilisis in</li>
                                <li class="list-group-item">Morbi leo risus</li>
                                <li class="list-group-item">Porta ac consectetur ac</li>
                                <li class="list-group-item">Vestibulum at eros</li>
                            </ul> --}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>
</body>

</html>