<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.blocks.head')
    <link rel="stylesheet" href="{{asset('frontend/assets/js/libs/tree/tree.css')}}">
</head>

<body data-rsssl=1 class="home page-id-2568 custom-background wp-custom-logo homepage-template">

   @include('frontend.blocks.menu')
    <div id="page" class="site">
        
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                      
                        <div class="table-responsive">
                            <table class="table table-borderless mb-3">
                                <h3>{!! $lang["document"] !!}</h3>
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                               
                                    <tr>
                                        <td scope="row">
                                           
                                            <?php showCategories($dm);?>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                   <div class="col-md-3 col-lg-3 col-xl-3">
                     
                        <ul class="list-group mb-4">
                            <li class="list-group-item">Bài viết nổi bật</li>
                            @foreach($hot as $tt)
                                <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>
    </div>
    <script src="{{asset('frontend/assets/js/libs/tree/tree.js')}}"></script>
    <script>
        JSLists.createTree("f1combined");
    </script>
</body>

</html>