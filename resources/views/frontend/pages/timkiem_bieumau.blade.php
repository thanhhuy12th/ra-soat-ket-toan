<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body class="home custom-background wp-custom-logo homepage-template">
    
    @include('frontend.blocks.menu')
    <div id="page" class="site">
        {{-- @include('frontend.pages.banner') --}}
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <div class="search-results mb-3">
                            <h3>Tìm kiếm : {{ $tukhoa }}</h3>
                        </div>
                        <div class="card mb-3">
                            @foreach($bieumau as $bm)
                            <div class="row no-gutters mb-4">
                                <div class="col-md-8">
                                    <div class="card-body ">
                                        <h5 class="card-title">{{$bm->title}}</h5>
                                       {{--  <p class="card-text">{{$bm->short_description}}</p> --}}
                                        <input type="hidden" id="motabreak" value="{{$bm->short_description}}">
                                        <p id="output"></p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p ><small class="text-muted">{{$bm->created_at}}</small></p>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="text-right">
                                                    <a href="{{route('bieu-mau-detail',$bm->alias)}}" class="btn btn-primary">{!! $lang["read_more"] !!}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div> 
                    </div>  
                </div>
                <!-- Phân trang -->
                <div class="row" />
                <nav aria-label="..." />
                    <ul class="pagination mt-4" />
                        @if( $bieumau->currentPage() != 1)
                            <li class="page-item"><a class="page-link" href="{!! $bieumau->url($bieumau->currentPage() - 1) !!}">Previous</a></li>
                        @endif
                        @for($i=1; $i <= $bieumau->lastPage(); $i= $i+1)
                            <li class="page-item {!! ($bieumau->currentPage() == $i )? 'active': '' !!}">
                                <a class="page-link" href="{!! $bieumau->url($i) !!}">{!! $i !!}</a>
                            </li>
                            @endfor
                            @if($bieumau->currentPage() != $bieumau->lastPage())
                                <li class="page-item"><a class="page-link" href="{!! $bieumau->url($bieumau->currentPage() + 1) !!}">Next</a></li>
                            @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    @include('frontend.blocks.footer')
    </div>
<script type="text/javascript">
    let textareaText = $('#motabreak').val();
    textareaText = textareaText.replace(/\r?\n/g, '<br />');
    $('#output').html(textareaText);
    
</script>
</body>

</html>