<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
    <script src="https://code.jquery.com/jquery-latest.js"></script>
</head>

<body class="home custom-background wp-custom-logo homepage-template">

    @include('frontend.blocks.menu')
    <div id="page" class="site">
        
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <h1>Bảng giá</h1>
                        <h3>{!! $banggia->title !!}</h3>
                        <input type="hidden" id="motabreak" value="{!!$banggia->description!!}">
                        <p id="output"></p>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">STT</th>
                                    <th scope="col">Tiêu đề</th>
                                    <th scope="col">Đơn giá</th>
                                    <th scope="col">Chọn</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                                @foreach($baogia as $item)
                                
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->content}}</td>
                                    <td>{{ number_format($item->price,0,",",".") }}</td>
                                    <td>
                                        <input style="margin-left:5%" type="checkbox" class="form-check-input" id="checkgia" data-id="{{$item->id_detail}}" data-title="{{$item->content}}" data-price="{{$item->price}}" value="on" onChange="changePrice(this)">
                                    </td>
                                </tr>
                                @endforeach
                                <!-- bảng giá -->
                                <tr>     
                                    <th colspan="3"  class="text-center" ><p>Thuế VAT 10%</th>
                                    <th colspan="2"  class="text-center" ><p id="vat"></p></th>   
                                </tr>
                                 <tr>     
                                    <th colspan="3"  class="text-center" ><p>Giá tạm tính</th>
                                    <th colspan="2"  class="text-center" ><p id="demo"></p></th>   
                                </tr>
                                <tr>
                                    <th colspan="5" class="text-center">
                                    <button type="button" class="btn btn-outline-primary" id="showModal"  data-toggle="modal" data-target="#exampleModalLong">Gửi báo giá</button>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content" style=" top: 100px">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Gửi bảng giá</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="padding-top: 0px; padding-bottom: 0px">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span style="font-weight: bold;">Thông tin đã chọn:</span>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">STT</th>
                                                        <th scope="col">Chi tiết</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="PriceInfo">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <form style="margin-top:0px; margin-bottom:0px">
                                        <div class="form-group" >
                                            <label for="recipient-name" class="col-form-label">Họ tên:</label>
                                            <input type="text" class="form-control" name="name" id="recipient-name" style="margin-bottom:0px" required />
                                            <label for="recipient-email" class="col-form-label">Email:</label>
                                            <input type="text" class="form-control" id="recipient-email" name="email" style="margin-bottom:0px" required />
                                            <label for="recipient-sdt" class="col-form-label">Số điện thoại:</label>
                                            <input type="text" class="form-control" id="recipient-sdt" name="phone" style="margin-bottom:0px" required />
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Message:</label>
                                            <textarea class="form-control" id="message-text" name="content" style="height:100px" required /></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                    <button type="submit" class="btn btn-primary" id="sentBaoGia">Gửi</button>
                                </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <div class="fixpo">
                            <ul class="list-group mb-4">
                                <li class="list-group-item">Bài viết nổi bật</li>
                                @foreach($hot as $tt)
                                    <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    </div>
</script>
    <script language="javascript">

        var tong        = 0;
        var vat         = 0;
        var dataTotal   = [];
        var Total       ="{{$banggia->title}} <br/>";
        $(document).ready(function(){
            $("#demo").html(tong);
            $("#vat").html(vat);
            $("#sentBaoGia").click(function(){
                for(var i = 0; i < dataTotal.length; i++) {
                    Total += "- "+dataTotal[i].detail + "<br/>";
                }
                
                $.ajax({
                    url:"../api/send-baogia",
                    method:"POST", 
                    data:{
                      Name:     $("#recipient-name").val(), 
                      Email:    $("#recipient-email").val(), 
                      Phone:    $("#recipient-sdt").val(), 
                      Message:  $("#message-text").val(), 
                      Detail: Total, 
                    },
                    beforeSend: function() {
                        $("#sentBaoGia").html("Sending ...");
                    }, 
                    success:function(response) {
                        alert("Bạn đã gửi thành công, chúng tôi sẽ sớm liên hệ với bạn");
                        $("#sentBaoGia").html("Gửi");
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown){
                        console.log(errorThrown);
                        $("#sentBaoGia").html("Gửi");
                        alert("Lỗi kỹ thuật, vui lòng liên hệ ban quản trị");
                    }

                });
                
            });
            $("#showModal").click(function(){
                $("#PriceInfo").html("");
                dataTotal.forEach(showChooseList);
            });
        })
        function showChooseList(item, index, arr) {
            $("#PriceInfo").append('<tr><td>'+parseInt(index+1)+'</td><td>'+item.detail+'</td></tr>');
        }
        const formatter = new Intl.NumberFormat('de-DE', {
                                        style: 'decimal',
                                        minimumFractionDigits: 0
                                        });
        function changePrice(elm) {
            var gia = elm.getAttribute("data-price");
            var title = elm.getAttribute("data-title");
            var id = elm.getAttribute("data-id");
            var check = $(elm).is(":checked");


            var dataDetail = {
                id: id,
                detail: title + ": " + gia
            };
    
            if (check) {
                tong = tong + parseInt(gia);
                vat = vat + parseInt(gia)*10/100;                
                $("#demo").html(formatter.format(tong)  + " vnđ");
                $("#vat").html(formatter.format(vat) + " vnđ");
                dataTotal.push(dataDetail);
            } else {
                tong -= parseInt(gia);
                vat -= parseInt(gia)*10/100;
                $("#demo").html(formatter.format(tong) + " vnđ");
                $("#vat").html(formatter.format(vat) + " vnđ");
                var index = dataTotal.map(x => {
                  return x.id;
                }).indexOf(id);

                dataTotal.splice(index, 1);
            }
        }

    let textareaText = $('#motabreak').val();
    textareaText = textareaText.replace(/\r?\n/g, '<br />');
    $('#output').html(textareaText);
    </script>  
     @include('frontend.blocks.footer')
</body>
</html>