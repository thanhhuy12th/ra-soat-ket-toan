<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.blocks.head')
</head>

<body data-rsssl=1 class="home page-id-2568 custom-background wp-custom-logo homepage-template">

   @include('frontend.blocks.menu')
    <div id="page" class="site">
        
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <!-- tìm kiếm -->
                        <form action="{{url('tim-kiem-bieu-mau')}}" method="POST">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" maxlength="50" name="tukhoa" placeholder="Tìm kiếm" aria-describedby="basic-addon1">
                            <div class="input-group-prepend">
                                <button type="submit" class="fa fa-search"></button>
                            </div>
                        </div>
                        </form>

                       <div class="card mb-3">
                            @foreach($bieumau as $bm)
                            <div class="row no-gutters mb-4">                                
                                <div class="col-md-8">
                                    <div class="card-body ">
                                        <h5 class="card-title">{{$bm->title}}</h5>
{{--                                         <p class="card-text">{{$bm->short_description}}</p> --}}
                                        <input type="hidden" id="motabreak" value="{{$bm->short_description}}">
                                        <p id="output"></p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p ><small class="text-muted">{{$bm->created_at}}</small></p>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="text-right">
                                                    <a href="{{route('bieu-mau-detail',$bm->alias)}}" class="btn btn-primary">{!! $lang["read_more"] !!}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>   
                    </div>
                     <div class="col-md-3 col-lg-3 col-xl-3">
                        <div class="fixpo">
                            <ul class="list-group mb-4">
                                <li class="list-group-item">Bài viết nổi bật</li>
                                @foreach($hot as $tt)
                                <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    

                <!-- Phân trang -->
                <div class="row">
                    <nav aria-label="...">
                        <ul class="pagination" />
                            @if( $bieumau->currentPage() != 1)
                                <li class="page-item"><a class="page-link" href="{!! $bieumau->url($bieumau->currentPage() - 1) !!}">Previous</a></li>
                            @endif
                            @for($i=1; $i <= $bieumau->lastPage(); $i= $i+1)
                                <li class="page-item {!! ($bieumau->currentPage() == $i )? 'active': '' !!}">
                                    <a class="page-link" href="{!! $bieumau->url($i) !!}">{!! $i !!}</a>
                                </li>
                           @endfor
                           @if($bieumau->currentPage() != $bieumau->lastPage())
                                <li class="page-item"><a class="page-link" href="{!! $bieumau->url($bieumau->currentPage() + 1) !!}">Next</a></li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>
    </div>
<script type="text/javascript">
    let textareaText = $('#motabreak').val();
    textareaText = textareaText.replace(/\r?\n/g, '<br />');
    $('#output').html(textareaText);
    
</script>
</body>

</html>
