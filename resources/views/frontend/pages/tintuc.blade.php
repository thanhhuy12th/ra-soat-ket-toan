<!DOCTYPE html>
<html lang="en">
<head>
	@include('frontend.blocks.head')
</head>
<body class="home custom-background wp-custom-logo homepage-template">

    @include('frontend.blocks.menu')
    <div id="page" class="site">
         
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <!-- tìm kiếm -->
                        <form action="{{url('tim-kiem')}}" method="POST">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" maxlength="50" name="tukhoa" placeholder="{!! $lang["search"] !!}" aria-describedby="basic-addon1">
                            <div class="input-group-prepend">
                                <button type="submit" class="fa fa-search"></button>
                            </div>
                        </div>
                        </form>
                        <!-- Tin tức hiển thị 6 tin tức -->
                        <div class="card mb-3">
                        	@foreach($news as $tt)
                            <div class="row no-gutters mb-4">
                                <div class="col-md-4">
                                    <img src="{{asset('uploads/news/'.$tt->image)}}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body ">
                                        <h5 class="card-title">{{$tt->title}}</h5>
                                        <p class="card-text">{{$tt->intro}}</p>
                                        <p class="card-text"><small class="text-muted">{{$tt->created_at}}</small></p>
                                        <div class="text-right">
                                            <a href="{{route('tin-tuc-detail',$tt->alias)}}" class="btn btn-primary">{!! $lang["read_more"] !!}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>   
                    </div>
                   <div class="col-md-3 col-lg-3 col-xl-3">                       
                        <ul class="list-group mb-4">
                                <li class="list-group-item">Danh mục tin tức</li>
                                    @foreach($dmtt as $item)
                                        <li class="list-group-item"><a href="{{route('danh-muc-tin-tuc',[$item->id,$item->alias])}}">{{$item->name}}</a></li>
                                    @endforeach
                        </ul>
                        <div class="fixpo">
                        <ul class="list-group mb-4">
                            <li class="list-group-item">{!! $lang["featured_article"] !!}</li>
                            @foreach($hot as $tt)
                                <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                            @endforeach
                        </ul>
                       </div>
                    </div>
                </div>

                <!-- Phân trang -->
                <div class="row" />
                    <nav aria-label="..." />
                    	<ul class="pagination" />
						    @if( $news->currentPage() != 1)
						    	<li class="page-item"><a class="page-link" href="{!! $news->url($news->currentPage() - 1) !!}">Previous</a></li>
						    @endif
						    @for($i=1; $i <= $news->lastPage(); $i= $i+1)
						    	<li class="page-item {!! ($news->currentPage() == $i )? 'active': '' !!}">
						        	<a class="page-link" href="{!! $news->url($i) !!}">{!! $i !!}</a>
						    	</li>
						   @endfor
						   @if($news->currentPage() != $news->lastPage())
						    	<li class="page-item"><a class="page-link" href="{!! $news->url($news->currentPage() + 1) !!}">Next</a></li>
						    @endif
						</ul>
                    </nav>
                </div>
            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>
</body>
  
</html>