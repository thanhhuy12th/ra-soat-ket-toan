<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')

    <style>
@import url('https://fonts.googleapis.com/css?family=Tajawal');
@import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

section{
    padding: 60px 0;
}

#accordion-style-1 h1,
#accordion-style-1 a{
    color:#007b5e;
}
#accordion-style-1 .btn-link {
    font-weight: 400;
    color: #0079AD;
    background-color: transparent;
    text-decoration: none !important;
    font-size: 16px;
    font-weight: bold;
    padding-left: 25px;
}

#accordion-style-1 .card-body {
    border-top: 2px solid #007b5e;
}

#accordion-style-1 .card-header .btn.collapsed .fa.main{
    display:none;
}

#accordion-style-1 .card-header .btn .fa.main{
    background: #007b5e;
    padding: 13px 11px;
    color: #ffffff;
    width: 35px;
    height: 41px;
    position: absolute;
    left: -1px;
    top: 10px;
    border-top-right-radius: 7px;
    border-bottom-right-radius: 7px;
    display:block;
} 
</style>
</head>

<body class="home custom-background wp-custom-logo homepage-template">
    <?php
    function changecolor($str, $tukhoa)
    {
        return str_replace($tukhoa, "<span style='color:red ;'>$tukhoa</span>", $str);
    }
    ?>
    @include('frontend.blocks.menu')
    <div id="page" class="site">
        {{-- @include('frontend.pages.banner') --}}
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <!-- Tin tức hiển thị 6 tin tức -->
                        <div class="search-results mb-3">
                            <h3>Tìm kiếm : {{ $tukhoa }}</h3>
                        </div>
                           <!-- Accordion -->
                        <div class="container-fluid bg-gray" id="accordion-style-1">
                            <div class="container">
                                <section>
                                    <div class="row">
                                        <div class="col-12 mx-auto">
                                            <div class="accordion" id="accordionExample">
                                                @foreach($faq as $item)
                                                <div class="card">
                                                    <div class="card-header" id="headingOne{{$item->id}}">
                                                        <h5 class="mb-0">
                                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne{{$item->id}}" aria-expanded="true" aria-controls="collapseOne">
                                                     <i class="fa fa-angle-double-right mr-3"></i>{{$item->title}}
                                                    </button>
                                                  </h5>
                                                    </div>
                                                    <div id="collapseOne{{$item->id}}" class="collapse fade" aria-labelledby="headingOne{{$item->id}}" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            {!! $item->content !!}
                                                             @if(isset($faqfile))
                                                            @foreach($faqfile as $ffe)
                                                                @if($ffe->id_faq == $item->id)
                                                                 <div  style="padding: 5px 10px;">
                                                                    <a style="color: #0079AD;text-decoration: underline;" href="{{$ffe->link}}" target="_blank"><li>{{$ffe->tenfile}}<i style="float: right;" ></i></li></a>
                                                                </div>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        </div>                                
                                                    </div>
                                                </div>
                                                 @endforeach                                                
                                            </div>
                                        </div>  
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- .// Accordion -->                        
                    </div>
                    
                </div>

                <!-- Phân trang -->
                <div class="row" />
                    <div class="col-4"></div>
                    <div class="col-4">
                        <nav aria-label="..." />                              
                            <ul class="pagination" >
                                 @if( $faq->currentPage() != 1)
                                    <li class="page-item">
                                      <a class="page-link" href="{!! $faq->url($faq->currentPage() - 1) !!}" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                    </li>
                                 @endif
                                 @for($i=1; $i <= $faq->lastPage(); $i= $i+1)
                                    <li class="page-item {!! ($faq->currentPage() == $i )? 'active': '' !!}">
                                        <a class="page-link" href="{!! $faq->url($i) !!}">{!! $i !!}</a></li>      
                                @endfor
                                @if($faq->currentPage() != $faq->lastPage())
                                    <li class="page-item">
                                      <a class="page-link" href="{!! $faq->url($faq->currentPage() + 1) !!}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </li>
                                 @endif
                            </ul>
                        </nav>
                    </div>
                    <div class="col-4"></div>                              
            </div>               
        </div>
    </div>
    @include('frontend.blocks.footer')
    </div>
</body>

</html>