<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.blocks.head')
</head>
<body class="home custom-background wp-custom-logo homepage-template">

    @include('frontend.blocks.menu')
    <div id="page" class="site">
        
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <!-- tìm kiếm -->
                        <form action="{{url('tim-kiem')}}" method="POST">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" maxlength="50" name="tukhoa" placeholder="{!! $lang["search"] !!}" aria-describedby="basic-addon1">
                            <div class="input-group-prepend">
                                <button type="submit" class="fa fa-search"></button>
                            </div>
                        </div>
                        </form>
                        <!-- Tin tức hiển thị 6 tin tức -->
                        <div class="card mb-3">
                            @foreach($banggia as $item)
                            <div class="row no-gutters mb-4">
                                
                                <div class="col-md-12">
                                    <div class="card-body ">
                                        <h5 class="card-title" >{{$item->title}}</h5>
                                        <div class="row">
                                            <div class="col-md-8">                                                
                                               <input type="hidden" id="motabreak" value="{!!$item->description!!}">
                                               <p id="output"></p>
                                            </div>                                            
                                        </div>
                                        <div class="text-right pt-4">
                                            <a href="{{route('bang-gia-detail',$item->alias)}}" class="btn btn-primary">{!! $lang["read_more"] !!}</a>
                                        </div>      
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>   
                    </div>
                   <div class="col-md-3 col-lg-3 col-xl-3">
                        <ul class="list-group mb-4">
                            <li class="list-group-item">{!! $lang["featured_article"] !!}</li>
                            @foreach($hot as $tt)
                                <li class="list-group-item" ><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>                              
                            @endforeach
                        </ul>
                    </div>
                </div>

                <!-- Phân trang -->
                 <div class="row" />
                    <div class="col-4"></div>
                    <div class="col-4">
                         <nav aria-label="..." />                              
                            <ul class="pagination" >
                                 @if( $banggia->currentPage() != 1)
                                    <li class="page-item">
                                      <a class="page-link" href="{!! $banggia->url($banggia->currentPage() - 1) !!}" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                    </li>
                                 @endif
                                 @for($i=1; $i <= $banggia->lastPage(); $i= $i+1)
                                    <li class="page-item {!! ($banggia->currentPage() == $i )? 'active': '' !!}">
                                        <a class="page-link" href="{!! $banggia->url($i) !!}">{!! $i !!}</a></li>      
                                @endfor
                                @if($banggia->currentPage() != $banggia->lastPage())
                                    <li class="page-item">
                                      <a class="page-link" href="{!! $banggia->url($banggia->currentPage() + 1) !!}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </li>
                                 @endif
                            </ul>
                        </nav>
                    </div>
                    <div class="col-4"></div>                               
                </div>
            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>
<script type="text/javascript">
    let textareaText = $('#motabreak').val();
    textareaText = textareaText.replace(/\r?\n/g, '<br />');
    $('#output').html(textareaText);
    
</script>
</body>
  
</html>