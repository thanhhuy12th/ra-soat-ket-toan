<!DOCTYPE html>
<html lang="en">
<head>
	@include('frontend.blocks.head')
</head>
<body class="home page-id-2568 custom-background wp-custom-logo homepage-template">
	@include('frontend.blocks.menu')
    <div id="page" class="site">
        <div class="page-content pt-5">
            <div class="gridContainer">
                <div id="post-1380" class="post-1380 page type-page status-publish hentry">
                    <div>
                        <article class="about-us">
                            <h2>{!! $lang['aboutus_title'] !!}</h2>
                        	<p>{!! $gioithieu->content !!}</p>
                           
                        </article>
                        <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator_gioithieucongty" data-width="" data-numposts="10"></div>
                    </div>

                    
                </div>
            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>

</body>

</html>