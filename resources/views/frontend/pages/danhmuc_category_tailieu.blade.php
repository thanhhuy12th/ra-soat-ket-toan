<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.blocks.head')
</head>

<body data-rsssl=1 class="home page-id-2568 custom-background wp-custom-logo homepage-template">

   @include('frontend.blocks.menu')
    <div id="page" class="site">
        {{-- @include('frontend.pages.banner') --}}
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <!-- tìm kiếm -->
                        {{-- <form action="" method="POST">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" maxlength="50" placeholder="{!! $lang["search"] !!}" aria-describedby="basic-addon1">
                            <div class="input-group-prepend">
                                 <button type="submit" class="fa fa-search"></button>
                            </div>
                        </div>
                        </form> --}}
                        <!-- Mỗi danh mục tài liệu hiển thị 6 tài liệu -->

                        <table class="table table-borderless mb-3">
                            <h3>{!! $lang["document"] !!}</h3>
                            <thead>
                                <tr>
                                    <th scope="col">{!! $lang["form_title"] !!}</th>
                                    <th scope="col">{!! $lang["form_document"] !!}</th>
                                    <th scope="col" col>{!! $lang["form_downloads"] !!}</th>
                                </tr>
                            </thead>
                            <tody>
                                @foreach($danhmuc_category_tailieu as $tl)
                                @php
                                    $data1 = $tl->file;
                                    $splittedstring=explode("||",$data1);   
                                @endphp
                                <tr>
                                    <td scope="row">{{$tl->title}}</td>
                                    <td>{{$tl->short_description}}</td>
                                   <td>
                                        @foreach($splittedstring as $fl)
                                        <a href="{{ asset('uploads/file/'.$fl)}}" download><br /> {{$fl}}</a>
                                        @endforeach
                                        
                                    </td> 
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <ul class="list-group mb-4">
                            <li class="list-group-item">Danh mục</li>
                            <?php 
                                $category_cha = select_category_cha();
                            ?>  
                            @foreach($category_cha as $cha)
                            <li class="list-group-item has-children">
                                <a href="{{route('danh-muc-tai-lieu',[$cha->id,$cha->alias])}}">{{$cha->name}}</a><i class="ml-1 fa fa-sort-down"></i>                              
                                <ul class="list-sub-item list-group">
                                    <?php 
                                        $category_con = select_category_con($cha->id);
                                    ?>  
                                    @foreach($category_con as $con)
                                    <li class="list-group-item has-children no-border"><a href="{{route('danh-muc-tai-lieu',[$con->id,$con->alias])}}">{{$con->name}}</a><i class="ml-1 fa fa-sort-down"></i>
                                        <ul class="list-sub-item list-group">
                                            <?php 
                                                $category_nho = select_category_con($con->id);
                                            ?>  
                                            @foreach($category_nho as $nho)
                                                <li class="list-group-item has-children no-border"><a href="{{route('danh-muc-tai-lieu',[$nho->id,$nho->alias])}}">{{$nho->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                        <ul class="list-group mb-4">
                            <li class="list-group-item">Bài viết nổi bật</li>
                            @foreach($hot as $tt)
                                <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                            @endforeach
                        </ul>
                        
                       {{--  <ul class="list-group mb-4">
                            <li class="list-group-item">Bài viết gần nhất</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul> --}}

                    </div>
                </div>

                <!-- Phân trang -->
                <div class="row">
                    <nav aria-label="...">
                        <ul class="pagination" />
                            @if( $danhmuc_category_tailieu->currentPage() != 1)
                                <li class="page-item"><a class="page-link" href="{!! $danhmuc_category_tailieu->url($danhmuc_category_tailieu->currentPage() - 1) !!}">Previous</a></li>
                            @endif
                            @for($i=1; $i <= $danhmuc_category_tailieu->lastPage(); $i= $i+1)
                                <li class="page-item {!! ($danhmuc_category_tailieu->currentPage() == $i )? 'active': '' !!}">
                                    <a class="page-link" href="{!! $danhmuc_category_tailieu->url($i) !!}">{!! $i !!}</a>
                                </li>
                           @endfor
                           @if($danhmuc_category_tailieu->currentPage() != $danhmuc_category_tailieu->lastPage())
                                <li class="page-item"><a class="page-link" href="{!! $danhmuc_category_tailieu->url($danhmuc_category_tailieu->currentPage() + 1) !!}">Next</a></li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>
    </div>
</body>

</html>