<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.blocks.head')

    <style>
.btn {
  background-color: DodgerBlue;
  border: none;
  color: white;
  padding: 0px 10px;
  cursor: pointer;
  font-size: 14px;
}

.btn:hover {
  background-color: RoyalBlue;
}
.noselect {
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
</head>
<body class="home custom-background wp-custom-logo homepage-template">
     @include('frontend.blocks.menu')
    <div id="page" class="site">
       
        <div class="content">
            <div class="container">               
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9 noselect" >
                        
                        <div class="new-conntent mt-4">
                            <h2>{!! $bieumaudetail->title !!}</h2> <br />
                           



                                    <p>Download {!! $lang['form'] !!}: 
                            
                                @php 
                                    $splittedstring=explode("||",$bieumaudetail->file); 
                                @endphp
                                @for($i =0; $i<count($splittedstring)-1; $i++)
                                  <a href="{{ asset('uploads/file/'.$splittedstring[$i])}}" download>
                                    <button class="btn mr-2"><i class="fa fa-download" ></i>Download</button></a>
                                @endfor
                        
                         
                           <p>{!! $bieumaudetail->description !!}</p>
                                
                        </div>
                        <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator_{!! $bieumaudetail->id !!}" data-width="" data-numposts="10"></div>
                    </div>

                    <div class="col-md-3 col-lg-3 col-xl-3 ">
                        <ul class="list-group mb-4">
                                <li class="list-group-item">{!! $lang["caregory_news"] !!}</li>
                                @foreach($dmtt as $dm)
                                    <li class="list-group-item"><a href="{{route('danh-muc-tin-tuc',[$dm->id,$dm->alias])}}">{{$dm->name}}</a></li>
                                @endforeach
                            </ul>
                        <div class="fixpo">

                            <ul class="list-group mb-4">
                                <li class="list-group-item">{!! $lang["featured_article"] !!}</li>
                                @foreach($hot as $tt)
                                     <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
       @include('frontend.blocks.footer')
    </div>

</body>

</html>