<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body class="home custom-background wp-custom-logo homepage-template">
    <?php
    function changecolor($str, $tukhoa)
    {
        return str_replace($tukhoa, "<span style='color:red ;'>$tukhoa</span>", $str);
    }
    ?>
    @include('frontend.blocks.menu')
    <div id="page" class="site">
        {{-- @include('frontend.pages.banner') --}}
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <!-- Tin tức hiển thị 6 tin tức -->
                        <div class="search-results mb-3">
                            <h3>Tìm kiếm : {{ $tukhoa }}</h3>
                        </div>
                        <div class="card mb-3">
                            @foreach($tintuc as $tt)
                            <div class="row no-gutters mb-4">
                                <div class="col-md-4">
                                    <img src="{{asset('uploads/news/'.$tt->image)}}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body ">
                                        <h5 class="card-title">{!! changecolor($tt->title, $tukhoa) !!} </h5>
                                        <p class="card-text">{!! changecolor($tt->intro, $tukhoa) !!} </p>
                                        <p class="card-text"><small class="text-muted">{{$tt->created_at}}</small></p>
                                        <div class="text-right">
                                            <a href="{{route('tin-tuc-detail',$tt->alias)}}" class="btn btn-primary">Đọc thêm</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <ul class="list-group mb-4">
                            <li class="list-group-item">Bài viết nổi bật</li>
                            @foreach($hot as $tt)
                            <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                            @endforeach
                        </ul>
                        {{-- <ul class="list-group mb-4">
                            <li class="list-group-item">Danh mục tin tức</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul>
                        <ul class="list-group mb-4">
                            <li class="list-group-item">Bài viết gần nhất</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul> --}}

                    </div>
                </div>

                <!-- Phân trang -->
                <div class="row" />
                <nav aria-label="..." />
                <ul class="pagination" />
                @if( $tintuc->currentPage() != 1)
                <li class="page-item"><a class="page-link" href="{!! $tintuc->url($tintuc->currentPage() - 1) !!}">Previous</a></li>
                @endif
                @for($i=1; $i <= $tintuc->lastPage(); $i= $i+1)
                    <li class="page-item {!! ($tintuc->currentPage() == $i )? 'active': '' !!}">
                        <a class="page-link" href="{!! $tintuc->url($i) !!}">{!! $i !!}</a>
                    </li>
                    @endfor
                    @if($tintuc->currentPage() != $tintuc->lastPage())
                    <li class="page-item"><a class="page-link" href="{!! $tintuc->url($tintuc->currentPage() + 1) !!}">Next</a></li>
                    @endif
                    </ul>
                    </nav>
            </div>
        </div>
    </div>
    @include('frontend.blocks.footer')
    </div>
</body>

</html>