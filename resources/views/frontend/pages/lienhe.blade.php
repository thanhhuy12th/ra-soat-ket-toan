<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body class="home custom-background wp-custom-logo homepage-template">
    @include('frontend.blocks.menu')
    <div id="page" class="site">
        
        <div class="content mt-5 mb-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xl-6">
                                <form class="ct-form" action="{{route('lien-he')}}" method="POST" />
                                @csrf
                                    <h6>{!! $lang["receive_contact_us"] !!}</h6>
                                    <div class="form-group">
                                        <label>{!! $lang["fullname"] !!}</label>
                                        <input type="text" class="form-control" maxlength="50" name="name" placeholder="{!! $lang["ph_fullname"] !!}" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>{!! $lang["email"] !!}</label>
                                        <input type="email" class="form-control" maxlength="50" name="email" placeholder="{!! $lang["ph_email"] !!}" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>{!! $lang["phone"] !!}</label>
                                        <input type="number" class="form-control" maxlength="50" name="phone" placeholder="{!! $lang["ph_phone"] !!}" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>{!! $lang["message"] !!}</label>
                                        <textarea rows="1" class="form-control" name="content" placeholder="{!! $lang["ph_message"] !!}"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="g-recaptcha" data-sitekey="6Ldsp60UAAAAAI-lfsDSsaLGnD_O9xRXW9-YOH3q"></div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">{!! $lang["send"] !!}</button>
                                </form>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-6">
                                <div class="ct-form mt-3">
                                    <h6>{!! $lang["title_contact_us"] !!}</h6>
                                    <p>{!! $lang["work_time"] !!}: Thứ 2- Thứ 7 - 8:00 AM đến 5:00 PM</p>
                                    <p>{!! $lang["adress"] !!}: {!! $address->content !!}</p>
                                    <p>{!! $lang["phone"] !!}: {!! $hotline1->content !!} - {!! $phonename1->name_phone !!}</p>
                                    <p>{!! $lang["email"] !!}: {!! $email->content !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="map">
                            {!! $map->content !!}
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <ul class="list-group mb-4">
                            <li class="list-group-item">{!! $lang["featured_article"] !!}</li>
                            @foreach($hot as $tt)
                                <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                            @endforeach
                        </ul>
                       {{--  <ul class="list-group mb-4">
                            <li class="list-group-item">Bài viết gần nhất</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul> --}}

                    </div>
                </div>


            </div>
        </div>
        @include('frontend.blocks.footer')
    </div>
</body>

</html>