<div class="header-wrapper">
    <div class='header-homepage  color-overlay' style='background-image:url({{asset('/uploads/slider/'. $slider[0]->image)}}); min-height:' data-parallax-depth='20'>
        <div class="header-description gridContainer content-on-center">
            <div class="row header-description-row">
                <div class="header-content header-content-centered">
                    <div class="align-holder">
                        <h1 class="heading8">{!! $lang["banner_text"] !!}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="header-separator header-separator-bottom "><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="svg-white-bg" d="M737.9,94.7L0,0v100h1000V0L737.9,94.7z" />
        </svg></div> -->
</div>