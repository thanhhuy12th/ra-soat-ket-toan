<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.blocks.head')
      
<style>
@import url('https://fonts.googleapis.com/css?family=Tajawal');
@import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

section{
    padding: 20px 0;
}

#accordion-style-1 h1,
#accordion-style-1 a{
    color:#007b5e;
}
#accordion-style-1 .btn-link {
    font-weight: 400;
    color: #0079AD;
    background-color: transparent;
    text-decoration: none !important;
    font-size: 16px;
    font-weight: bold;
    padding-left: 25px;
}

#accordion-style-1 .card-body {
    border-top: 2px solid #007b5e;
}

#accordion-style-1 .card-header .btn.collapsed .fa.main{
    display:none;
}

#accordion-style-1 .card-header .btn .fa.main{
    background: #007b5e;
    padding: 13px 11px;
    color: #ffffff;
    width: 35px;
    height: 41px;
    position: absolute;
    left: -1px;
    top: 10px;
    border-top-right-radius: 7px;
    border-bottom-right-radius: 7px;
    display:block;
} 
</style>

</head>
<body data-rsssl=1 class="home page-id-2568 custom-background wp-custom-logo homepage-template">
   @include('frontend.blocks.menu')
    <div id="page" class="site">
       
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <!-- tìm kiếm -->

                        <form action="{{url('tim-kiem-hoi-dap')}}" method="POST" id="search">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" class="form-control test" name="tukhoa" maxlength="50" placeholder="{!! $lang["search"] !!}" aria-describedby="basic-addon1">
                            <div class="input-group-prepend">
                                <button type="submit" id="submitButton" class="fa fa-search"></button>
                            </div>
                        </div>
                        <p>{!! $lang["Search_suggestions"] !!}: 
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["pit"] !!}">{!! $lang["pit"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["cit"] !!}">{!! $lang["cit"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["invoices"] !!}">{!! $lang["invoices"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["fct"] !!}">{!! $lang["fct"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["accouting"] !!}">{!! $lang["accouting"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["auditing"] !!}">{!! $lang["auditing"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["social_insuarance"] !!}">{!! $lang["social_insuarance"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["labour"] !!}">{!! $lang["labour"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["salary"] !!}">{!! $lang["salary"] !!}</a>,
                            <a href="#" onclick="onSubmit(this)" data-c="{!! $lang["order"] !!}">{!! $lang["order"] !!}</a>.
                        </p>
                        </form>

                        <!-- Accordion -->
                        <div class="container-fluid bg-gray" id="accordion-style-1">
                            <div class="container">
                                <section>
                                    <div class="row">
                                        <div class="col-12 mx-auto">
                                            <div class="accordion" id="accordionExample">
                                                @foreach($faq as $item)
                                                <div class="card">
                                                    <div class="card-header" id="headingOne{{$item->id}}">
                                                        <h5 class="mb-0">
                                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne{{$item->id}}" aria-expanded="true" aria-controls="collapseOne">
                                                     <i class="fa fa-angle-double-right mr-3"></i>{{$item->title}}
                                                    </button>
                                                  </h5>
                                                    </div>
                                                    <div id="collapseOne{{$item->id}}" class="collapse fade" aria-labelledby="headingOne{{$item->id}}" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            {!! $item->content !!}
                                                             @if(isset($faqfile))
                                                            @foreach($faqfile as $ffe)
                                                                @if($ffe->id_faq == $item->id)
                                                                 <div  style="padding: 5px 10px;">
                                                                    <a style="color: #0079AD;text-decoration: underline;" href="{{$ffe->link}}" target="_blank"><li>{{$ffe->tenfile}}<i style="float: right;" ></i></li></a>
                                                                </div>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        </div>                                
                                                    </div>
                                                </div>
                                                 @endforeach                                                
                                            </div>
                                        </div>  
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- .// Accordion -->

                        <!-- Phân trang -->
                        <div class="row" />
                            <div class="col-4"></div>
                            <div class="col-4">
                                 <nav aria-label="..." />                              
                                    <ul class="pagination" >
                                         @if( $faq->currentPage() != 1)
                                            <li class="page-item">
                                              <a class="page-link" href="{!! $faq->url($faq->currentPage() - 1) !!}" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                              </a>
                                            </li>
                                         @endif
                                         @for($i=1; $i <= $faq->lastPage(); $i= $i+1)
                                            <li class="page-item {!! ($faq->currentPage() == $i )? 'active': '' !!}">
                                                <a class="page-link" href="{!! $faq->url($i) !!}">{!! $i !!}</a></li>      
                                        @endfor
                                        @if($faq->currentPage() != $faq->lastPage())
                                            <li class="page-item">
                                              <a class="page-link" href="{!! $faq->url($faq->currentPage() + 1) !!}" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                              </a>
                                            </li>
                                         @endif
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-4"></div>                               
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <ul class="list-group mb-4">
                            <li class="list-group-item">{!! $lang["featured_article"] !!}</li>
                            @foreach($hot as $tt)
                               <li class="list-group-item"><a href="{{route('tin-tuc-detail',$tt->alias)}}">{{$tt->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    
                </div>
                <div class="row">
                    <form method="POST" action="{{route('faq-contact')}}" style="width:50%">
                        @csrf
                        <h3>{!! $lang['additional_advice'] !!}</h3>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{!! $lang["email"] !!} *</label>
                            <input type="email" class="form-control" maxlength="50" id="email" name="email"  placeholder="{!! $lang["ph_email"] !!}" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">{!! $lang["phone"] !!} *</label>
                            <input type="tel" class="form-control" maxlength="50" id="password" name="phone" placeholder="{!! $lang["ph_phone"] !!}" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">{!! $lang["message"] !!} *</label>
                            <textarea class="form-control" id="question" maxlength="50" name="content" rows="3" placeholder="{!! $lang["faq_message"] !!}" required=""></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">{!! $lang["faq_send"] !!}</button>
                    </form>
                </div>
             
            </div>   
        </div>
             @include('frontend.blocks.footer')
    </div>
<script type="text/javascript">
    function onSubmit(elm) {
            var text= elm.getAttribute('data-c');
            $('.test').val(text);
            $( "#search" ).submit();
        }
     

</script>
</body>

</html>