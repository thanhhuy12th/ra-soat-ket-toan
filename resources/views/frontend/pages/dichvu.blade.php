<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.blocks.head')
</head>
<body class="home page-id-2568 custom-background wp-custom-logo homepage-template">
    @include('frontend.blocks.menu')
    <div id="page" class="site">
       <div class="page-content pt-5">
    <div class="container">
        <div class="page type-page status-publish hentry">
            <div class="home-icon mt-3 mb-3 pt-2 pb-2">
                <div class="row">
                    @foreach($service as $dv)
                    <div class="col-md-4 mb-1">
                        <div class="icon-image"><i class="fa fa-money"></i></div>
                        <div class="icon-title">{{$dv->name}}</div>
                        <div class="icon-content">
                            {{$dv->content}}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div id="portfolio-2" style="background-color: #ffffff;">
            <div class="portfolio-full-section mb-3">
                @foreach($service as $dv)
                <div class="portfolio-full-projectco cp4cols cp4cols-tablet">
                    <div class="contentswap-effect ContentSwap103" hover-fx="ContentSwap103" id="ContentSwap103">
                        <div class="ContentSwap103_content initial-image"><img class="portfolio-full-projectimg" data-size="1200x800" src="{{asset('uploads/service/'. $dv->image)}}"> </div>
                        <div class="overlay" style="display: block; opacity: 1;"></div>
                        <div class="swap-inner" style="opacity: 1; display: block;">
                            <div class="ContentSwap103-center">
                                <h4 class="protfolio-full-itemtitle">{{$dv->name}}</h4><a class="button yellow" href="{{$dv->link}}" target="_self">{!! $lang["service_check"] !!}</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
        @include('frontend.blocks.footer')
    </div>
</body>

</html>