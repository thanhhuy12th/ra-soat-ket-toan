@extends('admin.master')
@section('title','Thêm Admin')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('admin.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.add_admin')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.fullname_admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="fullname" class="form-control" value="{{old('fullname')}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.username_admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="username" class="form-control" value="{{old('username')}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Email field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.email_admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="email" name="email" class="form-control" id="email" value="{{old('email')}}" />
                        </div>
                    </div>
                    <!-- /email field -->

					<!-- Number -->
                    <div class="form-group row">
						<label class="col-form-label col-lg-2">{{trans('template.phone_admin')}}<span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="phone" class="form-control" value="{{old('phone')}}" />
						</div>
					</div>
					<!-- /number -->

                    <!-- Password field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.password_admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="password" name="password" id="password" class="form-control"/>
                        </div>
                    </div>
                    <!-- /password field -->

                    <!-- Repeat password -->
                    <div class="form-group row">
                       <label class="col-form-label col-lg-2">{{trans('template.repasswork_admin')}}<span class="text-danger">*</span></label>
                       <div class="col-lg-9">
                           <input type="password" name="password_confirmation" class="form-control" />
                       </div>
                   </div>
                    <!-- /repeat password -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.image_admin')}}</label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" />
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Basic radio group -->
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">{{trans('template.level_admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
			               <select name="role" class="form-control" />
			                	<option value="1">Admin</option>
			                	<option value="2">Admin đăng bài</option>
			               </select>
			             </div>
                    </div>
                    <!-- /basic radio group -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.create')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection