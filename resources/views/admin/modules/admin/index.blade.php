@extends('admin.master') 
@section('title','Danh Sách UserAdmin') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.list_admin')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('admin.create')}}">
                <button type="button" class="btn btn-primary" class="">Thêm quản trị viên</button>
            </a>
        </div>
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    
                    <th>#</th>
                    <th>UserName</th>
                    <th>FullName</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Quyền</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($admin as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->username}}</td>
                    <td>{{$item->fullname}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->phone}}</td>
                    <td>
                    	@if ($item->role == 1)
    						<span class="badge badge-danger">Admin</span>
						@elseif ($item->role == 2)
    						<span class="badge badge-success">Admin đăng bài</span>
						@endif
					</td>
                    
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">                      
                        <div class="list-icons">
                            <a href="{{ route('admin.edit',['admin' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa thành viên"><i class="icon-pencil7"></i></a> &nbsp; 
                            <a href="{{ route('admin.destroy',['admin' => $item->id]) }}" onclick="return acceptDelete()" class="list-icons-item text-danger-600" title="Xóa thành viên"><i class="icon-trash"></i></a>
                            {{-- <a href="#" class="list-icons-item text-teal-600"><i class="icon-cog6"></i></a> --}}
                        </div>



                  </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
</div>
@endsection