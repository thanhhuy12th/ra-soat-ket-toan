@extends('admin.master')
@section('title','Thêm Bảng giá')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="form-validate-jquery" action="{{route('banggia.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.add_price_quote')}}</legend>

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"><span class="text-danger">*</span>{{trans('template.title_price_quote')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="title" class="form-control" value="{{old('title')}}" required="" />
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic alias input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-2">{{trans('template.alias_price_quote')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic alias input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"><span class="text-danger">*</span>{{trans('template.description_price_quote')}}</label>
                        <div class="col-lg-9">
                            <textarea rows="6" name="description" class="form-control col-md-12 col-xs-12" >{{old('description')}}</textarea>
                            <p id="output2"></p>
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"><span class="text-danger">*</span>{{trans('template.position_price_quote')}}</label>
                        <div class="col-lg-9">
                            <input type="number" name="position" class="form-control" value="{{old('position')}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->
                </fieldset>
                <div class="v container mb-3">
                        <fieldset class="sub-item-v"><h4>Bảng phụ 1</h4>
                                <i class="icon-cross pull-right absolute "  onclick="delete_item(this)"> </i> 

                                <div class="form-group row ">
                                    <input id="content" type="text" name="content[]" class="form-control col-lg-6 mr-3" value="{{old('content')}}" placeholder="Nội dung">
                                    <input id="price" type="number" name="price[]" class="form-control col-lg-2 mr-3" value="{{old('price')}}" placeholder="Giá">
                                    <input id="position" type="number" name="position_detail[]" class="form-control col-lg-2 mr-3" value="{{old('position')}}" placeholder="Vị trí">
                                </div> 
                        </fieldset>
                </div>

                <script>
                    function delete_item(elm) {
                        $(elm).parent().remove();
                    }
                    var count = 1;
                    $(document).ready(function($) {
                        $('.plusbtn').on('click', function() {
                            count++;
                            var html =
                                '<fieldset class="sub-item-v">'  + '<h4>Bảng phụ ' + count + '</h4>' +
                                '<i class="icon-cross pull-right absolute "  onclick="delete_item(this)"> </i> </div> '   +
                                '<div class="form-group row ">'  +
                                '<input id="content"'  + 'type="text"'  + 'name="content[]"'  + 'class="form-control col-lg-6 mr-3"' + 
                                'placeholder="Nội dung">' +
                                '<input id="price"'  + 'type="number"'  + 'name="price[]"'  + 'class="form-control col-lg-2 mr-3"' + 
                                'placeholder="Giá">' + 
                                '<input id="position"' + 'type="number"'  + 'name="position_detail[]"'  + 'class="form-control col-lg-2 mr-3"' + 
                                'placeholder="Vị trí"> </div> </fieldset>' ;
                            $('.v').append(html);
                        })
                    });    
                </script>

                <div class="d-flex justify-content-center align-items-center">
                    <div class="btn btn-light plusbtn">{{trans('template.add_price')}}<i class="icon-plus2 ml-2" ></i></div>
                    <button type="reset" class="btn btn-light ml-3" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.create')}}<i class="icon-paperplane ml-2"></i></button> 
                </div> 
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection