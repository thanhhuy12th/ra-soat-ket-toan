@extends('admin.master') 
@section('title','Danh Sách Service') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.list_service')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
         <div class="card-body">
            <a href="{{route('dichvu.create')}}">
                <button type="button" class="btn btn-primary" class="">Thêm Service</button>
            </a>
        </div>
        <form action="{{route('dichvu.destroy',['action' => 'delete'])}}" />
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>#</th>
                    <th>Tên dịch vụ</th>
                    <th>Ngôn ngữ</th>
                    <th>Ảnh</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($service as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->name}}</td>
                    <td>
                        @if ($item->language == "VN")
                            <span class="badge badge-primary">VN</span>
                        @elseif ($item->language == "EN")
                            <span class="badge badge-success">EN</span>
                        @endif
                        
                    </td>
                    <td>
                         <img src="{{asset('uploads/service/'.$item ->image)}}" width="15%">
                    </td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('dichvu.edit',['dichvu' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa dịch vụ"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.delete')}}</button><br/><br>              
        </form> 
    </div>
    <!-- /page length options -->
</div>
@endsection