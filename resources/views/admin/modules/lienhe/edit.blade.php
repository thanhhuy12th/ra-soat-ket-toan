@extends('admin.master')
@section('title','Chỉnh Sửa Liên hệ')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('lienhe.update',['id'=> $lienhe->id])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Add Contact')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.name_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" maxlength="50" class="form-control" value="{{$lienhe->name}}" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Email field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.email_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="email" name="email" maxlength="50" class="form-control" id="email" value="{{$lienhe->email}}" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /email field -->

					<!-- Number -->
                    <div class="form-group row">
						<label class="col-form-label col-lg-2">{{trans('template.phone_contact')}}<span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="phone" maxlength="50" class="form-control" value="{{$lienhe->phone}}" />
						</div>
					</div>
					<!-- /number -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.address_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="address" maxlength="50" class="form-control" value="{{$lienhe->address}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->


                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.content_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" name="content" class="form-control col-md-12 col-xs-12">{{$lienhe->content}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection