{{-- @extends('admin.master')
@section('title','Thêm Liên hệ')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('lienhe.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.add_contact')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.name_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" maxlength="50" name="name" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Email field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.email_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="email" maxlength="50" name="email" class="form-control" id="email" />
                        </div>
                    </div>
                    <!-- /email field -->

					<!-- Number -->
                    <div class="form-group row">
						<label class="col-form-label col-lg-3">{{trans('template.phone_contact')}}<span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" maxlength="50" name="phone" class="form-control" />
						</div>
					</div>
					<!-- /number -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.address_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" maxlength="50" name="address" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->


                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.content_contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" name="content" class="form-control col-md-12 col-xs-12"></textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.create')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection --}}