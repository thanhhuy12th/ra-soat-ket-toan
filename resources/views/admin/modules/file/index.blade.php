@extends('admin.master') 
@section('title','Danh Sách File') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.list_file')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('file.create')}}">
                <button type="button" class="btn btn-primary" class="">Thêm File</button>
            </a>
        </div>
        <form action="{{route('file.destroy',['action' => 'delete'])}}" />
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>#</th>
                    <th>Tiêu đề</th>
                    <th>Danh mục</th>
                    <th>Kiểu file</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($file as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->title}}</td>
                     <td>
                        @if(empty($item->id_category))
                            {{$item->name}}
                            @else
                                @foreach($opt_parent as $potionParent)
                                    @if($potionParent->id == $item->id_category)
                                        {{$potionParent->name}}
                                    @endif
                                @endforeach
                        @endif
                    </td>
                    <td>
                         @if ($item->type == 1)
                            <span class="badge badge-primary">Biểu mẫu</span>
                        @elseif ($item->type == 2)
                            <span class="badge badge-success">Tài liệu</span>
                        @endif
                    </td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('file.edit',['file' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa file"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.delete')}}</button><br/><br>              
        </form> 
    </div>
    <!-- /page length options -->
</div>
@endsection