@extends('admin.master')
@section('title','Edit File')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('file.update',['id' => $file->id])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.edit_file')}}</legend>
                    
                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.category_parent')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                           <select name="id_category" class="form-control">
                               {{-- <option value="0">ROOT</option> --}}
                                @for($i = 0; $i < count($cate_option); $i++)
                                <option value="{!!$cate_option[$i]['id']!!}" {{ $file->id_category == $cate_option[$i]['id'] ? "selected" : "" }}>{!!$cate_option[$i]['name']!!}</option>
                               @endfor
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                      <!-- Basic select option input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.type_file')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                             <select name="type" class="form-control" />
                             <option value="1" {{($file->type == 1)? 'selected':''}}>Biểu mẫu</option>
                             <option value="2" {{($file->type == 2)? 'selected':''}}>Tài liệu</option>
                           </select>
                        </div>
                    </div>
                    <!-- /basic select option input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.title_file')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="title" class="form-control" value="{{old('title',$file->title)}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic alias input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-2">Tiêu đề không dấu</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic alias input -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.keyword_file')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" name="keyword" class="form-control col-md-12 col-xs-12">{{$file->keyword}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->
                    
                     <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"></label>
                        <div class="col-lg-9">
                               @php 
                                    $splittedstring=explode("||",$file->file);   
                                @endphp
                                @foreach($splittedstring as $fl)
                                    <p>{{$fl}}</p>
                                @endforeach             

                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.file')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="file[]" class="form-input-styled" multiple=""/>
                            <p>Ghi chú: Chỉ cho phép định dạng jpg, jpeg, png, doc, docx, pdf, xlsx, xls và kích thước tối đa tệp tin là 10MB.</p>
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                     <!-- Basic content input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.description')}}</label>
                    </div>
                    
                    <hr>
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.short_description')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.description')}}</a></li>
                        </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.short_description')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" name="short_description" class="form-control col-md-12 col-xs-12" >{{ $file->short_description }}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab2">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.description')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="noidung" name="description" class="form-control col-md-12 col-xs-12">{{ $file->description }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic content input -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection