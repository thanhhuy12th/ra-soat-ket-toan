@extends('admin.master') 
@section('title','Danh Sách LinkFile') 
@section('content')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.list_file')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
       
       
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    
                    <th>#</th>
                    <th>Tiêu đề</th>
                    <th>Danh mục</th>
                    <th>Kiểu file</th>                   
                    <th>Link</th>

                  
                </tr>
            </thead>
            <tbody>
            	@foreach($file as $item)
                <tr>
                   
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->title}}</td>
                    <td>
                         @if(empty($item->id_category))
                            {{$item->name}}
                            @else
                                @foreach($opt_parent as $potionParent)
                                    @if($potionParent->id == $item->id_category)
                                        {{$potionParent->name}}
                                    @endif
                                @endforeach
                        @endif
                    </td>
                    <td>
                        @if ($item->type == 1)
                            <span class="badge badge-primary">Biểu mẫu</span>
                        @elseif ($item->type == 2)
                            <span class="badge badge-success">Tài liệu</span>
                        @endif
                    </td>
                    <td>
                        @php 
                            $splittedstring=explode("||",$item->file);   
                        @endphp
                        @for($i =0; $i<count($splittedstring)-1; $i++)
                            <p><a href="{{  url('/')."/uploads/file/".$splittedstring[$i]}}" target="_blank">{{$item->title}}</a></p>
                        @endfor

                    </td>
                    
                    
                    <td></td>
                </tr>
                @endforeach
            </tbody>
        </table>
           
    </div>
    <!-- /page length options -->
</div>
@endsection