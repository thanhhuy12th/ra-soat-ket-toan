@extends('admin.master')
@section('title','Thêm Nhận Xét')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('nhanxet.update',['id'=> $nhanxet->id])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.add_awareness')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.fullname_awareness')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="fullname" class="form-control" value="{{old('fullname',$nhanxet->fullname)}}" {{--  readonly="" --}} />
                        </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"></label>
                        <div class="col-lg-9">
                             <img src="{{ asset('uploads/avatar/'.$nhanxet->image)}}" width="20%">
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.image_awareness')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" />
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.content_awareness')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" name="content" class="form-control col-md-12 col-xs-12">{{old('content',$nhanxet->content)}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->


                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection