@extends('admin.master') 
@section('title','Danh Sách Category File') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.list_category_file')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('category-file.create')}}">
                <button type="button" class="btn btn-primary" class="">Thêm danh mục file</button>
            </a>
        </div>
        <form action="{{route('category-file.destroy',['action' => 'delete'])}}" />
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>#</th>
                    <th>Tên danh mục</th>
                    <th>Danh mục cha</th>
                    <th>Alias</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($catefile as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->name}}</td>
                    <td>
                        @if($item->parent_id == 0)
                        ROOT
                        @else
                            @foreach($optArr as $opt)
                                @if($opt->id == $item->parent_id)
                                    {{$opt->name}}
                                @endif
                            @endforeach
                        @endif 
                    </td>
                    </td>
                    <td>{{$item->alias}}</td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('category-file.edit',['category-file' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa danh mục file"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.delete')}}</button><br/><br>              
        </form> 
    </div>
    <!-- /page length options -->
</div>
@endsection