@extends('admin.master')
@section('title','Chỉnh Sửa Category File')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('category-file.update',['id'=> $catefile->id])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.edit_category_file')}}</legend>
                    
                       <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.parent_category_file')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                           <select name="parent_id" class="form-control">
                                <option value="0">ROOT</option>
                                @if($catefile->parent_id == 0)
                                        @for($i = 0; $i < count($optArr); $i++)
                                        <option value="{!!$optArr[$i]->id!!}">{!!$optArr[$i]->name!!}</option>
                                        @endfor
                                @else
                                    @for($i = 0; $i < count($optArr); $i++)
                                        <option value="{!!$optArr[$i]->id!!}" {{ $catefile->parent_id == $optArr[$i]->id ? "selected" : "" }}>{!!$optArr[$i]->name!!}</option>
                                    @endfor
                                @endif
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.name_category_file')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <input type="text" name="name" class="form-control" value="{{old('name',$catefile->name)}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->           

                   <!-- Basic text input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-3">{{trans('template.alias_category_file')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection