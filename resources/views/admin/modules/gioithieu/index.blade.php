@extends('admin.master')
@section('title','Giới Thiệu')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('introduce.update',['action' => 'id'])}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Introduce')}}</legend>
                    </center>

                     <!-- Basic ngon ngu input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.language_introduce')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                             <select id="Language" name="Language" class="form-control" />
                                <option data-content="{{$introducesVN->content}}" value="VN" {{($introducesVN->language == 'VN')? 'selected':''}}>VN</option>
                                <option data-content="{{$introducesEN->content}}" value="EN" {{($introducesEN->language == 'EN')? 'selected':''}}>EN</option>
                           </select>
                        </div>
                    </div>
                    <!-- /basic ngon ngu input -->
                
                    <!-- Basic text input -->
                    <div class="form-group row ">
                        <label class="col-form-label col-lg-2">{{trans('template.content_introduce')}}
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-9 hienthi">
                            <textarea rows="6" id="noidung" name="GioiThieu" class="form-control col-md-12 col-xs-12"></textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->
<script type="text/javascript">
    $(document).ready(function(){
        $("#Language").change(function(){
            $("#Language option:selected").each(function() {
                var id        = $(this).val();
                var content = $(this).data().content;
                var contentEN = $(this).val();
                console.log(id);
                if(id=="VN")
                {
                    CKEDITOR.instances.nd.setData(content);
                }
                else
                {
                    CKEDITOR.instances.nd.setData(content);
                }
            });
        });
    });
</script>
</div>
@endsection