@extends('admin.master')
@section('title','Thêm Slider')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('slider.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.add_slider')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.name_slider')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <input type="text" name="name" value="{{old('name')}}" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.intro_slider')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12">{{old('intro')}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.image_slider')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <input type="file" name="image" class="form-input-styled" >
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.create')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection