@extends('admin.master')
@section('title','LOGO')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('logo.update',['id' => $logo->id])}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-lg font-weight-bold">LOGO</legend>
                    </center>

                      <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"></label>
                        <div class="col-lg-9">
                            <img src="{{ asset('uploads/logo/'.$logo->content)}}" width="30%">
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                      <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.image_logo')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="content" class="form-input-styled" >
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection