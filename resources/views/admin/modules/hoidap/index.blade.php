@extends('admin.master') 
@section('title','Danh Sách FAQ') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.list_faq')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
       <div class="card-body">
            <a href="{{route('faq.create')}}">
                <button type="button" class="btn btn-primary" class="">Thêm FAQ</button>
            </a>
        </div>
        <form action="{{route('faq.destroy',['action' => 'delete'])}}" />
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>Tiêu đề</th>
                    <th>Nội dung</th>
                    <th>Keyword</th>
                    <th>Ngôn ngữ</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($faq as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$item->title}}</td>
                    <td>{!!$item->content!!}</td>
                    <td>{{$item->keyword}}</td>
                    <td>
                         @if ($item->language == "VN")
                            <span class="badge badge-primary">VN</span>
                        @elseif ($item->language == "EN")
                            <span class="badge badge-success">EN</span>
                        @endif
                    </td>
                    <td>{{ date("d/m/Y-H:i:s",strtotime($item ->created_at))}}</td>
                    <td class="text-center">
                        <a href="{{ route('faq.edit',['faq' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa FAQ"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.delete')}}</button><br/><br>              
        </form> 
    </div>
    <!-- /page length options -->
</div>
@endsection