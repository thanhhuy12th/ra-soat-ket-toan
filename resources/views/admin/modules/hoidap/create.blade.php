@extends('admin.master')
@section('title','Thêm FAQ')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('faq.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.add_faq')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.faq_title')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="title" maxlength="150" class="form-control" required="" />
                        </div>
                    </div>
                    <!-- /basic text input -->           

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-md-2">{{trans('template.faq_position')}}<span class="text-danger">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" type="number" name="position" required="">
                        </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.faq_keyword')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="keyword" class="form-control" required="" />                           
                        </div>
                    </div>
                    <!-- /basic text input -->
                 
                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.faq_content')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" id="noidung" name="content" class="form-control col-md-12 col-xs-12" required=""></textarea>
                            </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Basic ngon ngu input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.faq_language')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                             <select name="language" class="form-control" />
                                <option value="VN">VN</option>
                                <option value="EN">EN</option> 
                           </select>
                        </div>
                    </div>
                    <!-- /basic ngon ngu input -->

                </fieldset>
                <div class="v container mb-3 show">
                        <fieldset class="sub-item-v"><h4>Links 1</h4>
                                <i class="icon-cross pull-right absolute" onclick="delete_item(this)"> </i> 

                                <div class="form-group row ">
                                    <input id="linkfile" type="text" name="linkfile[]" class="form-control col-lg-7 mr-3" value="{{old('linkfile')}}" placeholder="Link file" required="">
                                    <input id="tenfile" type="text" name="tenfile[]" class="form-control col-lg-4 mr-3" value="{{old('tenfile')}}" placeholder="Tên file" required="">
                                </div> 
                        </fieldset>
                </div>

                <script>
                    function delete_item(elm) {
                        $(elm).parent().remove();
                    }
                    var count = 1;
                    $(document).ready(function($) {
                        $('.plusbtn').on('click', function() {
                            count++;
                            var html =
                                '<fieldset class="sub-item-v">'  + '<h4>Links ' + count + '</h4>' +
                                '<i class="icon-cross pull-right absolute "  onclick="delete_item(this)"> </i> </div> '   +
                                '<div class="form-group row ">'  +
                                '<input id="linkfile"'  + 'type="text"'  + 'name="linkfile[]"'  + 'class="form-control col-lg-7 mr-3"' + 
                                'placeholder="Link file">' +
                                '<input id="tenfile"'  + 'type="text"'  + 'name="tenfile[]"'  + 'class="form-control col-lg-4 mr-3"' + 
                                'placeholder="Tên file">' + 
                                '</div> </fieldset>' ;
                            $('.v').append(html);
                        })
                    });    
                </script>
                <div class="d-flex justify-content-center align-items-center">
                    <div class="btn btn-light plusbtn">{{trans('template.add_link')}}<i class="icon-plus2 ml-2" ></i></div>
                    <button type="reset" class="btn btn-light ml-3" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button name="btnAddFaq" type="submit" class="btn btn-primary ml-3">{{trans('template.create')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection