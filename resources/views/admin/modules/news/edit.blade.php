@extends('admin.master')
@section('title','Edit News')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('news.update',['id'=>$news->id])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.add_news')}}</legend>

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.parent_news')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                           <select name="parent_id" class="form-control">
                               <option value="0">ROOT</option>
                           
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic title input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.title_news')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="title" class="form-control" value="{{old('title',$news->title)}}" />
                        </div>
                    </div>
                    <!-- /basic title input -->

                    <!-- Basic alias input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-2">{{trans('template.alias_news')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic alias input -->


                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"></label>
                        <div class="col-lg-9">
                            <img src="{{asset('uploads/news/'.$news->image)}}" width="50%">
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.image_news')}}</label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" >
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Ưu tiên</label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                        <select name="status" class="form-control">
                                            <option value="0" {{($news->status == 0)? 'selected':'' }} >Không</option>                                    
                                            <option value="1" {{($news->status == 1)? 'selected':'' }} >Có</option>                                      
                                        </select>
                                    </div>                                   
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="language" class="form-control">
                                            <option value="VN" {{($news->language == 'VN')? 'selected':''}}>VN</option>
                                            <option value="EN" {{($news->language == 'EN')? 'selected':''}}>EN</option>
                                        </select>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>

                    <!-- Basic content input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.content')}}</label>
                    </div>
                    
                    <hr>
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.intro_news')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.content_news')}}</a></li>
                        </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.intro_news')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro',$news->intro)}}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab2">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.content_detail')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="nd" name="content" class="form-control col-md-12 col-xs-12">{{old('content',$news->content)}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic content input -->
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection