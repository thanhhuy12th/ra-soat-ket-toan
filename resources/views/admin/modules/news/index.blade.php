@extends('admin.master') 
@section('title','List News') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.list_news')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('news.create')}}">
                <button type="button" class="btn btn-primary">Thêm Tin Tức</button>
            </a>
        </div>
        <form action="{{route('news.destroy',['action' => 'delete'])}}" />
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>Tiêu đề</th>
                    <th>Mô tả</th>
                    <th>Nổi bật</th>
                    <th>Ngôn ngữ</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($news as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->intro }}</td>
                    <td>
                        <div class="form-check form-check form-check-switchery form-check-switchery-sm">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="status" class="form-input-switchery" {{ $item->status ? 'checked' : '' }}  >
                            </label>
                        </div>
                    </td>
                     <td>
                        @if ($item->language == "VN")
                            <span class="badge badge-primary">VN</span>
                        @elseif ($item->language == "EN")
                            <span class="badge badge-success">EN</span>
                        @endif
                        
                    </td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('news.edit',['news' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa tin tức"><i class="icon-pencil7"></i></a> &nbsp;
                      
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.delete')}}</button><br/><br>              
        </form>
    </div>
    <!-- /page length options -->
</div>
<script type="text/javascript">
$(document).ready(function () {
    $('.form-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var status = this.checked;
        $.ajax({
            type: "POST",
            url: "../../api/tin-tuc",
            data: {
                id: Id,
                status: status
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Bị hack rồi nha bạn :D');
            }
        })
    });
});

</script>
@endsection