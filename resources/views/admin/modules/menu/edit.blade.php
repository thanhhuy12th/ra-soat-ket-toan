@extends('admin.master')
@section('title','Edit Menu')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('menu.update',['id' => $menu->id])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.edit_menu')}}</legend>

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.menu_parent')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                           <select name="parent_id" class="form-control">
                            <option value="0">ROOT</option>
                                @if($menu->parent_id == 0)
                                        @for($i = 0; $i < count($optArr); $i++)
                                        <option value="{!!$optArr[$i]->id!!}">{!!$optArr[$i]->name!!}</option>
                                        @endfor
                                @else
                                    @for($i = 0; $i < count($optArr); $i++)
                                        <option value="{!!$optArr[$i]->id!!}" {{ $menu->parent_id == $optArr[$i]->id ? "selected" : "" }}>{!!$optArr[$i]->name!!}</option>
                                    @endfor
                                @endif
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.menu_name')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" value="{{$menu->name}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-2">{{trans('template.menu_slug')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="slug" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.menu_link')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="link" value="{{$menu->link}}" class="form-control"  />
                        </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"><span class="text-danger">*</span>{{trans('template.menu_position')}}</label>
                        <div class="col-lg-3">
                            <input type="number" name="position" class="form-control" value="{{old('position',$menu->position)}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Basic ngon ngu input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.language_menu')}}</label>
                        <div class="col-lg-3">
                             <select name="language" class="form-control" />
                                <option value="VN" {{($menu->language == 'VN')? 'selected':''}}>VN</option>
                                <option value="EN" {{($menu->language == 'EN')? 'selected':''}}>EN</option>
                           </select>
                        </div>
                    </div>
                    <!-- /basic ngon ngu input -->
                </fieldset>
                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection