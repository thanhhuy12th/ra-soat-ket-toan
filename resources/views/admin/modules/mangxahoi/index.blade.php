@extends('admin.master')
@section('title','Mạng Xã Hội')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('mangxahoi.update',['action' => 'id'])}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-lg font-weight-bold">Mạng Xã Hội</legend>
                    </center>

                      <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Facebook<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <input type="text" name="Facebook" class="form-control" value="{{$facebook->content}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                      <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Twitter<span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <input type="text" name="Twitter" class="form-control" value="{{$twitter->content}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection