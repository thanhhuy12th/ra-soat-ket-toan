@extends('admin.master')
@section('title','Footer')
@section('content')
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('footer.update',['action' => 'id'])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-lg font-weight-bold">Footer</legend>
                    </center>

                      <!-- Basic ngon ngu input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.language_footer')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                             <select name="Language" class="form-control" />
                                <option value="VN" {{($footer->language == 'VN')? 'selected':''}}>VN</option>
                                <option value="EN" {{($footer->language == 'EN')? 'selected':''}}>EN</option>
                           </select>
                        </div>
                    </div>
                    <!-- /basic ngon ngu input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Footer<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" id="nd" name="Footer" class="form-control col-md-12 col-xs-12">{{$footer->content}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.cancel')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection