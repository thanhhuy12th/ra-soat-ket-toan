<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->

    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="{{ asset('public/uploads/avatar/'.Auth::guard('admin')->user()->image)}}" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{Auth::guard('admin')->user()->fullname}}</div>
                        <div class="font-size-xs opacity-50">
                            <i style="color: green" class="icon-primitive-dot"></i> &nbsp;Online
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            @if(Auth::guard('admin')->user()->role ==1 )
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">ADMIN</div> <i class="icon-menu" title="Admin"></i></li>
                <li class="nav-item">
                    <a href="{{route('trangchu')}}" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                                    Trang Chủ
                                </span>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{route('admin.index')}}" class="nav-link"><i class="icon-people"></i> <span>Admin Pages</span></a>
                </li>
                 <li class="nav-item ">
                    <a href="{{route('menu.index')}}" class="nav-link"><i class="icon-menu3"></i> <span>Menu</span></a>
                </li>
                <li class="nav-item ">
                    <a href="{{route('slider.index')}}" class="nav-link"><i class="icon-images3"></i> <span>Slider</span></a>
                </li>
                <li class="nav-item ">
                    <a href="{{route('dichvu.index')}}" class="nav-link"><i class="icon-stats-bars2"></i> <span>Dịch Vụ</span></a>
                </li>
                 
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-price-tag"></i> <span>Bảng giá</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="FAQ">
                        <li class="nav-item"><a href="{{route('banggia.index')}}" class="nav-link">Danh sách bảng giá</a></li>
                        <li class="nav-item"><a href="{{route('baogiacontact.index')}}" class="nav-link">Danh sách gửi báo giá</a></li>
                    </ul>
                </li>
              
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-folder-plus"></i> <span>Tập tin</span></a>
                    
                    <ul class="nav nav-group-sub" data-submenu-title="Tập tin">
                        <li class="nav-item"><a href="{{route('category-file.index')}}" class="nav-link">Danh mục tập tin</a></li>
                        <li class="nav-item"><a href="{{route('file.index')}}" class="nav-link">Tập tin</a></li>
                        <li class="nav-item"><a href="{{route('file.link-file')}}" class="nav-link">Danh sách tập tin file</a></li>
                    </ul>
                </li>
               <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Quản lý tin tức</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Tin tức">
                        <li class="nav-item"><a href="{{route('category.index')}}" class="nav-link">Danh mục</a></li>
                        <li class="nav-item"><a href="{{route('news.index')}}" class="nav-link">Tin tức</a></li>
                    </ul>
                </li>
                <li class="nav-item ">
                    <a href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>FAQ</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="FAQ">
                        <li class="nav-item"><a href="{{route('faq.index')}}" class="nav-link">Danh sách câu hỏi</a></li>
                        <li class="nav-item"><a href="{{route('faqcontact.index')}}" class="nav-link">Danh sách hỗ trợ</a></li>
                    </ul>
                </li>
                 <li class="nav-item ">
                    <a href="{{route('nhanxet.index')}}" class="nav-link"><i class="icon-bubble-lines4"></i> <span>Nhận xét</span></a>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>Cấu hình website</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Cấu hình">
                        <li class="nav-item"><a href="{{route('introduce.index')}}" class="nav-link">Giới thiệu</a></li>
                        <li class="nav-item"><a href="{{route('logo.index')}}" class="nav-link">Logo - Banner</a></li>
                        <li class="nav-item"><a href="{{route('mangxahoi.index')}}" class="nav-link">Mạng xã hội</a></li>
                        <li class="nav-item"><a href="{{route('googlemap.index')}}" class="nav-link">Google Map</a></li>
                        <li class="nav-item"><a href="{{route('contactus.index')}}" class="nav-link">Liên hệ</a></li>
                        <li class="nav-item"><a href="{{route('footer.index')}}" class="nav-link">Footer</a></li>
                        <li class="nav-item"><a href="{{route('cauhinhkhac.index')}}" class="nav-link">Cấu hình khác</a></li>
                    </ul>
                </li> 
            </ul>
            @endif
            @if(Auth::guard('admin')->user()->role ==2 )
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">ADMIN ĐĂNG BÀI</div> <i class="icon-menu" title="Admin đăng bài"></i></li>
                <li class="nav-item">
                    <a href="{{route('trangchu')}}" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                                    Trang Chủ
                                </span>
                    </a>
                </li>
              
                <li class="nav-item ">
                    <a href="{{route('dichvu.index')}}" class="nav-link"><i class="icon-stats-bars2"></i> <span>Dịch Vụ</span></a>
                </li>
                 
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-price-tag"></i> <span>Bảng giá</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="FAQ">
                        <li class="nav-item"><a href="{{route('banggia.index')}}" class="nav-link">Danh sách bảng giá</a></li>
                        <li class="nav-item"><a href="{{route('baogiacontact.index')}}" class="nav-link">Danh sách gửi báo giá</a></li>
                    </ul>
                </li>
              
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-folder-plus"></i> <span>Tập tin</span></a>
                    
                    <ul class="nav nav-group-sub" data-submenu-title="Tập tin">
                      {{--   <li class="nav-item"><a href="{{route('category-file.index')}}" class="nav-link">Danh mục tập tin</a></li> --}}
                        <li class="nav-item"><a href="{{route('file.index')}}" class="nav-link">Tập tin</a></li>
                        <li class="nav-item"><a href="{{route('file.link-file')}}" class="nav-link">Danh sách tập tin file</a></li>
                    </ul>
                </li>
               <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Quản lý tin tức</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Tin tức">
                       {{--  <li class="nav-item"><a href="{{route('category.index')}}" class="nav-link">Danh mục</a></li> --}}
                        <li class="nav-item"><a href="{{route('news.index')}}" class="nav-link">Tin tức</a></li>
                    </ul>
                </li>
                <li class="nav-item ">
                    <a href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a>
                </li>
                 <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>FAQ</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="FAQ">
                        <li class="nav-item"><a href="{{route('faq.index')}}" class="nav-link">Danh sách câu hỏi</a></li>
                        <li class="nav-item"><a href="{{route('faqcontact.index')}}" class="nav-link">Danh sách hỗ trợ</a></li>
                    </ul>
                </li>
 
            @endif
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>