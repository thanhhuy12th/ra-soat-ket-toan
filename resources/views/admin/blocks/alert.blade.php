@if (\Session::has('success'))
<div class="card" id="flash-message">
    <div class="card-header bg-success">
      <h6 class="card-title">
        <a data-toggle="collapse" class="text-white" aria-expanded="true">Success</a>
      </h6>
    </div>
    <div id="collapsible-styled-group1" class="collapse show" style="">
      <div class="card-body">
          {!! __(\Session::get('success')) !!}
      </div>
    </div>
</div>
@endif
<!-- --------------------error ----------------- -->
@if (\Session::has('error'))
<div class="card" id="flash-message">
    <div class="card-header bg-danger">
      <h6 class="card-title">
        <a data-toggle="collapse" class="text-white" aria-expanded="true">Error</a>
      </h6>
    </div>
    <div id="collapsible-styled-group1" class="collapse show" style="">
      <div class="card-body">
          {!! __(\Session::get('error')) !!}
      </div>
    </div>
</div>
@endif
