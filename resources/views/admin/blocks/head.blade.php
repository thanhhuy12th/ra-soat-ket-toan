<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>@yield('title','Quản Trị website')</title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{asset('backend/global_assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('backend/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('backend/assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('backend/assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('backend/assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('backend/assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script src="{{asset('backend/global_assets/js/main/jquery.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="{{asset('backend/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>

<script type="text/javascript" src="{{asset('backend/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/global_assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>

<script src="{{asset('backend/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('ckfinder/ckfinder.js')}}"></script>

<script src="{{asset('backend/assets/js/app.js')}}"></script>
<script src="{{asset('backend/global_assets/js/demo_pages/dashboard.js')}}"></script>
<script src="{{asset('backend/global_assets/js/demo_pages/form_validation.js')}}"></script>
<script src="{{asset('backend/global_assets/js/demo_pages/datatables_advanced.js')}}"></script>
<!-- /theme JS files -->
<script src="{{asset('backend/global_assets/js/plugins/uploaders/dropzone.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/demo_pages/uploader_dropzone.js')}}"></script>
<!-- Theme JS files -->
<script src="{{asset('backend/global_assets/js/plugins/visualization/echarts/echarts.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/demo_pages/charts/echarts/columns_waterfalls.js')}}"></script>

<script src="{{asset('backend/global_assets/js/demo_pages/charts/echarts/areas.js')}}"></script>
<!-- /theme JS files -->
<script src="{{asset('frontend/assets/js/main.js')}}"></script>