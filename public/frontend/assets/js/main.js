$(document).ready(function() {

    $('.swap-inner').hide();
    $('.ContentSwap103').hover(function() {
        $('.swap-inner').stop(true, true).slideToggle();
    });

    var menuActive = false;
    if ($('.bar-menu').length) {
        var hamb = $('.bar-menu');
        hamb.on('click', function(event) {
            event.stopPropagation();
            if (!menuActive) {

                openMenu();
                $(document).one('click', function cls(e) {
                    if ($(e.target).hasClass('menu_mm')) {
                        $(document).one('click', cls);
                    } else {
                        closeMenu();
                    }
                });
            } else {
                $('.menu').removeClass('active');
                menuActive = false;
            }
        });
        //Handle page menu
        if ($('.page_menu_item').length) {
            var items = $('.page_menu_item');
            items.each(function() {
                var item = $(this);
                item.on('click', function(evt) {
                    if (item.hasClass('has-children')) {

                        evt.stopPropagation();
                        var subItem = item.find('> ul');
                        if (subItem.hasClass('active')) {
                            subItem.toggleClass('active');
                            TweenMax.to(subItem, 0.3, { height: 0 });
                        } else {
                            subItem.toggleClass('active');
                            TweenMax.set(subItem, { height: "auto" });
                            TweenMax.from(subItem, 0.3, { height: 0 });
                        }
                    } else {
                        evt.stopPropagation();
                    }
                });
            });
        }
    }

    function openMenu() {
        var fs = $('.menu');
        fs.addClass('active');
        menuActive = true;
    }

    function closeMenu() {
        var fs = $('.menu');
        fs.removeClass('active');
        menuActive = false;
    }

    if ($('.list-group-item').length) {
        var items = $('.list-group-item');
        items.each(function() {
            var item = $(this);
            item.on('click', function(evt) {
                if (item.hasClass('has-children')) {
                    evt.stopPropagation();
                    var subItem = item.find('> ul');
                    if (subItem.hasClass('active')) {
                        subItem.toggleClass('active');
                        TweenMax.to(subItem, 0.3, { height: 0 });
                    } else {
                        subItem.toggleClass('active');
                        TweenMax.set(subItem, { height: "auto" });
                        TweenMax.from(subItem, 0.3, { height: 0 });
                    }
                } else {
                    evt.stopPropagation();
                }
            });
        });

    }






});