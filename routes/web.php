<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::namespace('Frontend')->middleware('locale')->group(function(){
	Route::get('','HomeController@index')->name('home');
	Route::get('gioi-thieu','HomeController@Introduce')->name('gioi-thieu');
	Route::get('dich-vu','HomeController@Service')->name('dich-vu');
	Route::get('tin-tuc','HomeController@News')->name('tin-tuc');
	Route::get('bieu-mau','HomeController@BieuMau')->name('bỉeu-mau');
	Route::get('tai-lieu','HomeController@TaiLieu')->name('tai-lieu');
	Route::get('tin-tuc/{alias}','HomeController@Newsdetail')->name('tin-tuc-detail');
	Route::get('danh-muc-tin-tuc/{id}/{tenloai}','HomeController@DanhmucNews')->name('danh-muc-tin-tuc');
	Route::get('lien-he','HomeController@Lienhe')->name('lien-he');
	Route::post('lien-he','HomeController@PostLienhe');
	Route::get('faq-contact','HomeController@getFaqContact')->name('faq-contact');
	Route::post('faq-contact','HomeController@postFaqContact');
	Route::get('bang-gia','HomeController@BangGia')->name('bang-gia');

	Route::get('bang-gia-contact','HomeController@getBaoGiaContact')->name('bang-gia-contact');
	Route::post('bang-gia-contact','HomeController@postBaoGiaContact');
	
	Route::get('bang-gia/{alias}','HomeController@Banggiadetail')->name('bang-gia-detail');
	Route::get('bieu-mau','HomeController@Bieumau')->name('bieu-mau');
	Route::get('bieu-mau/{alias}','HomeController@Bieumaudetails')->name('bieu-mau-detail');
	Route::get('tai-lieu','HomeController@TaiLieu')->name('tai-lieu');
	Route::get('danh-muc-bieu-mau/{id}/{tenloai}','HomeController@CateDanhmucBieumau')->name('danh-muc-bieu-mau');
	Route::get('danh-muc-tai-lieu/{id}/{tenloai}','HomeController@CateDanhmucTaiLieu')->name('danh-muc-tai-lieu');
	Route::get('hoi-dap','HomeController@FAQ')->name('hoi-dap');
	Route::post('tim-kiem-hoi-dap','HomeController@timkiem_hoidap');
	Route::post('tim-kiem-bieu-mau','HomeController@timkiem_bieumau');
	Route::post('tim-kiem','HomeController@timkiem');
	
	Route::get('change-language/{language}', 'HomeController@changeLanguage')->name('user.change-language');

});

Route::get('sign-in','LoginController@getLogin')->middleware('loginal')->name('getLogin');
Route::get('logout','LoginController@getLogout')->name('getLogout');
Route::post('sign-in','LoginController@postLogin');

Route::namespace('Admin')->middleware('login')->prefix('admin')->group(function(){
	Route::get('/','HomeController@index')->name('trangchu');

	Route::prefix('admin')->name('admin.')->group(function(){
		Route::get('index','AdminsController@index')->name('index');

		Route::get('add','AdminsController@create')->name('create');
		Route::post('add','AdminsController@store')->name('store');

		Route::get('edit/{id}','AdminsController@edit')->name('edit');
		Route::post('edit/{id}','AdminsController@update')->name('update');

		Route::get('change-password','AdminsController@Getchangepass')->name('changepass.update');
		Route::post('change-password','AdminsController@changepass')->name('postchangepass.update');

		Route::get('delete/{id}','AdminsController@destroy')->name('destroy');
	});
	Route::prefix('menu')->name('menu.')->group(function(){
		Route::get('index','MenuController@index')->name('index');

		Route::get('add','MenuController@create')->name('create');
		Route::post('add','MenuController@store')->name('store');

		Route::get('edit/{id}','MenuController@edit')->name('edit');
		Route::post('edit/{id}','MenuController@update')->name('update');

		Route::get('delete/{id}','MenuController@destroy')->name('destroy');
	});
	Route::prefix('slider')->name('slider.')->group(function(){
		Route::get('index','SliderController@index')->name('index');

		Route::get('add','SliderController@create')->name('create');
		Route::post('add','SliderController@store')->name('store');

		Route::get('edit/{id}','SliderController@edit')->name('edit');
		Route::post('edit/{id}','SliderController@update')->name('update');

		Route::get('delete/{id}','SliderController@destroy')->name('destroy');
	});
	Route::prefix('lienhe')->name('lienhe.')->group(function(){
		Route::get('index','LienheController@index')->name('index');

		Route::get('add','LienheController@create')->name('create');
		Route::post('add','LienheController@store')->name('store');

		Route::get('edit/{id}','LienheController@edit')->name('edit');
		Route::post('edit/{id}','LienheController@update')->name('update');

		Route::get('delete/{id}','LienheController@destroy')->name('destroy');
	});
	Route::prefix('hoidap')->name('faq.')->group(function(){
		Route::get('index','FAQController@index')->name('index');

		Route::get('add','FAQController@create')->name('create');
		Route::post('add','FAQController@store')->name('store');

		Route::get('edit/{id}','FAQController@edit')->name('edit');
		Route::post('edit/{id}','FAQController@update')->name('update');

		Route::get('delete/{id}','FAQController@destroy')->name('destroy');
	});
	Route::prefix('nhanxet')->name('nhanxet.')->group(function(){
		Route::get('index','NhanXetController@index')->name('index');

		Route::get('add','NhanXetController@create')->name('create');
		Route::post('add','NhanXetController@store')->name('store');

		Route::get('edit/{id}','NhanXetController@edit')->name('edit');
		Route::post('edit/{id}','NhanXetController@update')->name('update');

		Route::get('delete/{id}','NhanXetController@destroy')->name('destroy');
	});
	Route::prefix('category-file')->name('category-file.')->group(function(){
		Route::get('index','CategoryFileController@index')->name('index');

		Route::get('add','CategoryFileController@create')->name('create');
		Route::post('add','CategoryFileController@store')->name('store');

		Route::get('edit/{id}','CategoryFileController@edit')->name('edit');
		Route::post('edit/{id}','CategoryFileController@update')->name('update');

		Route::get('delete/{id}','CategoryFileController@destroy')->name('destroy');
	});
	Route::prefix('file')->name('file.')->group(function(){
		Route::get('index','FileController@index')->name('index');
		Route::get('link-file','FileController@Linkfile')->name('link-file');

		Route::get('add','FileController@create')->name('create');
		Route::post('add','FileController@store')->name('store');

		Route::get('edit/{id}','FileController@edit')->name('edit');
		Route::post('edit/{id}','FileController@update')->name('update');

		Route::get('delete/{id}','FileController@destroy')->name('destroy');
	});
	Route::prefix('category')->name('category.')->group(function(){
		Route::get('index','CategoryController@index')->name('index');

		Route::get('add','CategoryController@create')->name('create');
		Route::post('add','CategoryController@store')->name('store');

		Route::get('edit/{id}','CategoryController@edit')->name('edit');
		Route::post('edit/{id}','CategoryController@update')->name('update');

		Route::get('delete/{id}','CategoryController@destroy')->name('destroy');
	});
	Route::prefix('news')->name('news.')->group(function(){
		Route::get('index','NewsController@index')->name('index');

		Route::get('add','NewsController@create')->name('create');
		Route::post('add','NewsController@store')->name('store');

		Route::get('edit/{id}','NewsController@edit')->name('edit');
		Route::post('edit/{id}','NewsController@update')->name('update');

		Route::get('delete/{id}','NewsController@destroy')->name('destroy');
	});
	Route::prefix('dichvu')->name('dichvu.')->group(function(){
		Route::get('index','ServiceController@index')->name('index');

		Route::get('add','ServiceController@create')->name('create');
		Route::post('add','ServiceController@store')->name('store');

		Route::get('edit/{id}','ServiceController@edit')->name('edit');
		Route::post('edit/{id}','ServiceController@update')->name('update');

		Route::get('delete/{id}','ServiceController@destroy')->name('destroy');
	});
	Route::prefix('banggia')->name('banggia.')->group(function(){
		Route::get('index','ListPriceController@index')->name('index');

		Route::get('add','ListPriceController@create')->name('create');
		Route::post('add','ListPriceController@store')->name('store');

		Route::get('edit/{id}','ListPriceController@edit')->name('edit');
		Route::post('edit/{id}','ListPriceController@update')->name('update');

		Route::get('delete/{id}','ListPriceController@destroy')->name('destroy');
	});
	Route::prefix('gioithieu')->name('introduce.')->group(function(){
		Route::get('index','IntroduceController@index')->name('index');
		Route::post('edit/{id}','IntroduceController@update')->name('update');
	});
	Route::prefix('logo')->name('logo.')->group(function(){
		Route::get('index','LogoController@index')->name('index');
		Route::post('edit/{id}','LogoController@update')->name('update');
	});
	Route::prefix('mangxahoi')->name('mangxahoi.')->group(function(){
		Route::get('index','SocialNetworkController@index')->name('index');
		Route::post('edit/{id}','SocialNetworkController@update')->name('update');
	});
	Route::prefix('googlemap')->name('googlemap.')->group(function(){
		Route::get('index','GoogleMapController@index')->name('index');
		Route::post('edit/{id}','GoogleMapController@update')->name('update');
	});
	Route::prefix('contactus')->name('contactus.')->group(function(){
		Route::get('index','ContactUsController@index')->name('index');
		Route::post('edit/{id}','ContactUsController@update')->name('update');
	});
	Route::prefix('footer')->name('footer.')->group(function(){
		Route::get('index','FooterController@index')->name('index');
		Route::post('edit/{id}','FooterController@update')->name('update');
	});
	Route::prefix('cauhinhkhac')->name('cauhinhkhac.')->group(function(){
		Route::get('index','ConfigurationController@index')->name('index');
		Route::post('edit/{id}','ConfigurationController@update')->name('update');
	});
	Route::prefix('faqcontact')->name('faqcontact.')->group(function(){
		Route::get('index','FAQContactController@index')->name('index');

		Route::get('add','FAQContactController@create')->name('create');
		Route::post('add','FAQContactController@store')->name('store');

		Route::get('edit/{id}','FAQContactController@edit')->name('edit');
		Route::post('edit/{id}','FAQContactController@update')->name('update');

		Route::get('delete/{id}','FAQContactController@destroy')->name('destroy');
	});
	Route::prefix('baogiacontact')->name('baogiacontact.')->group(function(){
		Route::get('index','BaoGiaContactController@index')->name('index');

		Route::get('add','BaoGiaContactController@create')->name('create');
		Route::post('add','BaoGiaContactController@store')->name('store');

		Route::get('edit/{id}','BaoGiaContactController@edit')->name('edit');
		Route::post('edit/{id}','BaoGiaContactController@update')->name('update');

		Route::get('delete/{id}','BaoGiaContactController@destroy')->name('destroy');
	});


});
